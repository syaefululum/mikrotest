$(function() {
	$('#datablesExaminations').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlExaminations').val(),
		"columnDefs" : [
		{
			"targets" : [0,6],
			"bSearchable" : false
		},		
		{
			"targets" : [6],
			"bSortable" : false,
			"class" : 'center-tr'
		},		
		{
			"targets" : [0],
			"visible" : false
		}],
		"columns" : [			
			{data : "id"},
			{data : "category"},
			{data : "name"},
			{data : "fare"},
			{data : "unit"},
			{data : "normal_value"},
			{
				width: "200px", 
				data : "actions"
			},
		],
		"order"          : [[0, "asc"]]
    });	
});
