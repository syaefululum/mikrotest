$(function() {
	var typeSelected = $("input[type='radio'][name='type']:checked");
	console.log(typeSelected.val());
	if (typeSelected.val() == '1') {
		$(".optional").css("display", "block");
	}
	else if (typeSelected.val() == '2') {
		$(".optional").css("display", "none");
	}
	
	$('input[type=radio][name=type]').change(function() {
		if (this.value == '1') {
			$(".optional").css("display", "block");
        }
        else if (this.value == '2') {
            $(".optional").css("display", "none");
        }
    });
});
