$(function() {
	$('#datablesExpenseOutcome').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlExpenseOutcome').val(),
		"columnDefs" : [
		{
			"targets" : [0,4],
			"bSearchable" : false
		},		
		{
			"targets" : [4],
			"bSortable" : false,
			"class" : 'center-tr'
		},		
		{
			"targets" : [0],
			"visible" : false
		}],
		"columns" : [			
			{data : "id"},
			{data : "date"},
			{data : "name"},
			{data : "value"},
			{
				width: "200px", 
				data : "actions"
			},
		],
		"order"          : [[1, "asc"]]
    });	
});
