<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_doctor extends MY_Model {
	protected $table = 'doctor';
	
	function get_doctor_service_by_id($id){
		$condition = array(
			"id" => $id,
			"status" => 1
		);
		$this->db->select("service");
		$this->db->from($this->table);
		$this->db->where($condition);
        $query = $this->db->get();
		
		$service = 0;
		if($query->result()){
			foreach($query->result() as $emply){
				$service = $emply->service;
			}
		}
		
		return $service;
	}
}

