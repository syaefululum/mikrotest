<style>
	.form-group.has-error .form-control.information{
		border-color: #d2d6de !important;
	}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Input Hasil Pemeriksaan
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Input Hasil Pemeriksaan</li>
    </ol>
</section>
<!-- Main content -->
<?php echo form_open_multipart(base_url(uri_string()), array('class' => 'form-horizontal form-ajax'));?>
<section class="content" id="printableArea">
    <div class="row">
        <div class="col-md-12">
			<div class="result">
				<?php
				if (! empty($message_success)) {
					echo '<div class="alert alert-success" role="alert">';
					echo $message_success;
					echo '</div>';
				}
				if (! empty($message)) {
					echo '<div class="alert alert-danger" role="alert">';
					echo $message;
					echo '</div>';
				}
				?>
			</div>
		</div>
		<div class='col-xs-6'>  
			<div class="box box-solid">
                <div class="box-body">
					<?php echo date("D, d/m/Y, h:i A", strtotime($transaction->date));?>
				</div>
            </div>
        </div>
		<div class='col-xs-6'>  
			<div class="box box-solid">
                <div class="box-body text-right">
					No. <?php echo $transaction->transaction_no;?>
				</div>
            </div>
        </div>
	</div>
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-solid transaction-identity-container-222">
                <div class="box-header with-border">
                    <h3 class="box-title">Identitas Pasien</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- form start -->
                    <div class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">Nama</label>
                                <div class="col-sm-9 patient-address"><?php echo $patient->name;?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">Tgl. Lahir</label>
                                <div class="col-sm-9 patient-dob"><?php echo $patient->dob;?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">Alamat</label>
                                <div class="col-sm-9 patient-address"><?php echo $patient->address;?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">No. Telp</label>
                                <div class="col-sm-9 patient-phone"><?php echo $patient->phone;?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">Jenis Kelamin</label>
									<div class="col-sm-9 patient-phone"><?php 									
									$options_gender = array(
											'' => '',
											'0' => '',
											'L' => 'Laki-laki',
											'P' => 'Perempuan',
									);
									echo $options_gender[$patient->gender];?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
        <div class="col-xs-6">
            <div class="box box-solid transaction-identity-container-222">
                <div class="box-header with-border">
                    <h3 class="box-title">Identitas Dokter</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body clearfix">
                    <!-- form start -->
                    <div class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">Nama</label>
                                <div class="col-sm-9 doctor-address"><?php echo $doctor->name;?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">Alamat</label>
                                <div class="col-sm-9 doctor-address"><?php echo $doctor->address;?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <div class="row">
		<div class="col-md-12">
            <div class="box box-solid">
			<?php foreach($examination_by_category as $ebc) : ?>
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo ucwords($ebc->name);?></h3>
				</div>
				<div class="box-body clearfix">
					<div class="form-horizontal">
						<div class="box-body">
							<?php 
							$examinations = $ebc->examination;
							foreach($examinations as $examination) : ?>
								<div class="form-group <?php echo (form_error('examination_result_input['.$examination->examination_result->id.']') ? 'has-error' : '')?>">
									<label for="password" class="col-sm-4 control-label"><?php echo $examination->name;?></label>
									<div class="col-sm-8 print-hide">
										<?php if ($examination->type == 1) {?>
										<div class="row">
											<div class="col-sm-4">
												<?php echo form_input($examination_result_input[$examination->examination_result->id]); ?>
												<span class="help-block"><?php echo form_error('examination_result_input['.$examination->examination_result->id.']'); ?></span>
											</div>
											<div class="col-sm-2 print-hide"><?php echo $examination->unit;?></div>
											<div class="col-sm-6">
												<?php echo form_input($examination_result_notes[$examination->examination_result->id]); ?>
											</div>
										</div>
										<?php }else{ ?>
											<input type="hidden" name="examination_result_input[<?php echo $examination->examination_result->id?>]" value="">
										<?php } ?>
									</div>
								</div>
								<div class="col-sm-1">
								</div>
								<div class="col-sm-12">
								<?php 
								foreach($examination->subexamination as $subexamination) : ?>
									<div class="form-group">
										<label for="password" class="col-sm-4 control-label"><?php echo $subexamination->name;?></label>
										<div class="col-sm-8 print-hide">
											<?php if($subexamination->type == 1){?>
											<div class="row">
												<div class="col-sm-4">
													<input class="form-control requiredTextField" type="text" placeholder="Hasil <?php echo $subexamination->name;?>" field-name="examination_result_input[<?php echo $subexamination->result_id?>]" maxlength="25" value="" name="examination_result_input[<?php echo $subexamination->result_id?>]">
													<span class="help-block" style="color:red;"><?php echo form_error('examination_result_input['.$subexamination->result_id.']'); ?></span>
												</div>
												<div style="padding-left:0px;" class="col-sm-2 print-hide"><?php echo $subexamination->unit;?></div>
												<div class="col-sm-6">
													<input class="form-control information" type="text" placeholder="Keterangan <?php echo $subexamination->name;?>" field-name="examination_result_notes[<?php echo $subexamination->result_id?>]" maxlength="25" value="" name="examination_result_notes[<?php echo $subexamination->result_id?>]">
													<span class="help-block" style="color:red;"><?php echo form_error('examination_result_notes['.$subexamination->result_id.']'); ?></span>
												</div>
											</div>
											<?php }else{?>
											<div class="row">
												<div class="col-sm-12">
													<input type="hidden" name="examination_result_input[<?php echo $subexamination->result_id?>]">
													<input class="form-control information" type="text" placeholder="Keterangan <?php echo $subexamination->name;?>" field-name="examination_result_notes[<?php echo $subexamination->result_id?>]" maxlength="25" value="" name="examination_result_notes[<?php echo $subexamination->result_id?>]">
													<span class="help-block" style="color:red;"><?php echo form_error('examination_result_notes['.$subexamination->result_id.']'); ?></span>
												</div>
											</div>
											<?php }?>
										</div>
									</div>
								<?php endforeach ?>
								</div>
							<?php endforeach ?>
						</div>
					</div>
				</div>
			<?php endforeach ?>
			</div>
        </div>
        <!-- /.col -->
    </div>
    <div class="row print-hide">
        <div class='col-md-12'>
			<button class="btn btn-primary pull-right" type="submit">Print Result</button> 
			<button class="btn btn-info pull-right hidden" id="printBeforeNext" type="button" style="margin-right: 10px;">Print Struk</button>
        </div>
	</div>
    <!-- /.row -->
</section>
<?php echo form_close(); ?>
<!-- /.content -->