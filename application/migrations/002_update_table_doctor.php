<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_table_doctor extends CI_Migration {

	public function up()
	{
		$fields_add = array(
			'service' => array(
				'type' => 'float',
				'default' => 0
			)
		);
		$this->dbforge->add_column('doctor', $fields_add);
	}

	public function down()
	{
		
	}
}
