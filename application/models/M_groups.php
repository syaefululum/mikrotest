<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_groups extends MY_Model {
	protected $table = 'groups';
	
	function get_groups_dropdown($group_id){
		$condition = array("id >=" => $group_id);
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where($condition);
        $query = $this->db->get();
		
		$dropdown = array();
		
		if($query->result()){
			foreach($query->result() as $group){
				$dropdown[$group->id] = $group->description;
			}
		}
		
		return $dropdown;
	}
}

