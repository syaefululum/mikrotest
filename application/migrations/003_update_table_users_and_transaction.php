<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_table_users_and_transaction extends CI_Migration {

	public function up()
	{
		$fields_add = array(
			'transaction_no' => array(
				'type' => 'varchar',
				'constraint' => '20',
				'default' => ''
			)
		);
		$this->dbforge->add_column('transaction', $fields_add);
		
		$fields_add = array(
			'updated_at' => array(
				'type' => 'date'
			),
			'updated_by' => array(
				'type' => 'int',
				'constraint' => '11',
				'default' => 0
			),
			'password_original' => array(
				'type' => 'varchar',
				'constraint' => '50'
			)
		);
		
		$this->dbforge->add_column('users', $fields_add);
	}

	public function down()
	{
		
	}
}
