$(function() {
	$('#datablesSalary').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlSalary').val(),
		"columnDefs" : [
		{
			"targets" : [0],
			"bSearchable" : false,
			"visible" : false
		},
		{
			"targets" : [5],
			"bSortable" : false,
			"class" : 'center-tr'
		},					
		{
			"targets" : [9],
			"bSearchable" : false,
		}],
		"columns" : [			
			{data : "id"},
			{data : "name"},
			{data : "date",width: "150px"},
			{data : "basic_salary"},
			{data : "incentive"},
			{data : "overtime"},
			{data : "rounding_off"},
			{data : "salary"},
			{data : "information"},
			{
				width: "85px", 
				data : "actions"
			},
		],
		"order"          : [[2, "desc"], [1, "asc"]]
    });	
	
	
    //Date picker
    $('.datepicker-input').datepicker({
		autoclose: true,
		format: "dd-mm-yyyy",
    });	
});
