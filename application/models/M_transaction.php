<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_transaction extends MY_Model {
	protected $table = 'transaction';
	
	public function get_income($condition){
        $this->db->select("SUM(total) as value");
        $this->db->where($condition);
		return $this->get($this->table)->row();
	}
	
	public function get_last_id(){
		$last_id = 1;
		$query = $this->db->query(
		"	SELECT MAX(id) as id
			FROM transaction
		"
		);
		if($query!=null){
			if($query->num_rows() == 1){
				$result = $query->result()[0];
				$last_id = $result->id;
			}
		}
		return $last_id;
	}
	
	public function get_report_income($date_start,$date_end){
		$select = array(
			't.id',
			'd.name as doctor',
			'p.name as patient',
			'DATE_FORMAT(t.date,"%d-%m-%Y %H:%i:%s") as date',
			't.total',
			't.discount',
		);
		
		$condition = array();
		
		if($date_start){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		
		$this->db->select(implode(",", $select));
		$this->db->from('transaction t');
		$this->db->where($condition);
		$this->db->join("doctor d", "d.id = t.doctor_id", "left");
		$this->db->join("patient p", "p.id = t.patient_id", "left");
		$this->db->order_by('t.date','desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_report_outcome($date_start,$date_end){
		$select = array(
			'name',
			'DATE_FORMAT(date,"%d-%m-%Y") as date',
			'value'
		);
		
		$condition = array();
		
		if($date_start){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		
		$this->db->select(implode(",", $select));
		$this->db->from('view_outcome');
		$this->db->where($condition);
		$this->db->order_by('date','desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_report_expense_outcome($date_start,$date_end){
		$select = array(
			'id',
			'name',
			'DATE_FORMAT(date,"%d-%m-%Y") as date',
			'value'
		);
		
		$condition = array(
			"status" => 1,
		);
		
		if($date_start){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		
		$this->db->select(implode(",", $select));
		$this->db->from('expense');
		$this->db->where($condition);
		$this->db->order_by('date','desc');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_report_salary($date_start,$date_end){
		$select = array(
			'salary_e.id',
			'employee.name',
			'DATE_FORMAT(date,"%d-%m-%Y") as date',
			'basic_salary',
			'incentive',
			'overtime',
			'rounding_off',
			'salary',
			'information',
		);
		
		$condition = array(
			"employee.status" => 1,
			"salary_e.status" => 1,
		);
		
		if($date_start){
			$condition["DATE_FORMAT(salary_e.date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(salary_e.date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		
		$this->db->select(implode(",", $select));
		$this->db->from('salary salary_e');
		$this->db->where($condition);
		$this->db->join("employee", "salary_e.employee_id = employee.id", "inner");
		$this->db->order_by('salary_e.date','desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_report_docter_service($date_start,$date_end){
		$select = array(
			't.id',
			'd.name as doctor',
			'p.name as patient',
			'DATE_FORMAT(t.date,"%d-%m-%Y") as date',
			't.total',
			't.percent_doctor_services',
			'((t.percent_doctor_services/100) * t.total) as doctor_services',
		);
		
		$condition = array("t.percent_doctor_services >" => 0);
		//$condition = array();
		if($date_start){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		
		$this->db->select(implode(",", $select));
		$this->db->from('transaction t');
		$this->db->where($condition);
		$this->db->join("doctor d", "d.id = t.doctor_id", "left");
		$this->db->join("patient p", "p.id = t.patient_id", "left");
		$this->db->order_by('t.date','desc');
		$query = $this->db->get();
		return $query->result();
		
	}
	
	public function generate_transaction_no(){
		$transaction_no = 0;
		$date = date('Y-m');
		$this->db->select('count(*) as total_transaction');
		$this->db->from('transaction');
		$this->db->where("DATE_FORMAT(date,'%Y-%m') =",$date);
		$query = $this->db->get();
		if($query->row()){
			$transaction_data = $query->row();
			$transaction_no = $transaction_data->total_transaction;
		}
		$transaction_no = $transaction_no + 1;
		$transaction_no = sprintf("%05d", $transaction_no);
		return $transaction_no;
	}
}

