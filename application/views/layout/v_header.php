<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
			<img style="height: 45px;margin-top: -3px" src="<?php echo base_url("assets/img/Logo3final.png"); ?>"/>
		</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
			<img style="height: 50px;margin-top: -5px" src="<?php echo base_url("assets/img/Logo1final.png"); ?>"/>
		</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="dist/img/user2-160x160.jpg" class="user-image hidden" alt="User Image">
                    <span class=""><?php echo $user->first_name." ".$user->last_name;?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header" style="height: 110px">
                            <img src="dist/img/user2-160x160.jpg" class="img-circle hidden" alt="User Image">
                            <p>
                                <?php echo $user->first_name." ".$user->last_name."</br>".$user->company;?>
                                <small><?php echo $user->email;?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left hidden">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a data-logout-url="<?php echo base_url("auth/logout");?>" class="btn btn-default btn-flat logout-button">
									Sign out
								</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Sign Out Button -->
                <li>
                    <a class="logout-button" data-logout-url="<?php echo base_url("auth/logout");?>" title="Sign Out">
						<i class="fa fa-sign-out"></i>
					</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header hidden">MAIN NAVIGATION</li>
			
			<?php $group_menu = array("examinations_create","examinations_results");
			if(array_intersect($group_menu, $menu_enabled)) : ?>
            <li class="<?php echo (in_array($active_menu, $group_menu) ? "active" : "");?> treeview">
                <a href="#">
                <i class="fa fa-exchange" aria-hidden="true"></i> <span>Transaksi</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="<?php echo (in_array($active_menu, $group_menu) ? "menu-open" : "");?> treeview-menu">
					<?php if(in_array("examinations_create", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "examinations_create" ? "active" : "");?>"><a href="<?php echo base_url("main/transaction");?>">Input Data Pasien </a></li>
					<?php endif ?>
					<?php if(in_array("examinations_results", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "examinations_results" ? "active" : "");?>"><a href="<?php echo base_url("main/examinations_results");?>">Input Hasil Pemeriksaan</a></li>
					<?php endif ?>
                </ul>
            </li>
			<?php endif ?>
			<?php $group_menu = array("expense_operational","expense_employees");
			if(array_intersect($group_menu, $menu_enabled)) : ?>
            <li class="<?php echo (in_array($active_menu, $group_menu) ? "active" : "");?> treeview">
                <a href="#">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Belanja</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="<?php echo (in_array($active_menu, $group_menu) ? "menu-open" : "");?> treeview-menu">
					<?php if(in_array("expense_operational", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "expense_operational" ? "active" : "");?>"><a href="<?php echo base_url("main/expense_operational");?>">Operasional</a></li>
					<?php endif ?>
					<?php if(in_array("expense_employees", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "expense_employees" ? "active" : "");?>"><a href="<?php echo base_url("main/expense_employees");?>">Pegawai</a></li>
					<?php endif ?>
                </ul>
            </li>
			<?php endif ?>
			<?php $group_menu = array("expense_outcome","salary","doctor_services","profit_loss","income");
			if(array_intersect($group_menu, $menu_enabled)) : ?>
            <li class="<?php echo (in_array($active_menu, $group_menu) ? "active" : "");?> treeview">
                <a href="#">
                <i class="fa fa-list-alt" aria-hidden="true"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="<?php echo (in_array($active_menu, $group_menu) ? "menu-open" : "");?> treeview-menu">
					<?php if(in_array("income", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "income" ? "active" : "");?>"><a href="<?php echo base_url("report/income");?>">Penerimaan</a></li>
					<?php endif ?>
					<?php if(in_array("expense_outcome", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "expense_outcome" ? "active" : "");?>"><a href="<?php echo base_url("report/expense_outcome");?>">Pengeluaran</a></li>
					<?php endif ?>
					<?php if(in_array("salary", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "salary" ? "active" : "");?>"><a href="<?php echo base_url("report/salary");?>">Gaji Pegawai</a></li>
					<?php endif ?>
					<?php if(in_array("doctor_services", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "doctor_services" ? "active" : "");?>"><a href="<?php echo base_url("report/doctor_services");?>">Jasa Dokter</a></li>
					<?php endif ?>
					<?php if(in_array("profit_loss", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "profit_loss" ? "active" : "");?>"><a href="<?php echo base_url("report/profit_loss");?>">Laba Rugi</a></li>
					<?php endif ?>
                </ul>
            </li>
			<?php endif ?>
			<?php $group_menu = array("users","patients","doctors","examinations","employees","transaction_report","subexaminations");
			if(array_intersect($group_menu, $menu_enabled)) : ?>
            <li class="treeview <?php echo (in_array($active_menu, $group_menu) ? "active" : "");?>">
                <a href="#">
                <i class="fa fa-database" aria-hidden="true"></i> <span>Data Master</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu <?php echo (in_array($active_menu, $group_menu) ? "menu-open" : "");?>">
					<?php if(in_array("users", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "users" ? "active" : "");?>">
						<a href="<?php echo base_url("main/users");?>">User</a>
					</li>
					<?php endif ?>
					<?php if(in_array("patients", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "patients" ? "active" : "");?>">
						<a href="<?php echo base_url("main/patients");?>">Pasien</a>
					</li>
					<?php endif ?>
					<?php if(in_array("doctors", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "doctors" ? "active" : "");?>">
						<a href="<?php echo base_url("main/doctors");?>">Dokter</a>
					</li>
					<?php endif ?>
					<?php if(in_array("examinations", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "examinations" ? "active" : "");?>">
						<a href="<?php echo base_url("main/examinations");?>">Pemeriksaan</a>
					</li>
					<?php endif ?>
					<?php if(in_array("employees", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "employees" ? "active" : "");?>">
						<a href="<?php echo base_url("main/employees");?>">Pegawai</a>
					</li>
					<?php endif ?>
					<?php if(in_array("transaction_report", $menu_enabled)) : ?>
                    <li class="<?php echo ($active_menu == "transaction_report" ? "active" : "");?>">
						<a href="<?php echo base_url("report/transaction");?>">Transaksi</a>
					</li>
					<?php endif ?>
                </ul>
            </li>
			<?php endif ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>