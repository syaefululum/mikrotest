$(function() {
	$('#datablesExpenseOutcome').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlExpenseOutcome').val(),
		"columnDefs" : [
		{
			"targets" : [0],
			"bSearchable" : false
		},			
		{
			"targets" : [0],
			"visible" : false
		}],
		"columns" : [			
			{data : "id"},
			{data : "date"},
			{data : "name"},
			{data : "value"}
		],
		"order" : [[2, "desc"], [1, "asc"]]
    });	
	
	
    //Date picker
    $('.datepicker-input').datepicker({
		autoclose: true,
		format: "dd-mm-yyyy",
    });	
});
