<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		} else {
			$this->user = $this->ion_auth->user()->row();
			$this->data['message_success'] = $this->session->flashdata('message_success');
			$this->data['message'] = $this->session->flashdata('message');
			$this->menu_enabled = $this->get_enabled_menu();
		}
		date_default_timezone_set("Asia/Jakarta");
	}
	
	public function expense_outcome()
	{
		$reject_user = !$this->is_has_access("expense_outcome");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		
		$this->data['date_start_value'] = $this->input->post('date_start');
		$this->data['date_end_value'] = $this->input->post('date_end');
		
		$this->data['date_start'] = array('name' => 'date_start', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_start', 
											'placeholder' => 'Dari', 
											'value' => $this->input->post('date_start'));
		$this->data['date_end'] = array('name' => 'date_end', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_end', 
											'placeholder' => 'Sampai', 
											'value' => $this->input->post('date_end'));
											
		$this->data['hidden_date_start'] = array('name' => 'date_start', 
											'type' => 'hidden', 
											'class' => '', 
											'field-name' => '', 
											'placeholder' => 'Dari', 
											'value' => $this->input->post('date_start'));
		$this->data['hidden_date_end'] = array('name' => 'date_end', 
											'type' => 'hidden', 
											'class' => '', 
											'field-name' => '', 
											'placeholder' => 'Sampai', 
											'value' => $this->input->post('date_end'));
												
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "plugins/moment/moment.min.js";
		$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
		$this->data['scripts'][] = "scripts/report/expenseOutcome.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		$this->data['styles'][] = "plugins/datepicker/datepicker3.css";		
		
		$this->data['active_menu'] = "expense_outcome";
        $this->view("report/v_expense_outcome", $this->data);
	}
	
	public function expense_outcome_report_print($display_menu = 0){
		$reject_user = !$this->is_has_access("expense_outcome");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model('m_transaction');
		
		$date_end = $this->input->post('date_end');
		$date_start = $this->input->post('date_start');
		
		
		$this->data['display_menu'] = $display_menu;
		$this->data['report'] = $this->m_transaction->get_report_expense_outcome($date_start,$date_end);
		$count_data = count($this->data['report']);
		if(!$date_start){
			if($count_data > 0){
				$date_start = date("Y-m-d", strtotime($this->data['report'][$count_data-1]->date));
			}
			
		}
		
		if(!$date_end){
			if($count_data > 0){
				$date_end = date("Y-m-d", strtotime($this->data['report'][0]->date));
			}
		}
		$this->data['end_date'] = $date_end;
		$this->data['start_date'] = $date_start;
		$this->load->view("report/v_expense_outcome_report_print", $this->data);
	}
	
	public function salary()
	{
		$reject_user = !$this->is_has_access("salary");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		
		$this->data['date_start_value'] = $this->input->post('date_start');
		$this->data['date_end_value'] = $this->input->post('date_end');
		
		$this->data['date_start'] = array('name' => 'date_start', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_start', 
											'placeholder' => 'Dari', 
											'value' => $this->input->post('date_start'));
		$this->data['date_end'] = array('name' => 'date_end', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_end', 
											'placeholder' => 'Sampai', 
											'value' => $this->input->post('date_end'));
											
		$this->data['hidden_date_start'] = array('name' => 'date_start', 
											'type' => 'hidden', 
											'class' => '', 
											'field-name' => '', 
											'placeholder' => 'Dari', 
											'value' => $this->input->post('date_start'));
		$this->data['hidden_date_end'] = array('name' => 'date_end', 
											'type' => 'hidden', 
											'class' => '', 
											'field-name' => 'date_end', 
											'placeholder' => 'Sampai', 
											'value' => $this->input->post('date_end'));
												
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "plugins/moment/moment.min.js";
		$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
		$this->data['scripts'][] = "scripts/report/salary.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		$this->data['styles'][] = "plugins/datepicker/datepicker3.css";		
		
		$this->data['active_menu'] = "salary";
        $this->view("report/v_salary", $this->data);
	}
	
	public function salary_report_print($display_menu = 0){
		$reject_user = !$this->is_has_access("salary");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model('m_transaction');
		
		$date_end = $this->input->post('date_end');
		$date_start = $this->input->post('date_start');
		

		$this->data["display_menu"] = $display_menu;
		
		$this->data['report'] = $this->m_transaction->get_report_salary($date_start,$date_end);

		$count_data = count($this->data['report']);
		if(!$date_start){
			if($count_data > 0){
				$date_start = date("d-m-Y", strtotime($this->data['report'][$count_data-1]->date));
			}
			
		}
		
		if(!$date_end){
			if($count_data > 0){
				$date_end = date("d-m-Y", strtotime($this->data['report'][0]->date));
			}
		}
		
		$this->data['end_date'] = $date_end;
		$this->data['start_date'] = $date_start;
		
		$this->load->view("report/v_salary_report_print", $this->data);
	}
	
	public function salary_print($id=0)
	{
		$reject_user = !$this->is_has_access("salary");
		$this->load->model("m_salary");
		$this->load->model("m_employee");
		
		$salary = $this->m_salary->get_one($id);
		
		if(!$salary){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->data["display_menu"] = 0;
		
		$this->data['salary'] = $salary;
		$this->data['employee'] = $this->m_employee->get_one($salary->employee_id);
		// echo '<pre>';
		// print_r($this->data['salary']);
		// print_r($this->data['employee']);
		// echo '</pre>';
		// $this->data['scripts'][] = "scripts/report/print.js";
		
		// $this->data['active_menu'] = "salary";
		// $this->data['content_only'] = true;
        $this->load->view("report/v_salary_print", $this->data);
	}
	
	public function transaction()
	{
		$reject_user = !$this->is_has_access("transaction_report");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		
		$this->data['date_start_value'] = $this->input->post('date_start');
		$this->data['date_end_value'] = $this->input->post('date_end');
		
		$this->data['date_start'] = array('name' => 'date_start', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_start', 
											'placeholder' => 'Dari', 
											'value' => $this->input->post('date_start'));
		$this->data['date_end'] = array('name' => 'date_end', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_end', 
											'placeholder' => 'Sampai', 
											'value' => $this->input->post('date_end'));
												
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "plugins/moment/moment.min.js";
		$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
		$this->data['scripts'][] = "scripts/report/transaction.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		$this->data['styles'][] = "plugins/datepicker/datepicker3.css";		
		
		$this->data['active_menu'] = "transaction_report";
        $this->view("report/v_transaction", $this->data);
	}
	
	public function income()
	{
		$reject_user = !$this->is_has_access("income");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		
		$this->data['date_start_value'] = $this->input->post('date_start');
		$this->data['date_end_value'] = $this->input->post('date_end');
		
		$this->data['date_start'] = array('name' => 'date_start', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_start', 
											'placeholder' => 'Dari', 
											'value' => $this->input->post('date_start'));
		$this->data['date_end'] = array('name' => 'date_end', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_end', 
											'placeholder' => 'Sampai', 
											'value' => $this->input->post('date_end'));
											
		$this->data['hidden_date_start'] = array('name' => 'date_start', 
											'type' => 'hidden', 
											'class' => '', 
											'field-name' => 'date_start',
											'value' => $this->input->post('date_start'));
		$this->data['hidden_date_end'] = array('name' => 'date_end', 
											'type' => 'hidden', 
											'class' => '', 
											'field-name' => 'date_end',
											'value' => $this->input->post('date_end'));
												
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "plugins/moment/moment.min.js";
		$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
		$this->data['scripts'][] = "scripts/report/income.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		$this->data['styles'][] = "plugins/datepicker/datepicker3.css";		
		
		$this->data['active_menu'] = "income";
        $this->view("report/v_income", $this->data);
	}
	
	public function income_report_print($display_menu = 0){
		$reject_user = !$this->is_has_access("income");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model('m_transaction');
		
		$date_end = $this->input->post('date_end');
		$date_start = $this->input->post('date_start');
		
		
		$this->data["patient"] = false;
		$this->data["doctor"] = false;
		$this->data["examination_result"] = array();
		$this->data["examination"] = array();
		$this->data["transaction"] = false;
		$this->data["examination_by_category"] = array();
		$this->data["user"] = $this->user;
		$this->data["display_menu"] = $display_menu;
		
		$this->data['report'] = $this->m_transaction->get_report_income($date_start,$date_end);
		$count_data = count($this->data['report']);
		if(!$date_start){
			if($count_data > 0){
				$date_start = date("Y-m-d", strtotime($this->data['report'][$count_data-1]->date));
			}
			
		}
		
		if(!$date_end){
			if($count_data > 0){
				$date_end = date("Y-m-d", strtotime($this->data['report'][0]->date));
			}
		}
		
		$this->data['end_date'] = $date_end;
		$this->data['start_date'] = $date_start;
		
		$this->load->view("report/v_income_report_print", $this->data);
	}
	
	public function income_detail($id=0)
	{
		$reject_user = !$this->is_has_access("income");
		
		$this->load->model("m_transaction");
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		$this->load->model("m_examination_result");
		
		$transaction = $this->m_transaction->get_one($id);
		$result = $this->m_examination_result->get_where(array("transaction_id" => $id), 0);
		
		if(!$transaction || !$result){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->data["patient"] = false;
		$this->data["doctor"] = false;
		$this->data["examination_result"] = array();
		$this->data["examination"] = array();
		$this->data["transaction"] = false;
		$this->data["examination_by_category"] = array();
		
		if($transaction){
			$this->load->model("m_patient");
			$this->load->model("m_doctor");
			$this->data["patient"] = $this->m_patient->get_one($transaction->patient_id);
			$this->data["doctor"] = $this->m_doctor->get_one($transaction->doctor_id);
			$this->data["examination_result"] = $result;
			$this->data["transaction"] = $transaction;
		}
		
		if($result){
			$examination_ids = array();
			$examination_result = array();
			foreach($result as $rslt){
				$examination_ids[] = $rslt->examination_id;
				$examination_result[$rslt->examination_id] = $rslt;
			}
			
			$condition = "id in (".implode(',', $examination_ids).")";
			$examination_data = $this->m_examination->get_where($condition, 0);
			
			$this->data["examination"] = array();
			$category_ids = array();
			$this->data["examination_category"] = array();
			foreach($examination_data as $data){
				$this->data["examination"][$data->id] = $data;
				$category_ids[] = $data->examination_category_id;
				if(!isset($this->data["examination_category"][$data->examination_category_id])){
					$this->data["examination_category"][$data->examination_category_id] = new StdClass;
				}
				if(!isset($this->data["examination_category"][$data->examination_category_id]->examination)){
					$this->data["examination_category"][$data->examination_category_id]->examination = array();
				}
				
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id] = $data;
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id]->examination_result = $examination_result[$data->id];
			}
			
			$condition = "id in (".implode(',', $category_ids).")";
			$examination_category_data = $this->m_examination_category->get_where($condition, 0);
			
			foreach($examination_category_data as $data){
				$examination = $this->data["examination_category"][$data->id]->examination;
				$this->data["examination_category"][$data->id] = $data;
				$this->data["examination_category"][$data->id]->examination = $examination;
			}
			
			$this->data["examination_by_category"] = $examination_category_data;
			
		}
		
		$this->data['active_menu'] = "transaction";
		$this->data['message_success'] = $this->session->flashdata('message_success');
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->data['active_menu'] = "income";
		$this->data['content_only'] = true;
		$this->data['scripts'][] = "scripts/report/print.js";
        $this->view("report/v_income_detail", $this->data);
	}
	
	public function income_print($id=0)
	{
		$reject_user = !$this->is_has_access("income");
		
		$this->load->model("m_transaction");
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		$this->load->model("m_examination_result");
		
		$transaction = $this->m_transaction->get_one($id);
		$result = $this->m_examination_result->get_where(array("transaction_id" => $id), 0);
		
		if(!$transaction || !$result){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->data["patient"] = false;
		$this->data["doctor"] = false;
		$this->data["examination_result"] = array();
		$this->data["examination"] = array();
		$this->data["transaction"] = false;
		$this->data["examination_by_category"] = array();
		
		if($transaction){
			$this->load->model("m_patient");
			$this->load->model("m_doctor");
			$this->data["patient"] = $this->m_patient->get_one($transaction->patient_id);
			$this->data["doctor"] = $this->m_doctor->get_one($transaction->doctor_id);
			$this->data["examination_result"] = $result;
			$this->data["transaction"] = $transaction;
		}
		
		if($result){
			$examination_ids = array();
			$examination_result = array();
			foreach($result as $rslt){
				$examination_ids[] = $rslt->examination_id;
				$examination_result[$rslt->examination_id] = $rslt;
			}
			
			$condition = "id in (".implode(',', $examination_ids).")";
			$examination_data = $this->m_examination->get_where($condition, 0);
			
			$this->data["examination"] = array();
			$category_ids = array();
			$this->data["examination_category"] = array();
			foreach($examination_data as $data){
				$this->data["examination"][$data->id] = $data;
				$category_ids[] = $data->examination_category_id;
				if(!isset($this->data["examination_category"][$data->examination_category_id])){
					$this->data["examination_category"][$data->examination_category_id] = new StdClass;
				}
				if(!isset($this->data["examination_category"][$data->examination_category_id]->examination)){
					$this->data["examination_category"][$data->examination_category_id]->examination = array();
				}
				
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id] = $data;
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id]->examination_result = $examination_result[$data->id];
			}
			
			$condition = "id in (".implode(',', $category_ids).")";
			$examination_category_data = $this->m_examination_category->get_where($condition, 0);
			
			foreach($examination_category_data as $data){
				$examination = $this->data["examination_category"][$data->id]->examination;
				$this->data["examination_category"][$data->id] = $data;
				$this->data["examination_category"][$data->id]->examination = $examination;
			}
			
			$this->data["examination_by_category"] = $examination_category_data;
			
		}
		
		$this->data['active_menu'] = "transaction";
		$this->data['message_success'] = $this->session->flashdata('message_success');
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->data['active_menu'] = "income";
		$this->data['content_only'] = true;
		$this->data['scripts'][] = "scripts/report/print.js";
        $this->view("report/v_income_print", $this->data);
	}
	
	public function doctor_services()
	{
		$reject_user = !$this->is_has_access("doctor_services");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		
		$this->data['date_start_value'] = $this->input->post('date_start');
		$this->data['date_end_value'] = $this->input->post('date_end');
		
		$this->data['date_start'] = array('name' => 'date_start', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_start', 
											'placeholder' => 'Dari', 
											'value' => $this->input->post('date_start'));
		$this->data['date_end'] = array('name' => 'date_end', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_end', 
											'placeholder' => 'Sampai', 
											'value' => $this->input->post('date_end'));
		
		$this->data['hidden_date_start'] = array('name' => 'date_start', 
											'type' => 'hidden', 
											'class' => '', 
											'field-name' => 'date_start', 
											'placeholder' => 'Dari', 
											'value' => $this->input->post('date_start'));
		$this->data['hidden_date_end'] = array('name' => 'date_end', 
											'type' => 'hidden', 
											'class' => '', 
											'field-name' => 'date_end', 
											'placeholder' => 'Sampai', 
											'value' => $this->input->post('date_end'));
												
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "plugins/moment/moment.min.js";
		$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
		$this->data['scripts'][] = "scripts/report/doctorServices.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		$this->data['styles'][] = "plugins/datepicker/datepicker3.css";		
		
		$this->data['active_menu'] = "doctor_services";
        $this->view("report/v_doctor_services", $this->data);
	}
	
	public function doctor_services_report_print($display_menu = 0){
		$reject_user = !$this->is_has_access("doctor_services");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model('m_transaction');
		$this->load->model('m_examination_result');
		$this->load->model('m_examination');
		
		$date_end = $this->input->post('date_end');
		$date_start = $this->input->post('date_start');
		
		
		$this->data['display_menu'] = $display_menu;

		$this->data['report'] = $this->m_transaction->get_report_docter_service($date_start,$date_end);
		if($this->data['report'] != null){
			foreach($this->data['report'] as $row){
				$result = $this->m_examination_result->get_where(array("transaction_id" => $row->id), 0);
				foreach($result as $rslt){
					$examination_ids[] = $rslt->examination_id;
				}
				$condition = "id in (".implode(',', $examination_ids).")";
				$examination_data = $this->m_examination->get_where($condition, 0);
				$row->examination_data = $examination_data;
			}
		}
		$count_data = count($this->data['report']);
		if(!$date_start){
			if($count_data > 0){
				$date_start = date("Y-m-d", strtotime($this->data['report'][$count_data-1]->date));
			}
			
		}
		
		if(!$date_end){
			if($count_data > 0){
				$date_end = date("Y-m-d", strtotime($this->data['report'][0]->date));
			}
		}
		
		$this->data['end_date'] = $date_end;
		$this->data['start_date'] = $date_start;
		
		$this->load->view("report/v_doctor_services_report_print", $this->data);
	}
	
	public function profit_loss()
	{
		$reject_user = !$this->is_has_access("profit_loss");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_transaction");
		$this->load->model("m_salary");
		$this->load->model("m_expense");
		$condition = array();
		
		$this->data['date_start_value'] = $this->input->post('date_start');
		$this->data['date_end_value'] = $this->input->post('date_end');
		
		if($this->data['date_start_value']){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($this->data['date_start_value']));
		}
		
		if($this->data['date_end_value']){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($this->data['date_end_value']));
		}
		
		$income = $this->m_transaction->get_income($condition);
		$outcome["expense"] = $this->m_expense->get_outcome($condition);
		$outcome["salary"] = $this->m_salary->get_outcome($condition);
		
		$this->data['profit_loss']["income"] = $income->value;
		$this->data['profit_loss']["outcome"]["expense"] = $outcome["expense"]->value;
		$this->data['profit_loss']["outcome"]["salary"] = $outcome["salary"]->salary;
		$this->data['profit_loss']["total"]= $income->value - ($outcome["expense"]->value + $outcome["salary"]->salary);
		
		$this->data['date_start'] = array('name' => 'date_start', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_start', 
											'placeholder' => 'Dari', 
											'value' => $this->input->post('date_start'));
		$this->data['date_end'] = array('name' => 'date_end', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_end', 
											'placeholder' => 'Sampai', 
											'value' => $this->input->post('date_end'));
											
		$this->data['hidden_date_start'] = array('name' => 'date_start', 
											'type' => 'hidden', 
											'class' => '', 
											'field-name' => 'date_start',
											'value' => $this->input->post('date_start'));
		$this->data['hidden_date_end'] = array('name' => 'date_end', 
											'type' => 'hidden', 
											'class' => '', 
											'field-name' => 'date_end',
											'value' => $this->input->post('date_end'));
												
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "plugins/moment/moment.min.js";
		$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
		$this->data['scripts'][] = "scripts/report/profitLoss.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		$this->data['styles'][] = "plugins/datepicker/datepicker3.css";	
		
		/*
		$this->load->model("m_transaction");
		$this->load->model("m_salary");
		$this->load->model("m_expense");
		$condition = array();
		
		if($this->data['date_start_value']){
			$condition["date >="] = date("Y-m-d", strtotime($this->data['date_start_value']));
		}
		
		if($this->data['date_end_value']){
			$condition["date <="] = date("Y-m-d", strtotime($this->data['date_end_value']));
		}
		
		$income = $this->m_transaction->get_income($condition);
		$outcome["expense"] = $this->m_expense->get_outcome($condition);
		$outcome["salary"] = $this->m_salary->get_outcome($condition);
		$this->data['profit_loss']["income"] = $income->value;
		$this->data['profit_loss']["outcome"]["expense"] = $outcome["expense"]->value;
		$this->data['profit_loss']["outcome"]["salary"] = $outcome["salary"]->salary;
		$this->data['profit_loss']["total"]= $income->value - ($outcome["expense"]->value + $outcome["salary"]->salary);
		
		$this->data['scripts'][] = "plugins/moment/moment.min.js";
		$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
		// $this->data['scripts'][] = "scripts/report/print.js";
		$this->data['scripts'][] = "scripts/report/profitLoss.js";
		$this->data['styles'][] = "plugins/datepicker/datepicker3.css";		
		*/
		$this->data['active_menu'] = "profit_loss";
        $this->view("report/v_profit_loss", $this->data);
	}
	
	public function profitloss_report_print($display_menu = 0){
		$reject_user = !$this->is_has_access("profit_loss");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model('m_transaction');
		
		$date_end = $this->input->post('date_end');
		$date_start = $this->input->post('date_start');
		
		
		$this->data['display_menu'] = $display_menu;
		$this->data['report_outcome'] = $this->m_transaction->get_report_outcome($date_start,$date_end);
		$this->data['report_income'] = $this->m_transaction->get_report_income($date_start,$date_end);
		
		$count_data = count($this->data['report_outcome']);
		$date_start_outcome = "";
		if(!$date_start){
			if($count_data > 0){
				$date_start_outcome = date("Y-m-d", strtotime($this->data['report_outcome'][$count_data-1]->date));
			}
			
		}
		$date_end_outcome = "";
		if(!$date_end){
			if($count_data > 0){
				$date_end_outcome = date("Y-m-d", strtotime($this->data['report_outcome'][0]->date));
			}
		}
		
		$count_data = count($this->data['report_income']);
		$date_start_income = "";
		if(!$date_start){
			if($count_data > 0){
				$date_start_income = date("Y-m-d", strtotime($this->data['report_income'][$count_data-1]->date));
				if(strtotime($date_start_outcome) < strtotime($date_start_income)){
					$date_start = $date_start_outcome;
				}else{
					$date_start = $date_start_income;
				}
			}
			
		}
		$date_end_income = "";
		if(!$date_end){
			if($count_data > 0){
				$date_end_income = date("Y-m-d", strtotime($this->data['report_income'][0]->date));
				if(strtotime($date_end_outcome) > strtotime($date_end_income)){
					$date_end = $date_end_outcome;
				}else{
					$date_end = $date_end_income;
				}
			}
		}
		
		$this->data['end_date'] = date("d-m-Y", strtotime($date_end));
		$this->data['start_date'] = date("d-m-Y", strtotime($date_start));
		
		$this->load->view("report/v_profitloss_report_print", $this->data);
	}
	
}
