<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	protected $user = null;
	protected $menu_enabled = array();
	
    function __construct(){
        parent::__construct();
		$this->load->helper(array('url','language'));
		$this->data['path_asset'] = base_url() . $this->config->item('path_asset');
    }
	
	function view($view){	
		if(!isset($this->data['content_only'])) $this->data['content_only'] = false;
		
		$base_dir = "";
		$this->data['controller'] = $this; 
		$this->data['user'] = $this->user;
		$this->data['menu_enabled'] = $this->menu_enabled;
		$this->data['page_head'] = $this->load->view($base_dir.'layout/v_head', $this->data, TRUE);
		$this->data['page_header'] = $this->load->view($base_dir.'layout/v_header', $this->data, TRUE);
		$this->data['page_content'] = $this->load->view($view, $this->data, TRUE);
		$this->data['page_footer'] = $this->load->view($base_dir.'layout/v_footer', $this->data, TRUE);
		$this->data['page_scripts'] = $this->load->view($base_dir.'layout/v_scripts', $this->data, TRUE);
		
		$this->load->view($base_dir.'/layout/v_base', $this->data);
	}
	
	function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key   = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }
	function encrypt_password($password){
		$options = [
			'cost' => 11,
		];
		$hash = password_hash($password, PASSWORD_BCRYPT, $options);
		return $hash;
	}
	
	function get_enabled_menu(){
		if($this->ion_auth->in_group("superadmin")){
			return explode(",", $this->config->item("menu_superadmin"));
		} else if($this->ion_auth->in_group("manager")){
			return explode(",", $this->config->item("menu_manager"));
		} else if($this->ion_auth->in_group("operator")){
			return explode(",", $this->config->item("menu_operator"));
		} 
	}
	
	function is_has_access($menu){
		return in_array($menu, $this->menu_enabled);
	}
}