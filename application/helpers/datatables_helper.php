<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * datatables_helper.php
 * Author: Firman Sugiharja
 * Date: 28/09/2015
 * Time: 11:04 AM
 */

//date format
function datatables_date_format($format, $date){
	return date($format, strtotime($date));
}

function get_number_format($number){
	return number_format($number);
}

function remove_useless_zero_desimal($number){
	return preg_replace('/([0-9]+(\.[0-9]+[1-9])?)(\.?0+$)/','$1', $number);
}

function get_actions($id, $main_function, $current_user_id, $rel)
{
	$actions_string = "";
	$actions_string .= "<div class='btn-group'>";
	$actions_string .= "<a href='" . base_url($main_function.'_edit/'.$id) . "'  class='btn btn-default'><i class='fa fa-pencil'></i> Edit</a>";
	if($current_user_id != $id){
		$actions_string .= "<a href='" . base_url($main_function.'_delete/'.$id) . "' class='btn btn-danger deleteNow' ";	
		$actions_string .= "rel='".$rel."'><i class='fa fa-trash-o'></i> Delete</a>";	
	}
	$actions_string .= "</div>";
	return $actions_string;
}

function get_actions_income($id, $main_function)
{
	$actions_string = "";
	$actions_string .= "<div class='btn-group'>";
	$actions_string .= "<a href='" . base_url('main/transaction_struk_print/'.$id) . "'  class='btn btn-default'><i class='fa fa-print '></i> Print</a>";
	$actions_string .= "<a href='" . base_url('main/examination_data_print/'.$id) . "'  class='btn btn-default'><i class='fa fa-list-alt '></i> Detail</a>";
	$actions_string .= "</div>";
	return $actions_string;
}

function get_actions_salary($id, $main_function)
{
	$actions_string = "";
	$actions_string .= "<div class='btn-group'>";
	//$actions_string .= "<a href='" . base_url('main/transaction_struk_print/'.$id) . "'  class='btn btn-default'><i class='fa fa-print '></i> Print</a>";
	$actions_string .= "<a href='" . base_url('report/salary_print/'.$id) . "'  class='btn btn-default'><i class='fa fa-print '></i> Print</a>";
	$actions_string .= "</div>";
	return $actions_string;
}

function get_gender($gender){
	$gender_string = "Perempuan";
	if(strtolower($gender) == "l" || strtolower($gender) == "m" ){
		$gender_string = "Laki-laki";
	}
	
	return $gender_string;
}

function get_dob($dob){
	return ($dob ? date("d/m/Y", strtotime($dob)) : "");
}

function get_actions_transaction($id, $result)
{
	$actions_string = "";
	$actions_string .= "<div class='btn-group'>";
	if($result == "empty"){
		$actions_string .= "<a href='" . base_url('main/examination_result/'.$id) . "' class='btn btn-default'><i class='fa fa-exchange '></i> Input Result</a>";
	} else {
		$actions_string .= "<a href='" . base_url('main/transaction_struk_print/'.$id) . "'  class='btn btn-default'><i class='fa fa-print '></i> Struk</a>";
		$actions_string .= "<a href='" . base_url('main/examination_data_print/'.$id) . "'  class='btn btn-default'><i class='fa fa-print '></i> Hasil</a>";
	}
	$actions_string .= "</div>";
	return $actions_string;
}

function set_examinations_hyperlink($id,$category,$type){
	if($type == 1){
		return $category;
	}else{
		return "<a href='".base_url('main/subexaminations	/'.$id)."'>".$category."</a>";
	}
	
}