$(function() {
	$('#datablesPatiens').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlPatients').val(),
		"columnDefs" : [
		{
			"targets" : [0,6],
			"bSearchable" : false
		},		
		{
			"targets" : [6],
			"bSortable" : false,
			"class" : 'center-tr'
		},		
		{
			"targets" : [0],
			"visible" : false
		}],
		"columns" : [			
			{data : "id"},
			{data : "name"},
			{data : "address"},
			{data : "phone"},
			{data : "gender"},
			{data : "dob"},
			{
				width: "200px", 
				data : "actions"
			},
		],
		"order"          : [[1, "asc"]]
    });	
});
