$(function() {
	$('#datablesSubExaminations').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlSubExaminations').val(),
		"columnDefs" : [
		{
			"targets" : [0,4],
			"bSearchable" : false
		},		
		{
			"targets" : [4],
			"bSortable" : false,
			"class" : 'center-tr'
		}],
		"columns" : [			
			{data : "id"},
			{data : "name"},
			{data : "unit"},
			{data : "normal_value"},
			{
				width: "200px", 
				data : "actions"
			},
		],
		"order"          : [[0, "asc"]]
    });	
});
