<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pengeluaran
        <small>Operasional</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pengeluaran</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">            
			<div class="result">
			<?php
				if (! empty($message_success)) {
					echo '<div class="alert alert-success" role="alert">';
					echo $message_success;
					echo '</div>';
				}
				if (! empty($message)) {
					echo '<div class="alert alert-danger" role="alert">';
					echo $message;
					echo '</div>';
				}
				?>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Pengeluaran</h3>
					<div class="pull-right">
						<a href="<?php echo base_url("main/expense_operational_create");?>" class="btn btn-primary">Add Pengeluaran</a>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				  <table id="datablesExpenseOutcome" class="table table-bordered table-striped">
					<thead>
					<tr>
						<td>ID</td>
						<td>TANGGAL</td>
						<td>URAIAN</td>
						<td>NILAI (Rp)</td>
						<td style="width:85px !important">TINDAKAN</td>
					</tr>
					</thead>
					<tbody></tbody>
				  </table>
				</div>
				<input hidden id="dataProcessUrlExpenseOutcome" value="<?php echo base_url("datatables_commhub/get_expense_operational");?>"/>
				<!-- /.box-body -->
			</div>
		</div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->