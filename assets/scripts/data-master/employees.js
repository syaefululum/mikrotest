$(function() {
	$('#datablesEmployees').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlEmployees').val(),
		"columnDefs" : [
		{
			"targets" : [0,2],
			"bSearchable" : false
		},		
		{
			"targets" : [2],
			"bSortable" : false,
			"class" : 'center-tr'
		},		
		{
			"targets" : [0],
			"visible" : false
		}],
		"columns" : [			
			{data : "id"},
			{data : "name"},
			{
				width: "200px", 
				data : "actions"
			},
		],
		"order"          : [[1, "asc"]]
    });	
});
