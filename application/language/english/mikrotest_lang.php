<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// error page
$lang['error_unauthorized'] = 'The user is unauthorized to access the requested resource';

//error label
$lang['error_label'] = 'Error';
$lang['error_label_page'] = 'Error Page';
$lang['error_label_404'] = '404';
$lang['error_label_message'] = 'Oops! Page not found.';
$lang['error_label_message_return'] = 'Return To Home';

// Button
$lang['ds_submit_login'] = 'Login';
$lang['ds_submit_save'] = 'Save';
$lang['ds_submit_save_exit'] = 'Save & Exit';
$lang['ds_submit_cancel'] = 'Cancel';
$lang['ds_btn_logout'] = 'Logout';
$lang['ds_btn_back'] = 'Back';
$lang['ds_btn_reset'] = 'Reset';
$lang['ds_btn_delete'] = 'Delete';
$lang['ds_btn_filter'] = 'Filter';
$lang['ds_btn_print'] = 'Print';

