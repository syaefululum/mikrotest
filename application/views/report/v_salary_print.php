<!DOCTYPE html>
<html lang="en">
    <head>
	
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
       
        <title>Mikrotest</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style_struk.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
		<script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<script src="<?php echo base_url('assets/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
    </head>

    <body>
		<nav class="navbar navbar-default navbar-custom">
			<div class="container-fluid">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php if($display_menu) : ?>
					<ul class="nav navbar-nav">
						<li><a class="btn btn-header" href="<?php echo $_SERVER['HTTP_REFERER'];?>">Kembali</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><button onclick="printDiv('printableArea')" class="btn btn-header print" type="button" style="margin-right: 20px;">Print</button></li>
						<li><a class="btn btn-header" href="<?php echo base_url("main/transaction");?>">Selesai</a></li>
					</ul>
					<?php else : ?>
					<ul class="nav navbar-nav navbar-right">
						<li><button onclick="printDiv('printableArea')" class="btn btn-header print" type="button" style="margin-right: 20px;">Print</button></li>
						<li><a class="btn btn-header" href="<?php echo $_SERVER['HTTP_REFERER'];?>">Kembali</a></li>
					</ul>
					<?php endif ?>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<div id="printableArea" class="container font-11 border-note">
			<div class="full-width margin-bottom-20" style="display:table;padding:0 15px;">
				
				<div class="col-xs-4" style="display:table;">
					<div class="row">
						<img src="<?php echo base_url("assets/img/Logo1final.png");?>" style="width:70%;margin:auto;display:table;">
						<p class="text-center">Jl. Raya Soreang No. 9 Telp. 022-5893029,  Soreang Kabupaten Bandung </p>
					</div>
				</div>
				<div class="col-xs-8" style="display:table;">
					<div class="row">
						<h4 class="uppercase text-right">Slip Gaji Pegawai</h4>
						<table class="table-border pull-right">
							<tr>
								<td class="border-right-hide" style="width:150px;">Tanggal</td>
								<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
								<td class="border-left-hide" style="width:220px;"><?php echo date("d-m-Y", strtotime($salary->date));?></td>
							</tr>
							<tr>
								<td class="border-right-hide">Nama</td>
								<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
								<td class="border-left-hide"><?php echo $employee->name;?></td>
							</tr>
							<tr>
								<td class="border-right-hide">No Telp</td>
								<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
								<td class="border-left-hide"><?php echo $employee->phone;?></td>
							</tr>
							<tr>
								<td class="border-right-hide">Alamat</td>
								<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
								<td class="border-left-hide"><?php echo $employee->address;?></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<table class="full-width table-border table-space">
					<thead>
					<tr>
						<td style="width:100px;" class="table-header">Rincian</td>
						<td class="table-header" style="width:180px;">Besaran (Rp)</td>
					</tr>
					</thead>
					<tr>
						<td>Gaji Pokok</td>
						<td class="text-right"><?php echo number_format($salary->basic_salary);?></td>
					</tr>
					<tr>
						<td>Insentif</td>
						<td class="text-right"><?php echo number_format($salary->incentive);?></td>
					</tr>
					<tr>
						<td>Lembur</td>
						<td class="text-right"><?php echo number_format($salary->overtime);?></td>
					</tr>
					<tr>
						<td>Pembulatan</td>
						<td class="text-right"><?php echo number_format($salary->rounding_off);?></td>
					</tr>
					<tfoot>
					<tr style="font-size:14px;">
						<td>Total Gaji</td>
						<td class="text-right"><?php echo number_format($salary->salary);?></td>
					</tr>
					</tfoot>
				</table>
			</div>
			<div class="col-xs-3 pull-right">
				<p class="text-center padding-top-20">Soreang, <?php echo date("d/m/Y");?> <br>HRD,</p>
				<p class="text-center padding-top-60"></p>
			</div>
		</div>
	</body>
</html>
<script>
	function printDiv(divName) {
		
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
		initDatePicker();
	}
	initDatePicker();
	
	function initDatePicker(){
		//Date picker
		$(document).find('.datepicker-input').datepicker({
			autoclose: true,
			format: "yyyy-mm-dd",
		});	
	}


</script>

<!--
<div id="printableArea">
	<section class="content" style="min-height:100%">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Gaji Pegawai</h3>
					</div>
					<div class="box-body">
						<div class="form-horizontal">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-5 control-label" style="padding-top: 0px;">Nama</label>
									<div class="col-sm-7"><?php echo $employee->name;?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-5 control-label" style="padding-top: 0px;">Tanggal</label>
									<div class="col-sm-7"><?php echo $salary->date;?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-5 control-label" style="padding-top: 0px;">Gaji Pokok</label>
									<div class="col-sm-7"><?php echo number_format($salary->basic_salary);?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-5 control-label" style="padding-top: 0px;">Insentif</label>
									<div class="col-sm-7"><?php echo number_format($salary->incentive);?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-5 control-label" style="padding-top: 0px;">Lembur</label>
									<div class="col-sm-7"><?php echo number_format($salary->overtime);?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-5 control-label" style="padding-top: 0px;">Pembulatan</label>
									<div class="col-sm-7"><?php echo number_format($salary->rounding_off);?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-5 control-label" style="padding-top: 0px;">Total Gaji</label>
									<div class="col-sm-7"><?php echo number_format($salary->salary);?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-5 control-label" style="padding-top: 0px;">Informasi</label>
									<div class="col-sm-7"><?php echo $salary->information;?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row print-hide">
			<div class='col-xs-12'>
				<button class="btn btn-info pull-right print" type="button" style="margin-right: 0px;">Print</button>
			</div>
		</div>
	</section>
</div>
-->