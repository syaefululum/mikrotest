<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_users extends MY_Model {
	protected $table = 'users';
	
	function get_group_by_id($user_id){
		$this->db->select("*");
		$this->db->from('users_groups');// I use aliasing make joins easier
		$this->db->where("user_id", $user_id);
        $query = $this->db->get();
		return  $query->row();
	}
	
	function validate_user( $username, $password ) {
		
		$return_data = new stdClass();
		$return_data->status = false;
		$return_data->message = false;
		
		$condition = array(
			"username" => $username,
			// "password" => $password,
		);
		$this->db->where($condition);
		$result = $this->db->get($this->table)->row();
		if($result){
			if (password_verify($password, $result->password)) {
				$return_data->status = true;
				$return_data->message = 'Valid user';
			} else {
				$return_data->message = 'Invalid password.';
			}
			$return_data->user = $result;
		}else {
			$return_data->message = "Username not registered";
		}
		return $return_data;
	}
}

