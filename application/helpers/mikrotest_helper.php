<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * mikrotest_helper.php
 * Author: Firman Sugiharja
 */

function _get_gender_dropdown(){
	return array(
		"L" => "Laki-laki",
		"P" => "Perempuan",
	);
}

function numeric_wcomma ($str){
	return preg_match('/^[0-9,]+$/', $str);
}