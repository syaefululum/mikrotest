<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Master
        <small>Sub Pemeriksaan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Master</li>
        <li class="active">Pemeriksaan</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">            
			<div class="result">
			<?php
				if (! empty($message_success)) {
					echo '<div class="alert alert-success" role="alert">';
					echo $message_success;
					echo '</div>';
				}
				if (! empty($message)) {
					echo '<div class="alert alert-danger" role="alert">';
					echo $message;
					echo '</div>';
				}
				?>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Sub Pemeriksaan</h3>
					<div class="pull-right">
						<a href="<?php echo base_url("main/subexaminations_create/".$examinations_data->id);?>" class="btn btn-primary">Add Sub Pemeriksaan</a>
					</div>
				</div>
				<div class="box-header">
					<span>Jenis Pemeriksaan : <?php echo $examinations_data->name?></span>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				  <table id="datablesSubExaminations" class="table table-bordered table-striped">
					<thead>
					<tr>
						<td>ID</td>
						<td>URAIAN</td>
						<td>SATUAN</td>
						<td>NORMAL VALUE</td>
						<td style="width:85px !important">TINDAKAN</td>
					</tr>
					</thead>
					<tbody></tbody>
				  </table>
				</div>
				<input hidden id="dataProcessUrlSubExaminations" value="<?php echo base_url("datatables_commhub/get_subexaminations/".$examinations_data->id);?>"/>
				<!-- /.box-body -->
			</div>
		</div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->