<div id="printableArea">
	<section class="content" style="background:#ecf0f5">
		<div class="row">
			<div class="col-md-12">
				<div class="result">
					<?php
					if (! empty($message_success)) {
						echo '<div class="alert alert-success" role="alert">';
						echo $message_success;
						echo '</div>';
					}
					if (! empty($message)) {
						echo '<div class="alert alert-danger" role="alert">';
						echo $message;
						echo '</div>';
					}
					?>
				</div>
			</div>
			<div class='col-xs-6'>  
				<div class="box box-solid">
					<div class="box-body">
						<?php echo date("D, d/m/Y, h:i A", strtotime($transaction->date));?>
					</div>
				</div>
			</div>
			<div class='col-xs-6'>  
				<div class="box box-solid">
					<div class="box-body text-right">
						ID Transaksi: <?php echo $transaction->id;?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Identitas Pasien</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<!-- form start -->
						<div class="form-horizontal">
							<div class="box-body">
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label" style="padding-top: 0px;">Nama</label>
									<div class="col-sm-10 patient-address"><?php echo $patient->name;?></div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label" style="padding-top: 0px;">Alamat</label>
									<div class="col-sm-10 patient-address"><?php echo $patient->address;?></div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label" style="padding-top: 0px;">No. HP</label>
									<div class="col-sm-10 patient-phone"><?php echo $patient->phone;?></div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- ./col -->
			<div class="col-xs-6">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Identitas Dokter</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body clearfix">
						<!-- form start -->
						<div class="form-horizontal">
							<div class="box-body">
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label" style="padding-top: 0px;">Nama</label>
									<div class="col-sm-10 doctor-address"><?php echo $doctor->name;?></div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label" style="padding-top: 0px;">Alamat</label>
									<div class="col-sm-10 doctor-address"><?php echo $doctor->address;?></div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label" style="padding-top: 0px;">No. HP</label>
									<div class="col-sm-10 doctor-phone"><?php echo $doctor->phone;?></div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- ./col -->
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid">
				<?php foreach($examination_by_category as $ebc) : ?>
					<div class="box-header">
						<h3 class="box-title"><?php echo ucwords($ebc->name);?></h3>
					</div>
					<div class="box-body table-responsive clearfix " style="padding-top: 0px;">
						<table class="table table-hover table-bordered">
							<tr>
							  <th width="30%">Nama</th>
							  <th width="30%">Hasil</th>
							  <th width="25%">Nilai Normal</th>
							  <th width="15%">Satuan</th>
							</tr>
							<?php 
							$examinations = $ebc->examination;
							foreach($examinations as $examination) : ?>
							<tr>
							  <td><?php echo $examination->name;?></td>
							  <td><?php echo $examination->examination_result->result_value;?></td>
							  <td><?php echo $examination->normal_value;?></td>
							  <td><?php echo $examination->unit;?></td>
							</tr>
							<?php endforeach ?>
						</table>
					</div>
				<?php endforeach ?>
					<div class="box-header with-border">
						<h3 class="box-title">Keterangan Tambahan</h3>
					</div>
					<div class="box-body clearfix">
						<div class="form-horizontal">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-2 control-label" style="padding-top: 0px;">Informasi</label>
									<div class="col-sm-10">
										<?php echo (isset($transaction->information) ? $transaction->information : "");?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.col -->
		</div>
		<div class="row print-hide">
			<div class='col-md-12'>
				<button class="btn btn-info pull-right print" type="button" style="margin-right: 10px;">Print</button>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>