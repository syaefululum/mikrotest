<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_expense extends MY_Model {
	protected $table = 'expense';
	
	public function get_outcome($condition){
        $this->db->select("SUM(value) as value");
        $this->db->where($condition);
		return $this->get($this->table)->row();
	}
}

