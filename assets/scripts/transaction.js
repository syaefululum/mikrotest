$(function() {	
	$(".combodate-input").combodate({
		maxYear: new Date().getFullYear(),
		minYear: 1920,
	});
	
	$("span.combodate").children("select").addClass("form-control");
	
	$('#datetimepickerTransaction').datetimepicker({
		"format" : "ddd, DD/MM/YYYY, h:mm A",
        // "defaultDate": moment()
	});
	
	//iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue'
    });
	
	$('#patientDropdown').dropdown({
		allowAdditions: false,
		onChange: function(value, text, $choice){
			if($choice){								
				$(".patient-dob").text($choice.data("dob"));
				$(".patient-address").text($choice.data("address"));
				$(".patient-phone").text($choice.data("phone"));
				$(".patient-gender").text($choice.data("gender"));
			} else {								
				$(".patient-dob").text("");
				$(".patient-address").text("");
				$(".patient-phone").text("");
				$(".patient-gender").text("");
			}
		}
	});
	
	$('#doctorDropdown').dropdown({
		allowAdditions: false,
		onChange: function(value, text, $choice){
			if($choice){					
				$(".doctor-address").text($choice.data("address"));
				// $(".doctor-phone").text($choice.data("phone"));
			} else {						
				$(".doctor-address").text("");
				// $(".doctor-phone").text("");
			}
		}
	});
	
	$("#discount").keypress(function (e) {
		inputValidation(e, "number");
	});
	
	$('#discount')
	.bind("cut copy paste",function(e) {
        e.preventDefault();
    });
	
	$('#changePatient').on("change",function(e) {
		if($(this).val() == "existing"){
			$("#existingPatient").removeClass("hidden");
			$("#newPatient").addClass("hidden");
		} else {
			$("#existingPatient").addClass("hidden");
			$("#newPatient").removeClass("hidden");
			$('#patientDropdown').dropdown("restore defaults");
		}
    });
	
	$('#changeDoctor').on("change",function(e) {
		if($(this).val() == "existing"){
			$("#existingDoctor").removeClass("hidden");
			$("#newDoctor").addClass("hidden");
		} else {
			$("#existingDoctor").addClass("hidden");
			$("#newDoctor").removeClass("hidden");
			$('#doctorDropdown').dropdown("restore defaults");
		}
    });
	
	$('#changePatient').trigger("change");
	$('#changeDoctor').trigger("change");
	
	if($("#alertMessage").val() != ""){
		customAlert($("#alertMessage").val());
	}
});
