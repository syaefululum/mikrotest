<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_examination_category extends MY_Model {
	protected $table = 'examination_category';
	
	function get_category_dropdown(){
		$this->db->select("*");
		$this->db->from($this->table);
        $query = $this->db->get();
		
		$dropdown = array();
		
		if($query->result()){
			foreach($query->result() as $group){
				$dropdown[$group->id] = ucwords($group->name);
			}
		}
		
		return $dropdown;
	}
	
}

