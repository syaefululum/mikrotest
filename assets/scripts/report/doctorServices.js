$(function() {
	$('#datablesDoctorServices').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlDoctorServices').val(),
		"columnDefs" : [
		{
			"targets" : [0],
			"bSearchable" : false
		},			
		{
			"targets" : [0],
			"visible" : true
		}],
		"columns" : [
			{data : "transaction_no"},
			{data : "doctor"},
			{data : "date"},
			{data : "total"},
			{data : "percent_doctor_services"},
			{data : "doctor_services"}
		],
		"order"          : [[2, "desc"], [1, "asc"]]
    });	
	
	
    //Date picker
    $('.datepicker-input').datepicker({
		autoclose: true,
		format: "dd-mm-yyyy",
    });	
});
