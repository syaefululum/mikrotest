$(function() {
    //Date picker
    $('.datepicker-input').datepicker({
		autoclose: true,
		format: "dd-mm-yyyy",
    });	
	
	function updateSalary(){
		var basicSalary = parseInt($('#basic_salary').val());
		var incentive = parseInt($('#incentive').val());
		var overtime = parseInt($('#overtime').val());
		var roundingOff = parseInt($('#rounding_off').val());
		
		if(!$.isNumeric(basicSalary)){
			basicSalary = 0;
		}
		
		if(!$.isNumeric(incentive)){
			incentive = 0;
		}
		
		if(!$.isNumeric(overtime)){
			overtime = 0;
		}
		
		if(!$.isNumeric(roundingOff)){
			roundingOff = 0;
		}
		
		console.log('basic_salary' ,basicSalary);
		console.log('incentive' ,incentive);
		console.log('overtime' ,overtime);
		console.log('rounding_off' ,roundingOff);
		
		console.log('total ', basicSalary + incentive + overtime + roundingOff);
		
		$('#salary').val(basicSalary + incentive + overtime + roundingOff);
	}
	
	$( ".salaryCalculation" ).keyup(function() {
	  updateSalary();
	});
	
	$('.numbersOnly').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
});
	
});


