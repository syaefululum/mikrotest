$(function() {
	$('#datablesDoctors').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlDoctors').val(),
		"columnDefs" : [
		{
			"targets" : [0,5],
			"bSearchable" : false
		},		
		{
			"targets" : [5],
			"bSortable" : false,
			"class" : 'center-tr'
		},
		{
			"targets" : [4],
			"class" : 'center-tr'
		},			
		{
			"targets" : [0],
			"visible" : false
		}],
		"columns" : [			
			{data : "id"},
			{data : "name"},
			{data : "address"},
			{data : "phone"},
			//{data : "gender"},
			{data : "service"},
			{
				width: "200px", 
				data : "actions"
			},
		],
		"order"          : [[1, "asc"]]
    });	
});
