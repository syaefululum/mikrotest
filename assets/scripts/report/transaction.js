$(function() {
	$('#datablesIncome').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlIncome').val(),
		"columnDefs" : [
		{
			"targets" : [0],
			"bSearchable" : false
		},		
		{
			"targets" : [6],
			"bSortable" : false,
			"bSearchable" : false,
			"class" : 'center-tr'
		},			
		{
			"targets" : [0],
			"visible" : true
		}],
		"columns" : [			
			{data : "transaction_no"},
			{data : "patient"},
			{data : "doctor"},
			{data : "date"},
			{data : "discount"},
			{data : "total"},
			{
				width: "200px", 
				data : "actions"
			},
		],
		"order"          : [[4, "desc"], [2, "asc"]]
    });	
	
	
    //Date picker
    $('.datepicker-input').datepicker({
		autoclose: true,
		format: "dd-mm-yyyy",
    });	
});
