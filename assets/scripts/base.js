function customAlert(text, callback, nocallback){
	var uniqueId = new Date().getTime();
	
	$('#alertContainerCustom').find('p').html(text); 
	$("#alertContainerCustom .btn-ok").css("width", "100%");
	$("#alertContainerCustom .btn-ok").addClass("btn-ok-alert-"+uniqueId);
	$("#alertContainerCustom").modal("show");
	
	$('#alertContainerCustom').on('hidden.bs.modal', function () {
		$('#alertContainerCustom').find('p').html("alert text here"); 
		$("#alertContainerCustom .btn-ok").removeClass("btn-ok-alert-"+uniqueId);
		if(nocallback != null) nocallback();
	})
	
	$('#alertContainerCustom').on('click', '.btn-ok-alert-'+uniqueId, function() {
		if(callback != null) callback();
	});
}

function customConfirm(text, callback, nocallback){
	var uniqueId = new Date().getTime();
	$('#alertContainerCustom').find('p').html(text); 
    $("#alertContainerCustom .btn-cancel").removeClass("hidden");
	$("#alertContainerCustom .btn-cancel").addClass("btn-cancel-confirm-"+uniqueId);
	$("#alertContainerCustom .btn-ok").css("width", "49%");
	$("#alertContainerCustom .btn-ok").addClass("btn-ok-confirm-"+uniqueId);
	
	$("#alertContainerCustom").modal("show");

	$('#alertContainerCustom').on('click', '.btn-ok-confirm-'+uniqueId, function() {
		if(callback != null) callback();
	});
	
	$('#alertContainerCustom').on('click', '.btn-cancel-confirm-'+uniqueId, function() {
		if(nocallback != null) nocallback();
	});
    
	$('#alertContainerCustom').on('hidden.bs.modal', function () {
		$('#alertContainerCustom').find('p').html("alert text here"); 
		$("#alertContainerCustom .btn-cancel").addClass("hidden");
		$("#alertContainerCustom .btn-cancel").removeClass("btn-cancel-confirm-"+uniqueId);
		$("#alertContainerCustom .btn-ok").removeClass("btn-ok-confirm-"+uniqueId);
	})
}

function centeredModalDoalog(){
	function centerModal() {
        $(this).css('display', 'block');
        var $dialog  = $(this).find(".modal-dialog"),
        offset       = ($(window).height() - $dialog.height()) / 2,
        bottomMargin = parseInt($dialog.css('marginBottom'), 10);

        if(offset < bottomMargin) offset = bottomMargin;
        $dialog.css("margin-top", offset);
    }

    $(document).on('show.bs.modal', '.modal', centerModal);
    $(window).on("resize", function () {
        $('.modal:visible').each(centerModal);
    });
}

function initLogout(){
	var LOGOUT_MESSAGE = "Are you sure you want to sign out?";
	var logoutUrl = $('.logout-button').data('logout-url');
	$(".logout-button").on("click", function (e) {
		function confirmOk(){
			window.location = logoutUrl;
			return true;
			// return orig_onclick.call(this, e.originalEvent);
		}
		customConfirm(LOGOUT_MESSAGE, confirmOk);
	});
}

function inputValidation(evt, text) {
	var regex = /[\w\d||]/g;
	switch (text) {
		case "nickname":
		var regex = /[\w\d]/g;
		break;
		case "name":
		var regex = /[\w\d ]/g;
		break;
		case "email":
		var regex = /[\w\d||_||\-||@||.]/g;
		break;
		case "number":
		var regex = /[\d]/g;
		break;
		case "phone":
		var regex = /[\d||()]/g;
		break;
		case "question":
		var regex = /[\w\d||.||?]/g;
		break;
		case "text":
		var regex = /[\w\d ||.||?||!||,||&||(||)||\-||\/||_||=||+||"||'||@]/g;
		break;
	}
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	var key = String.fromCharCode(charCode);
	if (!evt.which) {
		//left or right 
		if (charCode == 37 || charCode == 39) return true;
	}
	//delete || // tab  ||//delete
	if (charCode == 8 || charCode == 9 || charCode == 13 || regex.test(key)) {
		return true;
        } else {
		evt.preventDefault();
	}
}

function isValidUrl(url)
{
     return url.match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/);
}

function onReady(callback) {
    var intervalID = window.setInterval(checkReady, 1000);
    function checkReady() {
        if (document.getElementsByTagName('body')[0] !== undefined) {
            window.clearInterval(intervalID);
            callback.call(this);
        }
    }
}
function show(id, value, timeOut) {
	if(value){
		$(id).show();
	} else {
		var delay = 0;
		if(timeOut){
			delay = timeOut;
		}
		setTimeout(function() {
			//your code to be executed after 1 second
			$(id).fadeOut(1000);
		}, delay);
	}
    // document.getElementById(id).style.display = value ? 'block' : 'none';
}

onReady(function () {
    show('#page', true);
    show('#loading', false);
    show('.alert', false, 5000);
	$.AdminLTE.layout.activate();
	// $.AdminLTE.layout.fix();
	
});

$(function() {
	centeredModalDoalog();
	initLogout();
});

$(document).ajaxStop(function () {
	var orig_onclick = "";
	$('.deleteNow').on('click', function (e) {
		orig_onclick = $(this).prop('href');
		console.log(orig_onclick);
		function confirmOk(){
			console.log(orig_onclick);
			window.location = orig_onclick;
			return false;
		}
		customConfirm("Are you sure you want to delete this " + $(this).attr('rel') + " ?", confirmOk);
		
		return false;
	});
});
