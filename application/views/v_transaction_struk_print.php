<!DOCTYPE html>
<html lang="en">
    <head>
	
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
       
        <title>Mikrotest</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style_struk.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
		<script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<script src="<?php echo base_url('assets/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
    </head>

    <body>
		<nav class="navbar navbar-default navbar-custom">
			<div class="container-fluid">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php if($display_menu) : ?>
					<ul class="nav navbar-nav">
						<li><a class="btn btn-header" href="<?php echo base_url("main/transaction_delete/".$transaction->id);?>">Kembali</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><button onclick="printDiv('printableArea')" class="btn btn-header print" type="button" style="margin-right: 20px;">Print</button></li>
						<li><a class="btn btn-header" href="<?php echo base_url("main/transaction");?>">Selesai</a></li>
					</ul>
					<?php else : ?>
					<ul class="nav navbar-nav navbar-right">
						<li><button onclick="printDiv('printableArea')" class="btn btn-header print" type="button" style="margin-right: 20px;">Print</button></li>
						<li><a class="btn btn-header" href="<?php echo $_SERVER['HTTP_REFERER'];?>">Kembali</a></li>
					</ul>
					<?php endif ?>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<div id="printableArea" class="container font-11 border-note" >
		<div style="font-size:10px !important;">
			<div class="full-width margin-bottom-20" style="display:table;padding:0 15px;">
				<h4 class="uppercase text-right">nota pembayaran</h4>
				<div class="col-xs-4" style="display:table;">
					<div class="row">
						<img src="<?php echo base_url("assets/img/Logo1final.png");?>" style="width:70%;margin:auto;display:table;">
						<p class="text-center">Jl. Raya Soreang No. 9 Telp. 022-5893029,  Soreang Kabupaten Bandung </p>
					</div>
				</div>
				<div class="col-xs-8" style="display:table;">
				<div class="row">
					<table class="table-border pull-right">
						<tr>
							<td class="border-right-hide" style="width:100px;">Nomor</td>
							<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
							<td class="border-left-hide" style="width:220px;"><?php echo $transaction->transaction_no;?></td>
						</tr>
						<tr>
							<td class="border-right-hide">Nama Pasien</td>
							<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
							<td class="border-left-hide"><?php echo $patient->name;?></td>
						</tr>
						<tr>
							<?php
								$age = false;
								if($patient->dob){
									$age_diff = date_diff(date_create(date("Y-m-d", strtotime($patient->dob))), 
										date_create(date("Y-m-d", strtotime($transaction->date))));
									$age = $age_diff->format("%y Tahun %m Bulan %d Hari");
								}
							?>
							<td class="border-right-hide">Umur</td>
							<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
							<td class="border-left-hide"><?php echo $age;?></td>
						</tr>
						<tr>
							<td class="border-right-hide">Jenis Kelamin</td>
							<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
							<td class="border-left-hide"><?php echo (strtolower($patient->gender) == "l" ? "Laki-laki" : (strtolower($patient->gender) == "p" ? "Perempuan" : ""));?></td>
						</tr>
					</table>
				</div>
				</div>
			</div>
			<div class="col-sm-12">
				<table class="full-width table-border table-space">
					<tr>
						<td class="table-header" style="width:40px;">No</td>
						<td class="table-header">PEMERIKSAAN</td>
						<td class="table-header" style="width:200px;">HARGA (Rp)</td>
					</tr>
					<?php 
					$index = 1;
					foreach($examination as $exmntn): ?>
					<tr>
						<td class="text-center"><?php echo ($index);?></td>
						<td><?php echo $exmntn->name;?></td>
						<td class="text-right"><?php echo number_format($exmntn->examination_result->fare_applied);?></td>
					</tr>
					<?php $index++;
					endforeach ?>
					<tr>
						<td></td>
						<td class="text-right">Diskon</td>
						<td class="text-right"><?php echo number_format($transaction->discount);?></td>
					</tr>
					<tr>
						<td></td>
						<td class="text-right">Harus Bayar</td>
						<td class="text-right"><?php echo number_format($transaction->total);?></td>
					</tr>
					<tr>
						<td></td>
						<td class="text-right">Uang Muka</td>
						<td class="text-right"></td>
					</tr>
					<tr>
						<td></td>
						<td class="text-right">Sisa Pembayaran</td>
						<td class="text-right"></td>
					</tr>
				</table>
			</div>
			<div class="col-xs-3 pull-right">
				<p class="text-center padding-top-20">Soreang, <?php echo date("d/m/Y");?> <br>Penerima,</p>
				<p class="text-center padding-top-60" style="margin-bottom:0px;">Nama Operator</p>
				<p class="text-center"><?php echo $user->first_name." ".$user->last_name;?></p>
			</div>
			<div class="col-xs-12">
				<small class="pull-right text-right"><i><b>Catatan:</b><br> Nota Pembayaran ini harap dibawa/ diperlihatkan ketika mengambil kasil laboratorium </i></small>
			</div>
		</div>
		</div>
	</body>
</html>
<script>
	function printDiv(divName) {
		
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
		initDatePicker();
	}
	initDatePicker();
	
	function initDatePicker(){
		//Date picker
		$(document).find('.datepicker-input').datepicker({
			autoclose: true,
			format: "yyyy-mm-dd",
		});	
	}


</script>