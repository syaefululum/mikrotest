$(function() {
	$('#datablesUsers').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlUsers').val(),
		"columnDefs"     : [
		{
			"targets" : [0,4],
			"bSearchable" : false
		},		
		{
			"targets" : [4],
			"bSortable" : false,
			"class" : 'center-tr'
		},		
		{
			"targets" : [0],
			"visible" : false
		}],
		"columns" : [			
			{data : "id"},
			{data : "username"},
			{data : "fullname"},
			{data : "role"},
			{
				width: "200px", 
				data : "actions"
			},
		],
		"order"          : [[1, "asc"]]
    });	
});
