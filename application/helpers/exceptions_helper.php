<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('show_401'))
{
    function show_401($page = '', $log_error = TRUE)
    {
        $_error =& load_class('Exceptions', 'core');
        $_error->show_401($page, $log_error);
        exit;
    }
}