<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Master
        <small><?php echo $submenu;?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Master</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <?php echo form_open_multipart(base_url(uri_string()), array('class' => 'form-horizontal form-ajax'));?>
		<div class="col-lg-12">
			<div class="result">
				<?php
				if (! empty($message_success)) {
					echo '<div class="alert alert-success" role="alert">';
					echo $message_success;
					echo '</div>';
				}
				if (! empty($message)) {
					echo '<div class="alert alert-danger" role="alert">';
					echo $message;
					echo '</div>';
				}
				?>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group <?php echo (form_error('username') ? 'has-error' : '')?>">
										<label for="username" class="col-sm-2 control-label">Username</label>
										<div class="col-sm-10">
											<?php echo form_input($username); ?>
											<span class="help-block"><?php echo form_error('username'); ?></span>
										</div>
									</div>
									<?php if($is_edit) : ?>
									<div class="form-group <?php echo (form_error('oldpassword') ? 'has-error' : '')?>">
										<label for="oldpassword" class="col-sm-2 control-label">Old Password</label>
										<div class="col-sm-10">
											<?php echo form_input($oldpassword); ?>
											<span class="help-block"><?php echo form_error('oldpassword'); ?></span>
										</div>
									</div>
									<?php endif ?>
									<div class="form-group <?php echo (form_error('password') ? 'has-error' : '')?>">
										<label for="password" class="col-sm-2 control-label">Password</label>
										<div class="col-sm-10">
											<?php echo form_input($password); ?>
											<span class="help-block"><?php echo form_error('password'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('passconf') ? 'has-error' : '')?>">
										<label for="password" class="col-sm-2 control-label">Confirm Password</label>
										<div class="col-sm-10">
											<?php echo form_input($passconf); ?>
											<span class="help-block"><?php echo form_error('passconf'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('first_name') ? 'has-error' : '')?>">
										<label for="name" class="col-sm-2 control-label">First Name</label>
										<div class="col-sm-10">
											<?php echo form_input($first_name); ?>
											<span class="help-block"><?php echo form_error('first_name'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('last_name') ? 'has-error' : '')?>">
										<label for="name" class="col-sm-2 control-label">Last Name</label>
										<div class="col-sm-10">
											<?php echo form_input($last_name); ?>
											<span class="help-block"><?php echo form_error('last_name'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('email') ? 'has-error' : '')?>">
										<label for="name" class="col-sm-2 control-label">Email</label>
										<div class="col-sm-10">
											<?php echo form_input($email); ?>
											<span class="help-block"><?php echo form_error('email'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('phone') ? 'has-error' : '')?>">
										<label for="name" class="col-sm-2 control-label">Phone</label>
										<div class="col-sm-10">
											<?php echo form_input($phone); ?>
											<span class="help-block"><?php echo form_error('phone'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('group_id') ? 'has-error' : '')?>">
										<label for="role" class="col-sm-2 control-label">Group</label>
										<div class="col-sm-10">
											<?php
											echo form_dropdown('group_id', $groups_list, $selected_group, 'field-name = "Group" class="form-control requiredDropdown" autocomplete="off"');
											?>
											<span class="help-block"><?php echo form_error('group_id'); ?></span>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<button type="submit" name="btnAction" value="save"
													class="btn btn-primary"><?php echo $this->lang->line('ds_submit_save'); ?>
											</button>
											<button type="submit" name="btnAction" value="save_exit"
													class="btn btn-primary">
												<?php echo $this->lang->line('ds_submit_save_exit'); ?>
											</button>
											<a href="<?php echo base_url('main/users'); ?>"
											   class="btn btn-default"><?php echo $this->lang->line('ds_submit_cancel'); ?></a>
										</div>
									</div>
								</div>
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->