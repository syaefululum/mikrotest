<style>
	.form-horizontal .form-group{
		margin-bottom: 0px;
	}
</style>
<input hidden id="alertMessage" value="<?php echo $message;?>" />
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Input Data Pasien
        <small class="hidden">Small Menu</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Input Data Pasien</li>
    </ol>
</section>
<!-- Main content -->
<?php echo form_open_multipart(base_url(uri_string()), array('class' => 'form-horizontal form-ajax'));?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
			<div class="result">
				<?php
				if (! empty($message_success)) {
					echo '<div class="alert alert-success" role="alert">';
					echo $message_success;
					echo '</div>';
				}
				if (! empty($message)) {
					echo '<div class="alert alert-danger" role="alert">';
					echo $message;
					echo '</div>';
				}
				$options_gender = array(
						'' => '',
						'0' => '',
						'L' => 'Laki-laki',
						'P' => 'Perempuan',
				);
				?>
			</div>
		</div>
		<div class='col-sm-6'>  
			<div class="box box-solid">
                <div class="box-body">
					<div class="form-groups">
						<div class='input-group date' id='datetimepickerTransaction'>
							<input type='text' class="form-control" name="date" value="<?php echo $date;?>" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
            </div>
        </div>
	</div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-solid transaction-identity-container">
                <div class="box-header with-border" style="padding:3px">
                    <h3 class="box-title" style="padding:8px">Identitas Pasien</h3>
					<div class="pull-right">
						<div class="form-group" style="margin:0px">
							<select name="patient_select" class="form-control select2" style="width: 150px;" id="changePatient">
							  <option <?php echo ($change_patient == 'existing' ? 'selected="selected"' : '');?> value="existing">Lama</option>
							  <option <?php echo ($change_patient == 'new' ? 'selected="selected"' : '');?> value="new">Baru</option>
							</select>
						</div>
					</div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- form start -->
                    <div class="form-horizontal" id="existingPatient">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-9">
									<div class="ui fluid search selection dropdown" id="patientDropdown">
									  <input type="hidden" name="patient" value="<?php echo $selected_patient;?>">
										<i class="dropdown icon"></i>
										<div class="default text">Select Pasien</div>
										<div class="menu">
										<?php foreach($patients as $patient) : ?>
											<div class="item" 
												data-value="<?php echo $patient->id;?>" 
												data-dob="<?php echo ($patient->dob ? date("d-m-Y", strtotime($patient->dob)) : "");?>" 
												data-address="<?php echo $patient->address;?>" 
												data-phone="<?php echo $patient->phone;?>"
												data-gender="<?php echo $options_gender[$patient->gender];?>">
												<?php echo $patient->name;?>
											</div>
										<?php endforeach ?>
										</div>
									</div>
                                </div>
                            </div>
                            <div class="form-group" style="padding:4px 0 4px">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">Tgl. Lahir</label>
                                <div class="col-sm-9 patient-dob"><?php echo $selected_patient_dob;?></div>
                            </div>
                            <div class="form-group" style="padding:4px 0">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">Alamat</label>
                                <div class="col-sm-9 patient-address"><?php echo $selected_patient_address;?></div>
                            </div>
                            <div class="form-group" style="padding:4px 0">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">No. Telp</label>
                                <div class="col-sm-9 patient-phone"><?php echo $selected_patient_phone;?></div>
                            </div>
                            <div class="form-group" style="padding:4px 0">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">Jenis Kelamin</label>
                                <div class="col-sm-9 patient-gender"><?php 			
									echo $options_gender[$selected_patient_gender];
								?></div>
                            </div>
                        </div>
                    </div>
                    <!-- form start -->
                    <div class="form-horizontal hidden" id="newPatient">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-9">									
									<?php echo form_input($new_patient_name); ?>
									<span class="help-block"><?php echo form_error('new_patient_name'); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 8px;">Tgl. Lahir</label>
                                <div class="col-sm-9">
									<?php echo form_input($new_patient_dob); ?>
									<span class="help-block"><?php echo form_error('new_patient_dob'); ?></span>
								</div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 8px;">Alamat</label>
                                <div class="col-sm-9">
									<?php echo form_input($new_patient_address); ?>
									<span class="help-block"><?php echo form_error('new_patient_address'); ?></span>
								</div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 8px;">No. Telp</label>
                                <div class="col-sm-9">
									<?php echo form_input($new_patient_phone); ?>
									<span class="help-block"><?php echo form_error('new_patient_phone'); ?></span>
								</div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 8px;">Jenis Kelamin</label>
                                <div class="col-sm-9">
									<?php 
										$options = array(
												'L' => 'Laki-laki',
												'P' => 'Perempuan',
										);
										$attr = array("class" => "form-control select2");
										echo form_dropdown('new_patient_gender', $options, $selected_patient_gender, $attr);
									?>
									<span class="help-block"><?php echo form_error('selected_patient_gender'); ?></span>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
        <div class="col-md-6">
            <div class="box box-solid transaction-identity-container">
                <div class="box-header with-border" style="padding:3px;">
                    <h3 class="box-title" style="padding:8px;">Identitas Dokter</h3>
					<div class="pull-right">
						<div class="form-group" style="margin:0px">
							<select name="doctor_select" class="form-control select2" style="width: 150px;" id="changeDoctor">
							  <option <?php echo ($change_doctor == 'existing' ? 'selected="selected"' : '');?> value="existing">Lama</option>
							  <option <?php echo ($change_doctor == 'new' ? 'selected="selected"' : '');?> value="new">Baru</option>
							</select>
						</div>
					</div>
                </div>
                <!-- /.box-header -->
                <div class="box-body clearfix">
                    <!-- form start -->
                    <div class="form-horizontal" id="existingDoctor">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-9">
                                    <div class="ui fluid search selection dropdown" id="doctorDropdown">
									  <input type="hidden" name="doctor" value="<?php echo $selected_doctor;?>">
										<i class="dropdown icon"></i>
										<div class="default text">Select Dokter</div>
										<div class="menu">
										<?php foreach($doctors as $doctor) : ?>
											<div class="item" 
												data-value="<?php echo $doctor->id;?>" 
												data-address="<?php echo $doctor->address;?>">
												<?php echo $doctor->name;?>
											</div>
										<?php endforeach ?>
										</div>
									</div>
                                </div>
                            </div>
                            <div class="form-group" style="padding:4px 0">
                                <label class="col-sm-3 control-label" style="padding-top: 0px;">Alamat</label>
                                <div class="col-sm-9 doctor-address"><?php echo $selected_doctor_address;?></div>
                            </div>
                        </div>
                    </div>
                    <!-- form start -->
                    <div class="form-horizontal hidden" id="newDoctor">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-9">									
									<?php echo form_input($new_doctor_name); ?>
									<span class="help-block"><?php echo form_error('new_doctor_name'); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top: 8px;">Alamat</label>
                                <div class="col-sm-9">
									<?php echo form_input($new_doctor_address); ?>
									<span class="help-block"><?php echo form_error('new_doctor_address'); ?></span>
								</div>
                            </div>
							<div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Jasa (%)</label>
                                <div class="col-sm-9">									
									<?php echo form_input($new_doctor_service); ?>
									<span class="help-block"><?php echo form_error('new_doctor_service'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom" style="padding-bottom: 10px;">
				<ul class="nav nav-tabs">
					<?php 
					$active = "active";
					foreach($examinations as $ex) : 
						echo '<li class="'.$active.'"><a href="#examinationCategory'.$ex->id.'" data-toggle="tab">'.ucwords($ex->name).'</a></li>';
						$active = "";
					endforeach
					?>
				</ul>
				<div class="tab-content examination-content">
					<?php $active = "active";
					foreach($examinations as $ex) : ?>
					<div class="tab-pane <?php echo $active;?>" id="examinationCategory<?php echo $ex->id;?>">
						<?php 
						$limit_first_column = ceil(count($ex->examination)/2);
						$ex_list = $ex->examination;
						for($i = 0; $i < count($ex_list);$i++) : 
							if($i == 0 || $i == ($limit_first_column)) {
								echo '<div class="col-md-6"><div class="form-group">';
							} ?>
							
							<label>
								<input type="checkbox" <?php echo (in_array($ex_list[$i]->id, $selected_examination) ? "checked" : "");?>
									name="examination[<?php echo $ex_list[$i]->id;?>]" class="minimal">
								<?php echo $ex_list[$i]->name;?>
							</label>
						<?php if($i == ($limit_first_column - 1) || $i == ( count($ex_list) - 1)) {
								echo '</div></div>';
							}
						?>
							
						<?php endfor ?>
					</div>
					<?php $active = "";
					endforeach ?>
				</div>
				<!-- /.tab-content -->
			</div>
			<!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
		<div class='col-md-12'>
			<div class="box box-solid">
                <!-- /.box-header -->
                <div class="box-body">
					<div class='col-md-6'>
						<div class="form-horizontal">
							<div class="box-body">
								<div class="form-group" style="margin-bottom: 0px;">
									<label class="col-sm-2 control-label">Discount</label>
									<div class="col-sm-10">
										<input value="<?php echo $discount;?>" type="text" class="form-control" id="discount" name="discount" placeholder="Tuliskan nominal potongan harga disini">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class='col-md-6'>
						<div class="form-group pull-right" style="margin-top: 13px;">
							<label>
								<input type="checkbox" <?php echo ($doctor_services ? "checked" : "");?> name="doctor_services" class="minimal">
								Jasa Dokter
							</label>
						</div>
					</div>
				</div>
			</div>
        </div>
        <div class='col-md-12'>
			<button class="btn btn-primary pull-right" type="submit">Bayar</button>
        </div>
	</div>
    <!-- /.row -->
</section>
<?php echo form_close(); ?>
<!-- /.content -->