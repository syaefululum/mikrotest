<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_employee extends MY_Model {
	protected $table = 'employee';
	
	function get_employee_dropdown(){
		$condition = array("status" => 1);
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where($condition);
        $query = $this->db->get();
		
		$dropdown = array();
		
		if($query->result()){
			foreach($query->result() as $emply){
				$dropdown[$emply->id] = $emply->name;
			}
		}
		
		return $dropdown;
	}
	
}

