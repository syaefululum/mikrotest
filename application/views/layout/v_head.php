<head>
    <meta charset="utf-8">
    <meta name="description" content="sequislife">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mikrotest</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/bootstrap/css/bootstrap.min.css"); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/font-awesome-4.5.0/css/font-awesome.min.css"); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/ionicons-2.0.1/ionicons.min.css"); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url("assets/css/AdminLTE.min.css"); ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url("assets/css/skins/_all-skins.min.css"); ?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo base_url("assets/css/styles.css"); ?>">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <?php if (!empty($styles)): foreach ($styles as $val): ?>
    <link href="<?php echo $path_asset . $val ?>" rel="stylesheet" type="text/css"/>
    <?php endforeach;
        endif ?>
    <!-- END PAGE LEVEL STYLES -->
    <!-- favicon -->
	<link rel="shortcut icon" href="<?php echo base_url("assets/img/Logo2final.png"); ?>" type="image/png"/>
	<link rel="icon" href="<?php echo base_url("assets/img/Logo2final.png"); ?>" type="image/png" />
	<!-- end favicon -->
</head>