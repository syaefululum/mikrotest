<!-- Modal -->
<div class="modal fade" id="alertContainerCustom" role="dialog">
    <div class="modal-dialog">
		<div class="modal-content" style="margin: 0 auto;width: 70%">
			<div class="modal-header hidden">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body" style="padding:10px; text-align:center">
				<p>[alert_text]</p>
				<div style="height: 40px;">
					<button class="btn btn-primary btn-ok pull-left" data-dismiss="modal" style="width: 100%">Ok</button>
					<button class="btn btn-danger btn-cancel pull-right hidden" data-dismiss="modal" style="width: 49%; margin :0 0 0 2%">Cancel</button>
				</div>
			</div>
			<div class="modal-footer hidden">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
		
	</div>
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2016.</strong> All rights
    reserved.
</footer>