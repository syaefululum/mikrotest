$(function() {
	$('#datablesProfitLossIncome').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlProfitLossIncome').val(),
		"columnDefs" : [
		{
			"targets" : [0],
			"bSearchable" : false
		},	
		{
			"targets" : [0],
			"bSortable" : false,
			"class" : 'center-tr'
		},			
		{
			"targets" : [0],
			"visible" : true
		}],
		"columns" : [
			{data : "date"},
			{data : "transaction_no"},
			{data : "patient"},
			{data : "doctor"},
			{data : "total"}
		],
		"order" : [[3, "desc"], [2, "asc"]]
    });	
	
	$('#datablesProfitLossExpenseOutcome').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlProfitLossExpenseOutcome').val(),
		"columnDefs" : [
		{
			"targets" : [0],
			"bSearchable" : false
		},	
		{
			"targets" : [0],
			"bSearchable" : false
		},			
		{
			"targets" : [0],
			"visible" : false
		}],
		"columns" : [		
			{data : "date"},
			{data : "date"},
			{data : "name"},
			{data : "value"}
		],
		"order" : [[2, "desc"], [1, "asc"]]
    });	
	
    //Date picker
    $('.datepicker-input').datepicker({
		autoclose: true,
		format: "dd-mm-yyyy",
    });	
});




$(function() {
	$(document).on("click", ".print", function(){
		printDiv('printableArea');
	});
	function printDiv(divName) {
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
		initDatePicker();
	}
	initDatePicker();
	
	function initDatePicker(){
		//Date picker
		$(document).find('.datepicker-input').datepicker({
			autoclose: true,
			format: "dd-mm-yyyy",
		});	
	}
});
