<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_table_employe extends CI_Migration {

	public function up()
	{
		$fields_update = array(
			'value' => array(
				'name' => 'salary',
				'type' => 'int',
				'default' => 0
			),
		);
		$this->dbforge->modify_column('salary', $fields_update);
		
		$fields_add = array(
			'basic_salary' => array(
				'type' => 'int',
				'constraint' => '11',
				'default' => 0
			),
			'incentive' => array(
				'type' => 'int',
				'constraint' => '11',
				'default' => 0
			),
			'overtime' => array(
				'type' => 'int',
				'constraint' => '11',
				'default' => 0
			),
			'rounding_off' => array(
				'type' => 'int',
				'constraint' => '11',
				'default' => 0
			)
		);
		
		$this->dbforge->add_column('salary', $fields_add);

	}

	public function down()
	{
		
	}
}
