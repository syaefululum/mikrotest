<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Laporan
        <small>Penerimaan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Laporan Penerimaan</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">            
			<div class="result">
			<?php
				if (! empty($message_success)) {
					echo '<div class="alert alert-success" role="alert">';
					echo $message_success;
					echo '</div>';
				}
				if (! empty($message)) {
					echo '<div class="alert alert-danger" role="alert">';
					echo $message;
					echo '</div>';
				}
				?>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Penerimaan</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<?php echo form_open_multipart(base_url(uri_string()), array('class' => 'form-horizontal form-ajax'));?>
						<div class="col-md-4">
							<div class="form-group">
								<label for="date_start" class="col-sm-2 control-label">Dari</label>
								<div class="col-sm-10">
									<?php echo form_input($date_start); ?>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="date_end" class="col-sm-2 control-label">Sampai</label>
								<div class="col-sm-10">
									<?php echo form_input($date_end); ?>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<button type="submit" name="btnAction" value="Filter"
									class="btn btn-primary"><?php echo $this->lang->line('ds_btn_filter'); ?>
							</button>
							<?php echo form_close(); ?>							
						</div>
						<div class="col-md-2 pull-right" style="text-align:right;">
						<?php echo form_open_multipart(base_url('report/income_report_print'), array('class' => ''));?>
							<button type="submit" name="btnAction" value="Print"
								class="btn btn-primary"><?php echo $this->lang->line('ds_btn_print'); ?>
							</button>
							<?php echo form_input($hidden_date_start); ?>
							<?php echo form_input($hidden_date_end); ?>
							<?php echo form_close(); ?>
						</div>
						
					</div>
					<table id="datablesIncome" class="table table-bordered table-striped">
						<thead>
						<tr>
							<td>TANGGAL</td>
							<td>ID TRANSAKSI</td>
							<td>PASIEN</td>
							<td>DOKTER</td>
							<td>DISKON</td>
							<td>TOTAL</td>
							<td style="width:85px !important">ACTION</td>
						</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<input hidden id="dataProcessUrlIncome" value="<?php echo base_url("datatables_commhub/get_income?date_start=".$date_start_value."&date_end=".$date_end_value);?>"/>
				<!-- /.box-body -->
			</div>
		</div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->