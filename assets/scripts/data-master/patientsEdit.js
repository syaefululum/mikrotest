$(function() {
	$(".combodate-input").combodate({
		maxYear: new Date().getFullYear(),
		minYear: 1920,
	});
	
	$("span.combodate").children("select").addClass("form-control");
	
});
