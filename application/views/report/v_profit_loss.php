<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Laporan
        <small>Laba Rugi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Laporan Laba Rugi</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12"> 
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Laba Rugi</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<?php echo form_open_multipart(base_url(uri_string()), array('class' => 'form-horizontal form-ajax'));?>
						<div class="col-md-4">
							<div class="form-group">
								<label for="date_start" class="col-sm-2 control-label">Dari</label>
								<div class="col-sm-10">
									<?php echo form_input($date_start); ?>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="date_end" class="col-sm-2 control-label">Sampai</label>
								<div class="col-sm-10">
									<?php echo form_input($date_end); ?>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<button type="submit" name="btnAction" value="Filter"
									class="btn btn-primary"><?php echo $this->lang->line('ds_btn_filter'); ?>
							</button>
						</div>
						<?php echo form_close(); ?>
						<div class="col-md-2 pull-right" style="text-align:right;">
						<?php echo form_open_multipart(base_url('report/profitloss_report_print'), array('class' => ''));?>
							<button type="submit" name="btnAction" value="Print"
								class="btn btn-primary"><?php echo $this->lang->line('ds_btn_print'); ?>
							</button>
							<?php echo form_input($hidden_date_start); ?>
							<?php echo form_input($hidden_date_end); ?>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<!-- ./col -->
	</div>
	<div class="row">
			<div class="col-xs-12">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Laba Rugi</h3>
					</div>
					<div class="box-body">
						<div class="form-horizontal">
							<div class="box-body">
								<div class="col-xs-6">
									<h3 class="box-title" style="text-align:center;">URAIAN PENERIMAAN</h3>
									<table id="datablesProfitLossIncome" class="table table-bordered table-striped">
										<thead>
											<tr>
												<td>TANGGAL</td>
												<td>ID TRANS</td>
												<td>PASIEN</td>
												<td>DOKTER</td>
												<td>TOTAL</td>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
									<input hidden id="dataProcessUrlProfitLossIncome" value="<?php echo base_url("datatables_commhub/get_income?date_start=".$date_start_value."&date_end=".$date_end_value);?>"/>
								</div>
								
								<div class="col-xs-6">
									<h3 class="box-title" style="text-align:center;">URAIAN PENGELUARAN</h3>
									<table id="datablesProfitLossExpenseOutcome" class="table table-bordered table-striped">
										<thead>
											<tr>
												<td>ID</td>
												<td>TANGGAL</td>
												<td>URAIAN PENGELUARAN</td>
												<td>HARGA</td>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
									<input hidden id="dataProcessUrlProfitLossExpenseOutcome" value="<?php echo base_url("datatables_commhub/get_view_outcome?date_start=".$date_start_value."&date_end=".$date_end_value);?>"/>
								</div>
							</div>
							<div class="box-body">
								<div class="col-xs-12">
									<table class="table table-bordered table-striped">
										<tr>
											<td colspan="2" style="text-align:center;">TOTAL</td>
											
										</tr>
										<tr>
											<td>TOTAL PENERIMAAN(Rp)</td>
											<td style="text-align:right;width:250p"><?php echo number_format($profit_loss["income"]);?></td>
											</tr>
										<tr>
											<td>TOTAL PENGELUARAN(Rp)</td>
											<td style="text-align:right;width:250p"><?php echo number_format($profit_loss["outcome"]["expense"] + $profit_loss["outcome"]["salary"]);?></td>
										</tr>
										<tr>
											<td>TOTAL(Rp)</td>
											<td style="text-align:right;width:250p"><?php echo number_format($profit_loss["total"]);?></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
	<div id="printableArea">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Laba Rugi
						<?php
							if($date_start_value || $date_end_value){
								echo " ( ";
							}
							if($date_start_value){
								echo " Dari : ".$date_start_value;
							}
							if($date_end_value){
								echo " Sampai : ".$date_end_value;
							}
							if($date_start_value || $date_end_value){
								echo " ) ";
							}
						?>
						</h3>
					</div>
					<div class="box-body">
						<div class="form-horizontal">
							<div class="box-body">
								<table class="table table-hover table-bordered">
									<tr>
									  <td width="50%" class="text-left text-bold">Income</td>
									  <td class="text-right">Rp. <?php echo number_format($profit_loss["income"]);?></td>
									</tr>
									<tr>
									  <td width="50%" class="text-left text-bold">Outcome</td>
									  <td class="text-right"></td>
									</tr>
									<tr>
									  <td width="50%" class="text-left" style="padding-left: 20px;">Operasional</td>
									  <td class="text-right">Rp. <?php echo number_format($profit_loss["outcome"]["expense"]);?></td>
									</tr>
									<tr>
									  <td width="50%" class="text-left" style="padding-left: 20px;">Gaji Pegawai</td>
									  <td class="text-right">Rp. <?php echo number_format($profit_loss["outcome"]["salary"]);?></td>
									</tr>
									<tr>
									  <td width="50%" class="text-left text-bold">Total</td>
									  <td class="text-right text-bold">Rp. <?php echo number_format($profit_loss["total"]);?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	-->
	<div class="row print-hide">
		<div class='col-xs-12'>
			<button class="btn btn-info pull-right print" type="button" style="margin-right: 0px;">Print</button>
		</div>
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->