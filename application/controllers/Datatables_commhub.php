<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Datatables_commhub extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		if ($this->ion_auth->logged_in())
		{
			$this->user = $this->ion_auth->user()->row();
		}
	}
	
	public function get_users()
	{
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
        $this->load->model("m_users_groups");
	
		$current_user_id = 0;
		$current_user_group_id = 0;
		if($this->user){
			$current_user_id = $this->user->id;
			$user_group = $this->m_users_groups->get_by($current_user_id, "user_id");
			if($user_group){
				$current_user_group_id = $user_group->group_id;
			}
		}
		
		$select = array(
			'users.id',
			'users.username',
			'concat(users.first_name, " ", users.last_name) as fullname',
			'groups.description as role',
		);
		
		$condition = array("users.active" => 1, "groups.id >=" => $current_user_group_id);
		
        $this->datatables->select(implode(",", $select))->from('users')
		->join('users_groups', 'users.id = users_groups.user_id', 'left')
		->join('groups', 'users_groups.group_id = groups.id', 'left')
		->where($condition)
		->add_column('actions', "$1", 'get_actions(id,"/main/users",'.$current_user_id.',"User")');
			
        echo $this->datatables->generate();
	}
	
	public function get_patients()
	{
		$current_user_id = 0;
		// if($this->user){
			// $current_user_id = $this->user->id;
		// }
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'id',
			'name',
			'address',
			'phone',
			'gender',
			'dob',
		);
		
		$condition = array("status" => 1);
		
        $this->datatables->select(implode(",", $select))->from('patient')
		->where($condition)
		->unset_column('gender')->add_column('gender', "$1", 'get_gender(gender)')
		->unset_column('dob')->add_column('dob', "$1", 'get_dob(dob)')
		->add_column('actions', "$1", 'get_actions(id,"/main/patients",'.$current_user_id.',"Pasien")');
			
        echo $this->datatables->generate();
	}
	
	public function get_doctors()
	{
		$current_user_id = 0;
		// if($this->user){
			// $current_user_id = $this->user->id;
		// }
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'id',
			'name',
			'address',
			'phone',
			'gender',
			'service'
		);
		
		$condition = array("status" => 1);
		
        $this->datatables->select(implode(",", $select))->from('doctor')
		->where($condition)
		->unset_column('gender')->add_column('gender', "$1", 'get_gender(gender)')
		->add_column('actions', "$1", 'get_actions(id,"/main/doctors",'.$current_user_id.',"Dokter")');
			
        echo $this->datatables->generate();
	}
	
	public function get_examinations()
	{
		$current_user_id = 0;
		// if($this->user){
			// $current_user_id = $this->user->id;
		// }
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'examination.id',
			'examination_category.name as category',
			'examination.name',
			'examination.fare',
			'examination.unit',
			'examination.normal_value',
			'examination.type',
		);
		
		$condition = array(
			"examination.status" => 1,
			"examination.parent_id" => 0
		);
		
        $this->datatables->select(implode(",", $select))->from('examination')
		->join('examination_category', 'examination.examination_category_id = examination_category.id', 'left')
		->where($condition)
		->unset_column('name')->add_column('name', "$1", 'set_examinations_hyperlink(id,name,type)')
		->unset_column('fare')->add_column('fare', "$1", 'get_number_format(fare)')
		->unset_column('type')
		->add_column('actions', "$1", 'get_actions(id,"/main/examinations",'.$current_user_id.',"Pemeriksaan")');
			
        echo $this->datatables->generate();
	}
	
	public function get_subexaminations($id = 0)
	{
		$current_user_id = 0;
		
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'examination.id',
			'examination.name',
			'examination.unit',
			'examination.normal_value',
		);
		
		$condition = array(
			"examination.status" => 1,
			"examination.parent_id" => $id
		);
		
        $this->datatables->select(implode(",", $select))->from('examination')
		->join('examination_category', 'examination.examination_category_id = examination_category.id', 'left')
		->where($condition)
		->add_column('actions', "$1", 'get_actions(id,"/main/subexaminations",'.$current_user_id.',"Pemeriksaan")');
			
        echo $this->datatables->generate();
	}
	
	public function get_employees()
	{
		$current_user_id = 0;
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'id',
			'name'
		);
		
		$condition = array("status" => 1);
		
        $this->datatables->select(implode(",", $select))->from('employee')
		->where($condition)
		->add_column('actions', "$1", 'get_actions(id,"/main/employees",'.$current_user_id.',"Pegawai")');
			
        echo $this->datatables->generate();
	}
	
	public function get_expense_employees()
	{
		$current_user_id = 0;
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'salary_e.id',
			'employee.name',
			'DATE_FORMAT(date,"%d-%m-%Y") as date',
			'basic_salary',
			'incentive',
			'overtime',
			'rounding_off',
			'salary',
			'information',
		);
		
		$condition = array(
			"employee.status" => 1,
			"salary_e.status" => 1,
		);
		
        $this->datatables->select(implode(",", $select))->from('salary salary_e')
		->join("employee", "salary_e.employee_id = employee.id", "inner")
		->where($condition)
		->unset_column('basic_salary')->add_column('basic_salary', "$1", 'get_number_format(basic_salary)')
		->unset_column('incentive')->add_column('incentive', "$1", 'get_number_format(incentive)')
		->unset_column('overtime')->add_column('overtime', "$1", 'get_number_format(overtime)')
		->unset_column('rounding_off')->add_column('rounding_off', "$1", 'get_number_format(rounding_off)')
		->unset_column('salary')->add_column('salary', "$1", 'get_number_format(salary)')
		->add_column('actions', "$1", 'get_actions(id,"/main/expense_employees",'.$current_user_id.',"Gaji Pegawai")');
			
        echo $this->datatables->generate();
	}
	
	public function get_salary()
	{
		$current_user_id = 0;
		$date_start = $this->input->get("date_start");
		$date_end = $this->input->get("date_end");
		
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'salary_e.id',
			'employee.name',
			'DATE_FORMAT(date,"%d-%m-%Y") as date',
			'basic_salary',
			'incentive',
			'overtime',
			'rounding_off',
			'salary',
			'information',
		);
		
		$condition = array(
			"employee.status" => 1,
			"salary_e.status" => 1,
		);
		
		if($date_start){
			$condition["DATE_FORMAT(salary_e.date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(salary_e.date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		
        $this->datatables->select(implode(",", $select))->from('salary salary_e')
		->join("employee", "salary_e.employee_id = employee.id", "inner")
		->where($condition)
		->unset_column('basic_salary')->add_column('basic_salary', "$1", 'get_number_format(basic_salary)')
		->unset_column('incentive')->add_column('incentive', "$1", 'get_number_format(incentive)')
		->unset_column('overtime')->add_column('salary', "$1", 'get_number_format(overtime)')
		->unset_column('rounding_off')->add_column('rounding_off', "$1", 'get_number_format(rounding_off)')
		->unset_column('salary')->add_column('salary', "$1", 'get_number_format(salary)')
		->add_column('actions', "$1", 'get_actions_salary(id,"/report/salary")');
			
        echo $this->datatables->generate();
	}
	
	public function get_expense_operational()
	{
		$current_user_id = 0;
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'id',
			'name',
			'DATE_FORMAT(date,"%d-%m-%Y") as date',
			'value',
		);
		
		$condition = array(
			"status" => 1,
		);
		
        $this->datatables->select(implode(",", $select))->from('expense')
		->where($condition)
		->unset_column('value')->add_column('value', "$1", 'get_number_format(value)')
		->add_column('actions', "$1", 'get_actions(id,"/main/expense_operational",'.$current_user_id.',"Pengeluaran Operasional")');
			
        echo $this->datatables->generate();
	}
	
	public function get_expense_outcome($date_start=0, $date_end=0)
	{
		$current_user_id = 0;
		$date_start = $this->input->get("date_start");
		$date_end = $this->input->get("date_end");
		
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'id',
			'DATE_FORMAT(date,"%d-%m-%Y") as date',
			'name',
			'value',
		);
		
		$condition = array(
			"status" => 1,
		);
		
		if($date_start){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		
        $this->datatables->select(implode(",", $select))->from('expense')
		->where($condition)
		->unset_column('value')->add_column('value', "$1", 'get_number_format(value)');
			
        echo $this->datatables->generate();
	}
	
	public function get_view_outcome($date_start=0, $date_end=0)
	{
		$current_user_id = 0;
		$date_start = $this->input->get("date_start");
		$date_end = $this->input->get("date_end");
		
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'name',
			'DATE_FORMAT(date,"%d-%m-%Y") as date',
			'value',
		);
		
		$condition = array();
		
		if($date_start){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		
        $this->datatables->select(implode(",", $select))->from('view_outcome')
		->where($condition)
		->unset_column('value')->add_column('value', "$1", 'get_number_format(value)');
			
        echo $this->datatables->generate();
	}
	
	public function get_examination_result($date_start=0, $date_end=0)
	{
		$current_user_id = 0;
		$date_start = $this->input->get("date_start");
		$date_end = $this->input->get("date_end");
		
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			't.id',
			'd.name as doctor',
			'p.name as patient',
			't.date',
			't.total',
			't.discount',
		);
		
		$condition = array();
		
		if($date_start){
			$condition["date >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["date <="] = date("Y-m-d", strtotime($date_end));
		}
		
        $this->datatables->select(implode(",", $select))->from('transaction t')
		->where($condition)
		->join("doctor d", "d.id = t.doctor_id", "left")
		->join("patient p", "p.id = t.patient_id", "left")
		->unset_column('total')->add_column('total', "$1", 'get_number_format(total)')
		->unset_column('discount')->add_column('discount', "$1", 'get_number_format(discount)');
			
        echo $this->datatables->generate();
	}
	
	public function get_income($date_start=0, $date_end=0)
	{
		$current_user_id = 0;
		$date_start = $this->input->get("date_start");
		$date_end = $this->input->get("date_end");
		
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			'DATE_FORMAT(t.date,"%d-%m-%Y %H:%i:%s") as date',
			't.id',
			't.transaction_no',
			'd.name as doctor',
			'p.name as patient',
			't.total',
			't.discount',
		);
		
		$condition = array();
		
		if($date_start){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		
        $this->datatables->select(implode(",", $select))->from('transaction t')
		->where($condition)
		->join("doctor d", "d.id = t.doctor_id", "left")
		->join("patient p", "p.id = t.patient_id", "left")
		->unset_column('total')->add_column('total', "$1", 'get_number_format(total)')
		->unset_column('discount')->add_column('discount', "$1", 'get_number_format(discount)')
		->add_column('actions', "$1", 'get_actions_income(id,"/report/income")');
			
        echo $this->datatables->generate();
	}
	
	public function get_transaction()
	{
		$current_user_id = 0;
		$date_start = $this->input->get("date_start");
		$date_end = $this->input->get("date_end");
		$result = ($this->input->get("result") ? $this->input->get("result") : "all");
		
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			't.id',
			't.transaction_no',
			'd.name as doctor',
			'p.name as patient',
			'DATE_FORMAT(t.date,"%d-%m-%Y %H:%i:%s") as date',
			't.total',
			't.discount',
		);
		
		$condition = array();
		
		if($date_start){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		if($result == "empty"){
			$condition["examination_result.result_value"] = NULL;
		}
		
        $this->datatables->select(implode(",", $select))->from('transaction t')
		->where($condition)
		->join("doctor d", "d.id = t.doctor_id", "left")
		->join("patient p", "p.id = t.patient_id", "left")
		->join("examination_result", "examination_result.transaction_id = t.id", "left")
		->unset_column('total')->add_column('total', "$1", 'get_number_format(total)')
		->unset_column('discount')->add_column('discount', "$1", 'get_number_format(discount)')
		->add_column('actions', "$1", 'get_actions_transaction(id, "'.$result.'")')
		->group_by('t.id');
			
        echo $this->datatables->generate();
	}
	
	public function get_doctor_services($date_start=0, $date_end=0)
	{
		$current_user_id = 0;
		$date_start = $this->input->get("date_start");
		$date_end = $this->input->get("date_end");
		
		$this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));
		
		$select = array(
			't.id',
			't.transaction_no',
			'd.name as doctor',
			'p.name as patient',
			'DATE_FORMAT(t.date,"%d-%m-%Y %H:%i:%s") as date',
			't.total',
			't.percent_doctor_services',
			'((t.percent_doctor_services/100) * t.total) as doctor_services',
		);
		
		$condition = array("t.percent_doctor_services >" => 0);
		//$condition = array();
		
		if($date_start){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') >="] = date("Y-m-d", strtotime($date_start));
		}
		
		if($date_end){
			$condition["DATE_FORMAT(date,'%Y-%m-%d') <="] = date("Y-m-d", strtotime($date_end));
		}
		
        $this->datatables->select(implode(",", $select))->from('transaction t')
		->where($condition)
		->join("doctor d", "d.id = t.doctor_id", "left")
		->join("patient p", "p.id = t.patient_id", "left")
		->unset_column('total')->add_column('total', "$1", 'get_number_format(total)')
		->unset_column('percent_doctor_services')->add_column('percent_doctor_services', "$1", 'get_number_format(percent_doctor_services)')
		->unset_column('doctor_services')->add_column('doctor_services', "$1", 'get_number_format(doctor_services)');
			
        echo $this->datatables->generate();
	}
}
