<!DOCTYPE html>
<html lang="en">
    <head>
	
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
       
        <title>Mikrotest</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style_struk.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
		<script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<script src="<?php echo base_url('assets/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
    </head>

    <body>
		<nav class="navbar navbar-default navbar-custom">
			<div class="container-fluid">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php if($display_menu) : ?>
					<ul class="nav navbar-nav">
						<li><a class="btn btn-header" href="<?php echo $_SERVER['HTTP_REFERER'];?>">Kembali</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><button onclick="printDiv('printableArea')" class="btn btn-header print" type="button" style="margin-right: 20px;">Print</button></li>
						<li><a class="btn btn-header" href="<?php echo base_url("main/transaction");?>">Selesai</a></li>
					</ul>
					<?php else : ?>
					<ul class="nav navbar-nav navbar-right">
						<li><button onclick="printDiv('printableArea')" class="btn btn-header print" type="button" style="margin-right: 20px;">Print</button></li>
						<li><a class="btn btn-header" href="<?php echo $_SERVER['HTTP_REFERER'];?>">Kembali</a></li>
					</ul>
					<?php endif ?>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<div id="printableArea" class="container font-11 border-note">
			<div class="full-width margin-bottom-20" style="display:table;padding:0 15px;">
				
				<div class="col-xs-4" style="display:table;">
					<div class="row">
						<img src="<?php echo base_url("assets/img/Logo1final.png");?>" style="width:70%;margin:auto;display:table;">
						<p class="text-center">Jl. Raya Soreang No. 9 Telp. 022-5893029,  Soreang Kabupaten Bandung </p>
					</div>
				</div>
				<div class="col-xs-8" style="display:table;">
				<div class="row">
					<h4 class="uppercase text-right">Laporan Laba Rugi</h4>
					<table class="table-border pull-right">
						<tr>
							<td class="border-right-hide" style="width:150px;">Dari Tanggal</td>
							<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
							<td class="border-left-hide" style="width:220px;"><?php echo $start_date;?></td>
						</tr>
						<tr>
							<td class="border-right-hide">Sampai Tanggal</td>
							<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
							<td class="border-left-hide"><?php echo $end_date;?></td>
						</tr>
					</table>
				</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row">
				<div class="col-sm-6">
				<h4 class="box-title" style="text-align:center;">Uraian Penerimaan</h4>
				<table class="full-width table-border table-space">
					<tr>
						<td class="table-header" style="width:40px;">No</td>
						<td class="table-header">Tanggal</td>
						<td class="table-header">Pasien</td>
						<td class="table-header">Dokter</td>
						<td class="table-header">Total</td>
					</tr>
					<?php 
					$index = 1;
					$subtotal_income = 0;
					foreach($report_income as $income_row): ?>
					<tr>
						<td class="text-center"><?php echo ($index);?></td>
						<td style="width:100px;"><?php echo $income_row->date;?></td>
						<td><?php echo $income_row->patient;?></td>
						<td><?php echo $income_row->doctor;?></td>
						<td style="width:150px;" class="text-right"><?php echo number_format($income_row->total);?></td>
					</tr>
					<?php $index++;$subtotal_income += $income_row->total;
					endforeach ?>
					<tr>
						<td colspan="4"  class="text-right">Sub Total Penerimaan</td>
						<td class="text-right"><?php echo number_format($subtotal_income);?></td>
					</tr>
				</table>
				</div>
				<div class="col-sm-6">
				<h4 class="box-title" style="text-align:center;">Uraian Pengeluaran</h4>
				<table class="full-width table-border table-space">
					<tr>
						<td class="table-header" style="width:40px;">No</td>
						<td class="table-header">Tanggal</td>
						<td class="table-header">Uraian Belanja</td>
						<td class="table-header">Harga</td>
					</tr>
					<?php 
					$index = 1;
					$subtotal_outcome = 0;
					foreach($report_outcome as $outcome_row): ?>
					<tr>
						<td class="text-center"><?php echo ($index);?></td>
						<td style="width:100px;"><?php echo $outcome_row->date;?></td>
						<td><?php echo $outcome_row->name;?></td>
						<td style="width:150px;"class="text-right"><?php echo number_format($outcome_row->value);?></td>
					</tr>
					<?php $index++;$subtotal_outcome += $outcome_row->value;
					endforeach ?>
					<tr>
						<td colspan="3"  class="text-right">Sub Total Pengeluaran</td>
						<td class="text-right"><?php echo number_format($subtotal_outcome);?></td>
					</tr>
				</table>
				</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row">
				<h4 class="box-title" style="text-align:center;">Ringkasan Laba Rugi</h4>
				<table class="full-width table-border table-space">
					<tr>
						<td style="text-align:right;">SUB TOTAL PENERIMAAN(Rp)</td>
						<td style="text-align:right;width:150p"><?php echo number_format($subtotal_income);?></td>
						</tr>
					<tr>
						<td style="text-align:right;">SUB TOTAL PENGELUARAN(Rp)</td>
						<td style="text-align:right;width:150p"><?php echo number_format($subtotal_outcome);?></td>
					</tr>
					<tr>
						<td style="text-align:right;">GRAND TOTAL(Rp)</td>
						<td style="text-align:right;width:150p"><?php echo number_format($subtotal_income - $subtotal_outcome);?></td>
					</tr>
				</div>
			</div>
		</div>
	</body>
</html>
<script>
	function printDiv(divName) {
		
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
		initDatePicker();
	}
	initDatePicker();
	
	function initDatePicker(){
		//Date picker
		$(document).find('.datepicker-input').datepicker({
			autoclose: true,
			format: "yyyy-mm-dd",
		});	
	}


</script>