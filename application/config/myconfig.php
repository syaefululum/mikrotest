<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['path_asset'] = "assets/";
$config['menu_superadmin'] = "examinations_create,examinations_results,expense_operational,expense_employees,users,patients,employees,";
$config['menu_superadmin'] .= "doctors,examinations,examination_result,income,expense_outcome,salary,doctor_services,profit_loss,transaction_report,";
$config['menu_superadmin'] .= "expense_employee,subexaminations";
$config['menu_manager'] = "examinations_create,examinations_results,expense_operational,expense_employee,patients,doctors,";
$config['menu_manager'] .= "examinations,examination_result,income,expense_outcome,doctor_services,transaction_report";
//$config['menu_operator'] = "examinations_create,examinations_results,expense_operational,patients,transaction_report";
$config['menu_operator'] = "examinations_create,examinations_results,transaction_report";
/*
examinations_create
examinations_results
expense_operational
expense_employees
users
patients
employees
doctors
examinations
subexaminations
examination_result
income
expense_outcome
salary
doctor_services
profit_loss
transaction_report
expense_employee
*/