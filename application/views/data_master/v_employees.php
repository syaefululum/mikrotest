<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Master
        <small>Pegawai</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Master</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">            
			<div class="result">
			<?php
				if (! empty($message_success)) {
					echo '<div class="alert alert-success" role="alert">';
					echo $message_success;
					echo '</div>';
				}
				if (! empty($message)) {
					echo '<div class="alert alert-danger" role="alert">';
					echo $message;
					echo '</div>';
				}
				?>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Pegawai</h3>
					<div class="pull-right">
						<a href="<?php echo base_url("main/employees_create");?>" class="btn btn-primary">Add Pegawai</a>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				  <table id="datablesEmployees" class="table table-bordered table-striped">
					<thead>
					<tr>
						<td>ID</td>
						<td>NAME</td>
						<td style="width:85px !important">ACTION</td>
					</tr>
					</thead>
					<tbody></tbody>
				  </table>
				</div>
				<input hidden id="dataProcessUrlEmployees" value="<?php echo base_url("datatables_commhub/get_employees");?>"/>
				<!-- /.box-body -->
			</div>
		</div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->