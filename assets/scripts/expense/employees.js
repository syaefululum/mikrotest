$(function() {
	$('#datablesSalaryEmployees').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"bProcessing" : true,
		"bServerSide" : true,
		"sServerMethod" : "POST",
		"ajax" : $('#dataProcessUrlSalaryEmployees').val(),
		"columnDefs" : [
		{
			"targets" : [0,9],
			"bSearchable" : false
		},		
		{
			"targets" : [5],
			"bSortable" : false,
			"class" : 'center-tr'
		},		
		{
			"targets" : [0],
			"visible" : false
		}],
		"columns" : [			
			{data : "id"},
			{data : "name"},
			{data : "date"},
			{data : "basic_salary"},
			{data : "incentive"},
			{data : "overtime"},
			{data : "rounding_off"},
			{
				data : "salary"
			},
			{data : "information"},
			{
				width: "150px", 
				data : "actions"
			},
		],
		"order"          : [[2, "DESC"], [1, "ASC"]]
    });	
});
