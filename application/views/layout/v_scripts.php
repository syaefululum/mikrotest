<script>
	var base_url = "<?php echo base_url()?>";
</script>
<!-- BEGIN CORE PLUGINS -->
<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url("assets/plugins/jQuery/jQuery-2.2.0.min.js");?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url("assets/plugins/jQueryUI/jquery-ui.min.js");?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url("assets/plugins/bootstrap/js/bootstrap.min.js");?>"></script>
<!-- Base -->
<script src="<?php echo base_url("assets/plugins/slimScroll/jquery.slimscroll.min.js");?>"></script>
<script src="<?php echo base_url("assets/js/app.min.js");?>"></script>
<script src="<?php echo base_url("assets/scripts/base.js");?>"></script>

<!--[if lt IE 9]>
<![endif]-->   
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php if(!empty($scripts)): foreach($scripts as $val):?>
<script src="<?php echo $path_asset.$val?>" type="text/javascript"></script>
<?php endforeach; endif?>
<!-- END PAGE LEVEL SCRIPTS --> 