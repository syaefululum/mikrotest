<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Master
        <small><?php echo $submenu;?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Master</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <?php echo form_open_multipart(base_url(uri_string()), array('class' => 'form-horizontal form-ajax'));?>
		<div class="col-lg-12">
			<div class="result">
				<?php
				if (! empty($message_success)) {
					echo '<div class="alert alert-success" role="alert">';
					echo $message_success;
					echo '</div>';
				}
				if (! empty($message)) {
					echo '<div class="alert alert-danger" role="alert">';
					echo $message;
					echo '</div>';
				}
				?>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group <?php echo (form_error('name') ? 'has-error' : '')?>">
										<label for="name" class="col-sm-2 control-label">Name</label>
										<div class="col-sm-10">
											<?php echo form_input($name); ?>
											<span class="help-block"><?php echo form_error('name'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('address') ? 'has-error' : '')?>">
										<label for="address" class="col-sm-2 control-label">Address</label>
										<div class="col-sm-10">
											<?php echo form_textarea($address); ?>
											<span class="help-block"><?php echo form_error('address'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('phone') ? 'has-error' : '')?>">
										<label for="name" class="col-sm-2 control-label">Phone</label>
										<div class="col-sm-10">
											<?php echo form_input($phone); ?>
											<span class="help-block"><?php echo form_error('phone'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('service') ? 'has-error' : '')?>">
										<label for="name" class="col-sm-2 control-label">Jasa (%)</label>
										<div class="col-sm-10">
											<?php echo form_input($service); ?>
											<span class="help-block"><?php echo form_error('service'); ?></span>
										</div>
									</div>
									<!--
									<div class="form-group <?php echo (form_error('gender') ? 'has-error' : '')?>">
										<label for="role" class="col-sm-2 control-label">Gender</label>
										<div class="col-sm-10">
											<?php
											echo form_dropdown('gender', $groups_list, $selected_group, 'field-name = "Gender" class="form-control requiredDropdown" autocomplete="off"');
											?>
											<span class="help-block"><?php echo form_error('gender'); ?></span>
										</div>
									</div>
									-->
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<button type="submit" name="btnAction" value="save"
													class="btn btn-primary"><?php echo $this->lang->line('ds_submit_save'); ?>
											</button>
											<button type="submit" name="btnAction" value="save_exit"
													class="btn btn-primary">
												<?php echo $this->lang->line('ds_submit_save_exit'); ?>
											</button>
											<a href="<?php echo base_url('main/doctors'); ?>"
											   class="btn btn-default"><?php echo $this->lang->line('ds_submit_cancel'); ?></a>
										</div>
									</div>
								</div>
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->