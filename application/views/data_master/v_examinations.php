<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data Master
        <small>Pemeriksaan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Master</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">            
			<div class="result">
			<?php
				if (! empty($message_success)) {
					echo '<div class="alert alert-success" role="alert">';
					echo $message_success;
					echo '</div>';
				}
				if (! empty($message)) {
					echo '<div class="alert alert-danger" role="alert">';
					echo $message;
					echo '</div>';
				}
				?>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Pemeriksaan</h3>
					<div class="pull-right">
						<a href="<?php echo base_url("main/examinations_create");?>" class="btn btn-primary">Add Pemeriksaan</a>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				  <table id="datablesExaminations" class="table table-bordered table-striped">
					<thead>
					<tr>
						<td>ID</td>
						<td>KELOMPOK</td>
						<td>JENIS PEMERIKSAAN</td>
						<td>HARGA</td>
						<td>SATUAN</td>
						<td>NORMAL VALUE</td>
						<td style="width:85px !important">TINDAKAN</td>
					</tr>
					</thead>
					<tbody></tbody>
				  </table>
				</div>
				<input hidden id="dataProcessUrlExaminations" value="<?php echo base_url("datatables_commhub/get_examinations");?>"/>
				<!-- /.box-body -->
			</div>
		</div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->