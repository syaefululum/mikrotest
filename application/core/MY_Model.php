<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by DIOS.
 * User: Alta Falconeri
 * Date: 12/16/2014
 * Time: 10:03 AM
 */
class MY_Model extends CI_Model
{
	protected $table;
    
    /**
     * Generic function to get data
     *
     * @param   string
     *
     * @return  object
     */
    public function get($limit = 0)
    {
        if ($limit > 0) {
            $this->db->limit($limit);
        }

        return $this->db->get($this->table);
    }

    /**
     * @param string $column
     * @param bool   $distinct
     *
     * @return mixed
     */
    public function get_column($column = 'id', $distinct = FALSE)
    {
        if ($distinct === TRUE) {
            $this->db->distinct();
        }
        $this->db->select($column);

        return $this->db->get($this->table);
    }

    /**
     * Get paged data
     *
     * @access public
     *
     * @param   string
     * @param   int
     * @param   int
     * @param   boolean /array
     *
     * @return  integer/boolean
     */
    public function paged($limit = 0, $offset = 0, $where = FALSE, $order_by = FALSE)
    {
        if ($limit > 0) {
            $this->db->limit($limit, $offset);
        }

        if ($where) {
            $this->db->where($where);
        }

        if ($order_by !== FALSE) {
            $this->db->order_by($order_by);
        }

        return $this->get($this->table);
    }

    /**
     * get one row by id
     *
     * @param   string
     * @param   int
     * @param   bool
     *
     * @return  object
     */
    public function get_one($id = 0, $result_array = FALSE)
    {
        $this->db->where(array('id' => $id))->limit(1);

        return $result_array ? $this->get($this->table)->row_array() : $this->get($this->table)->row();
    }


    /**
     * get one row by any field
     *
     * @param   string
     * @param   int
     * @param   string
     *
     * @return  object
     */
    public function get_by($value = 0, $field = 'id')
    {
        $this->db->where($field, $value)->limit(1);

        return $this->db->get($this->table)->row();
    }
	
    /**
     * get rows by any field
     *
     * @param   string
     * @param   int
     * @param   string
     *
     * @return  object
     */
    public function get_where($where = array(), $limit = 0, $group_by = false, $offset = 0)
    {
        $this->db->where($where)->limit($limit);
		
		if($group_by) {
			$group_by["group"] = (isset($group_by["group"]) ? $group_by["group"] : "id");
			$group_by["sort"] = (isset($group_by["sort"]) ? $group_by["sort"] : "asc");
			$this->db->group_by($group_by["group"]);
			$this->db->order_by($group_by["order"], $group_by["sort"]);
		}
		
        return $this->db->get($this->table)->result();
    }

    /**
     * A generic function to delete data from a selected table
     * You just need to provide the table name and the item id
     *
     * @access  public
     *
     * @param   string
     * @param   integer
     * @param   string
     *
     * @return  boolean
     */
    public function delete($value = 0, $field = 'id')
    {
        $this->db->limit(1);

        return (boolean)$this->db->delete($this->table, array($field => $value));
    }


    public function delete_by_where($where)
    {
        $this->db->limit(1);

        return (boolean)$this->db->delete($this->table, $where);
    }

    public function delete_by_limit($where, $limit = 1)
    {
        if ($limit > 0) {
            $this->db->limit($limit);
        }

        return (boolean)$this->db->delete($this->table, $where);
    }


    /**
     * Save data on db
     * the 1st parameter is the table name
     * the 2nd is an array with the data that will be saved
     * the 3dt is optional. If you give an id the function will update the current id.
     * Without id a new entry will add into db
     *
     * @access public
     *
     * @param   string - table name
     * @param   array - columns w/ data to be added
     * @param   integer - pass ID to insert data
     *
     * @return  integer/boolean
     */
    public function save($columns = array(), $id = 0, $where = false)
    {
        if ($where !== false) {
            $this->db->where($where);
        }

        if ($id > 0) {
            $this->db->where('id', $id);
            $result = $this->db->update($this->table, $columns);
        }
        else if($id == 0 && $where){
            $result = $this->db->update($this->table, $columns);
        }
        else {
            $result = $this->db->insert($this->table, $columns);
        }

        if ($result) {
            return ((int)$id == 0) ? $this->db->insert_id() : $id;
        }
        else {
            return FALSE;
        }
    }

    /**
     * @param array  $columns
     * @param bool   $where
     *
     * @return bool
     */
    public function update_where($columns = array(), $where = false)
    {
        if ($where !== false) {
            $this->db->where($where);
        }

        $result = $this->db->update($this->table, $columns);

        return (bool)$this->db->affected_rows();
    }


    /**
     * returns num rows
     *
     * @access public
     *
     * @param   string
     * @param   boolean /array
     *
     * @return  integer
     */
    public function total_rows($where = FALSE)
    {
        if ($this->table == '') {
            return 0;
        }

        if ($where) {
            $this->db->where($where);
        }

        return $this->db->count_all_results($this->table);
    }
	
    public function get_all($limit = 0, $order_by = false)
    {
        if ($limit > 0) {
            $this->db->limit($limit);
        }
		
		if ($order_by !== FALSE) {
            $this->db->order_by($order_by);
        }
        return $this->db->get($this->table)->result();
    }
	
	public function insert_batch($data){
		 $result = $this->db->insert_batch($this->table, $data); 

        if ($result) {
            return $this->db->insert_id();
        }
        else {
            return FALSE;
        }
	}
}