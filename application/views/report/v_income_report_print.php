<!DOCTYPE html>
<html lang="en">
    <head>
	
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
       
        <title>Mikrotest</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style_struk.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
		<script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<script src="<?php echo base_url('assets/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
    </head>

    <body>
		<nav class="navbar navbar-default navbar-custom">
			<div class="container-fluid">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php if($display_menu) : ?>
					<ul class="nav navbar-nav">
						<li><a class="btn btn-header" href="<?php echo $_SERVER['HTTP_REFERER'];?>">Kembali</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><button onclick="printDiv('printableArea')" class="btn btn-header print" type="button" style="margin-right: 20px;">Print</button></li>
						<li><a class="btn btn-header" href="<?php echo base_url("main/transaction");?>">Selesai</a></li>
					</ul>
					<?php else : ?>
					<ul class="nav navbar-nav navbar-right">
						<li><button onclick="printDiv('printableArea')" class="btn btn-header print" type="button" style="margin-right: 20px;">Print</button></li>
						<li><a class="btn btn-header" href="<?php echo $_SERVER['HTTP_REFERER'];?>">Kembali</a></li>
					</ul>
					<?php endif ?>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<div id="printableArea" class="container font-11 border-note">
			<div class="full-width margin-bottom-20" style="display:table;padding:0 15px;">
				
				<div class="col-xs-4" style="display:table;">
					<div class="row">
						<img src="<?php echo base_url("assets/img/Logo1final.png");?>" style="width:70%;margin:auto;display:table;">
						<p class="text-center">Jl. Raya Soreang No. 9 Telp. 022-5893029,  Soreang Kabupaten Bandung </p>
					</div>
				</div>
				<div class="col-xs-8" style="display:table;">
				<div class="row">
					<h4 class="uppercase text-right">Laporan Penerimaan</h4>
					<table class="table-border pull-right">
						<tr>
							<td class="border-right-hide" style="width:150px;">Dari Tanggal</td>
							<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
							<td class="border-left-hide" style="width:220px;"><?php echo $start_date;?></td>
						</tr>
						<tr>
							<td class="border-right-hide">Sampai Tanggal</td>
							<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
							<td class="border-left-hide"><?php echo $end_date;?></td>
						</tr>
					</table>
				</div>
				</div>
			</div>
			<div class="col-sm-12">
				<table class="full-width table-border table-space">
					<tr>
						<td class="table-header" style="width:40px;">No</td>
						<td class="table-header">Tanggal</td>
						<td class="table-header">ID Transaksi</td>
						<td class="table-header">Pasien</td>
						<td class="table-header">Dokter</td>
						<td class="table-header">Diskon</td>
						<td class="table-header">Total</td>
					</tr>
					<?php 
					$index = 1;
					$subtotal = 0;
					foreach($report as $row): ?>
					<tr>
						<td class="text-center"><?php echo ($index);?></td>
						<td style="width:150px;"><?php echo $row->date;?></td>
						<td><?php echo $row->id;?></td>
						<td><?php echo $row->patient;?></td>
						<td><?php echo $row->doctor;?></td>
						<td class="text-right"><?php echo number_format($row->discount);?></td>
						<td class="text-right"><?php echo number_format($row->total);?></td>
					</tr>
					<?php $index++;$subtotal += $row->total;
					endforeach ?>
					<tr>
						<td colspan="6"  class="text-right">Jumlah Total</td>
						<td class="text-right"><?php echo number_format($subtotal);?></td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>
<script>
	function printDiv(divName) {
		
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
		initDatePicker();
	}
	initDatePicker();
	
	function initDatePicker(){
		//Date picker
		$(document).find('.datepicker-input').datepicker({
			autoclose: true,
			format: "yyyy-mm-dd",
		});	
	}


</script>