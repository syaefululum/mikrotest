<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Gaji Pegawai
        <small><?php echo $submenu;?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gaji Pegawai</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <?php echo form_open_multipart(base_url(uri_string()), array('class' => 'form-horizontal form-ajax'));?>
		<div class="col-lg-12">
			<div class="result">
				<?php
				if (! empty($message_success)) {
					echo '<div class="alert alert-success" role="alert">';
					echo $message_success;
					echo '</div>';
				}
				if (! empty($message)) {
					echo '<div class="alert alert-danger" role="alert">';
					echo $message;
					echo '</div>';
				}
				?>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group <?php echo (form_error('employee_id') ? 'has-error' : '')?>">
										<label for="employee_id" class="col-sm-2 control-label">Pegawai</label>
										<div class="col-sm-10">
											<?php
											echo form_dropdown('employee_id', $groups_list, $selected_group, 'field-name = "employee_id" class="form-control requiredDropdown" autocomplete="off"');
											?>
											<span class="help-block"><?php echo form_error('employee_id'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('date') ? 'has-error' : '')?>">
										<label for="date" class="col-sm-2 control-label">Tanggal</label>
										<div class="col-sm-10">
											<?php echo form_input($date); ?>
											<span class="help-block"><?php echo form_error('date'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('basic_salary') ? 'has-error' : '')?>">
										<label for="basic_salary" class="col-sm-2 control-label">Gaji Pokok</label>
										<div class="col-sm-10">
											<?php echo form_input($basic_salary); ?>
											<span class="help-block"><?php echo form_error('basic_salary'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('incentive') ? 'has-error' : '')?>">
										<label for="incentive" class="col-sm-2 control-label">Insentif</label>
										<div class="col-sm-10">
											<?php echo form_input($incentive); ?>
											<span class="help-block"><?php echo form_error('incentive'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('overtime') ? 'has-error' : '')?>">
										<label for="overtime" class="col-sm-2 control-label">Lembur</label>
										<div class="col-sm-10">
											<?php echo form_input($overtime); ?>
											<span class="help-block"><?php echo form_error('overtime'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('rounding_off') ? 'has-error' : '')?>">
										<label for="rounding_off" class="col-sm-2 control-label">Pembulatan</label>
										<div class="col-sm-10">
											<?php echo form_input($rounding_off); ?>
											<span class="help-block"><?php echo form_error('rounding_off'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('salary') ? 'has-error' : '')?>">
										<label for="salary" class="col-sm-2 control-label">Total Gaji</label>
										<div class="col-sm-10">
											<?php echo form_input($salary); ?>
											<span class="help-block"><?php echo form_error('salary'); ?></span>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('information') ? 'has-error' : '')?>">
										<label for="information" class="col-sm-2 control-label">Informasi</label>
										<div class="col-sm-10">
											<?php echo form_textarea($information); ?>
											<span class="help-block"><?php echo form_error('information'); ?></span>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<button type="submit" name="btnAction" value="save"
													class="btn btn-primary"><?php echo $this->lang->line('ds_submit_save'); ?>
											</button>
											<button type="submit" name="btnAction" value="save_exit"
													class="btn btn-primary">
												<?php echo $this->lang->line('ds_submit_save_exit'); ?>
											</button>
											<a href="<?php echo base_url('main/expense_employees'); ?>"
											   class="btn btn-default"><?php echo $this->lang->line('ds_submit_cancel'); ?></a>
										</div>
									</div>
								</div>
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->