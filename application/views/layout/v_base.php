<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php echo $page_head?>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="hold-transition skin-blue sidebar-mini" style="background:#ecf0f5">
	<div class="wrapper" id="page">
		<?php echo (!$content_only ? $page_header : ""); ?>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="<?php echo ($content_only ? "margin-left: 0px" : "");?>">
			<?php echo $page_content ?>
		</div>
		<?php echo (!$content_only ? $page_footer : ""); ?>
	</div>
	<div id="loading"></div>
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<?php echo $page_scripts?>
	<!-- END JAVASCRIPTS -->
</body><!-- END BODY -->
</html>