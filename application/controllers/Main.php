<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->lang->load('auth');
		$this->load->library('session');
		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		} else {
			$this->user = $this->ion_auth->user()->row();
			$this->data['message_success'] = $this->session->flashdata('message_success');
			$this->data['message'] = $this->session->flashdata('message');
			$this->menu_enabled = $this->get_enabled_menu();
		}
		date_default_timezone_set("Asia/Jakarta");
	}
	
	public function index()
	{
		$this->transaction();
	}	
	
	public function examinations_results_delete($id = 0)
	{
		$reject_user = !$this->is_has_access("examinations_results");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		if($id){
			$this->load->model("m_examination_result");
			$results = $this->m_examination_result->save(array("result_value" => NULL), 0, array("transaction_id" => (int)$id));	
		}
		
		redirect('main/examination_result/'.$id, 'refresh');
	}
	
	public function examinations_results()
	{
		$reject_user = !$this->is_has_access("examinations_results");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		
		$this->data['date_start_value'] = $this->input->post('date_start');
		$this->data['date_end_value'] = $this->input->post('date_end');
		
		$this->data['date_start'] = array('name' => 'date_start', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_start', 
											'placeholder' => 'Dari', 
											'value' => $this->input->post('date_start'));
		$this->data['date_end'] = array('name' => 'date_end', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField  datepicker-input', 
											'field-name' => 'date_end', 
											'placeholder' => 'Sampai', 
											'value' => $this->input->post('date_end'));
												
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "plugins/moment/moment.min.js";
		$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
		$this->data['scripts'][] = "scripts/examinationsResults.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		$this->data['styles'][] = "plugins/datepicker/datepicker3.css";		
		
		$this->data['active_menu'] = "examinations_results";
        $this->view("v_examinations_results", $this->data);
	}
	
	public function transaction_delete($id = 0)
	{
		$reject_user = !$this->is_has_access("examinations_results");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->load->model("m_transaction");
		$this->load->model("m_examination_result");
		
		$results = $this->m_examination_result->get_where(array("transaction_id" => (int)$id, "result_value is not" => NULL));
		
		if($results){
			$this->session->set_flashdata('message', 'Data pemeriksaan gagal terhapus, karena data hasil pemeriksaan sudah disimpan');
		} else {
			$this->m_examination_result->delete($id, "transaction_id");
			$this->m_transaction->delete($id);
		}
		redirect('main/transaction', 'refresh');
	}
	
	public function transaction()
	{
		$reject_user = !$this->is_has_access("examinations_create");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_examination_category");
		$this->load->model("m_examination");
		$this->load->model("m_patient");
		$this->load->model("m_doctor");
		
		$category = $this->m_examination_category->get_all();
		foreach($category as $e_cat){
			$condition = array(
				"examination_category_id" => $e_cat->id, 
				"status" => 1,
				"parent_id" => 0
			);
			$e_cat->examination = $this->m_examination->get_where($condition, 0);
		}
		$this->data['selected_patient'] = false;
		$this->data['selected_patient_dob'] = false;
		$this->data['selected_patient_address'] = false;
		$this->data['selected_patient_phone'] = false;
		$this->data['selected_patient_gender'] = false;
		$this->data['selected_doctor'] = false;
		$this->data['selected_doctor_address'] = false;
		$this->data['selected_doctor_phone'] = false;
		$this->data['selected_doctor_service'] = false;
		$this->data['selected_examination'] = array();
		$this->data['doctor_services'] = false;
		$this->data['discount'] = false;
		$this->data['date'] = date("D, d/m/Y, h:i A");
		
		//if($this->session->userdata('data_struk_transaction')!=null){
		//	$session_patient=$this->session->userdata('data_struk_transaction');
		//	print_r($session_patient);die();
		//}
		
		$this->data['new_patient_name'] = array('name' => 'new_patient_name', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField', 
											'field-name' => 'Name', 
											'placeholder' => 'Name');
											
		$this->data['new_patient_dob'] = array('name' => 'new_patient_dob', 
											'type' => 'text', 
											'class' => 'form-control combodate-input requiredTextField', 
											'field-name' => 'Tgl. Lahir', 
											'data-format' => 'YYYY-MM-DD', 
											'data-template' => 'D MMMM YYYY',
											'value' => date("Y-m-d"));
		
		$this->data['new_patient_address'] = array('name' => 'new_patient_address', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField', 
											'field-name' => 'Address', 
											'placeholder' => 'Address');
											
		$this->data['new_patient_phone'] = array('name' => 'new_patient_phone', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField', 
											'field-name' => 'Phone', 
											'placeholder' => 'Phone');
		
		$this->data['new_doctor_name'] = array('name' => 'new_doctor_name', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField', 
											'field-name' => 'Name', 
											'placeholder' => 'Name');
											
		$this->data['new_doctor_address'] = array('name' => 'new_doctor_address', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField', 
											'field-name' => 'Address', 
											'placeholder' => 'Address');
		
		$this->data['new_doctor_service'] = array('name' => 'new_doctor_service', 
											'type' => 'text', 
											'class' => 'form-control requiredTextField', 
											'field-name' => 'Jasa', 
											'placeholder' => 'Jasa (%)');
		
		if($this->input->post()){
			$patient = $this->input->post("patient");
			$doctor = $this->input->post("doctor");
			$examination = $this->input->post("examination");
			$doctor_services = (!empty($this->input->post("doctor_services")) ? true : false);
			$discount = (!empty($this->input->post("discount")) ? $this->input->post("discount") : 0);
			$date = (!empty($this->input->post("date")) ? $this->input->post("date") : $this->data['date']);
			$valid = false;
			$patient_select = $this->input->post("patient_select");
			$doctor_select = $this->input->post("doctor_select");
			
			if($patient_select == "new"){
				$patient = false;
				$patient_isexist = false;
				//validate form input
				$this->form_validation->set_rules('new_patient_name', 'Name', 'required|xss_clean|min_length[1]');
				$this->form_validation->set_rules('new_patient_dob', 'Tgl. Lahir', 'required|xss_clean|min_length[1]|max_length[255]');
				$this->form_validation->set_rules('new_patient_address', 'Address', 'required|xss_clean|min_length[1]|max_length[255]');
				$this->form_validation->set_rules('new_patient_phone', 'Phone', 'required|xss_clean|min_length[1]|max_length[20]');
				$this->form_validation->set_rules('new_patient_gender', 'Jenis Kelamin', 'required|xss_clean|min_length[1]|max_length[20]');
				
				if ($this->form_validation->run() == true) {				
					$new_patient_name = $this->input->post('new_patient_name');
					$new_patient_dob = $this->input->post('new_patient_dob');
					$new_patient_address = $this->input->post('new_patient_address');
					$new_patient_phone = $this->input->post('new_patient_phone');
					$new_patient_gender = $this->input->post('new_patient_gender');
					
					$data_input = array(
						"name" => $new_patient_name,
						"dob" => $new_patient_dob,
						"address" => $new_patient_address,
						"phone" => $new_patient_phone,
						"gender" => $new_patient_gender,
						"created_at" => date("Y-m-d"),
						"created_by" => $this->user->id,
					);
					
					$condition = Array(
						"name" => $new_patient_name,
						"dob" => $new_patient_dob,
						"address" => $new_patient_address,
						"status" => 1
					);
					$data_patient = $this->m_patient->get_where($condition);
					if(!$data_patient){
						$save = $this->m_patient->save($data_input);

						if ($save) {
							$patient = $save;
							$patient_select = "existing";
						}
					}else{
						$patient_isexist = true;
					}
				}
				else {
					$this->data['new_patient_name']['value'] = $this->input->post('new_patient_name');
					$this->data['new_patient_dob']['value'] = $this->input->post('new_patient_dob');
					$this->data['new_patient_address']['value'] = $this->input->post('new_patient_address');
					$this->data['new_patient_phone']['value'] = $this->input->post('new_patient_phone');
					$this->data['new_patient_gender']['value'] = $this->input->post('new_patient_gender');
				}
			}
			$this->session->set_flashdata('message', 'Pasien tidak boleh kosong');
			
			if($doctor_select == "new"){
				$doctor = false;
				$doctor_isexist = false;
				//validate form input
				$this->form_validation->set_rules('new_doctor_name', 'Name', 'required|xss_clean|min_length[1]');
				$this->form_validation->set_rules('new_doctor_address', 'Address', 'required|xss_clean|min_length[1]|max_length[255]');
				$this->form_validation->set_rules('new_doctor_service', 'Jasa', 'required|xss_clean|numeric');
				// $this->form_validation->set_rules('new_doctor_phone', 'Phone', 'required|xss_clean|min_length[1]|max_length[20]');
				
				if ($this->form_validation->run() == true) {				
					$new_doctor_name = $this->input->post('new_doctor_name');
					$new_doctor_address = $this->input->post('new_doctor_address');
					$new_doctor_service = $this->input->post('new_doctor_service');
					// $new_doctor_phone = $this->input->post('new_doctor_phone');
					
					$data_input = array(
						"name" => $new_doctor_name,
						"address" => $new_doctor_address,
						"service" => $new_doctor_service,
						// "phone" => $new_doctor_phone,
						"created_at" => date("Y-m-d"),
						"created_by" => $this->user->id,
					);

					$condition = Array(
						"name" => $new_doctor_name,
						"address" => $new_doctor_address,
						"status" => 1
					);
					$data_doctor = $this->m_doctor->get_where($condition);
					if(!$data_doctor){
						$save = $this->m_doctor->save($data_input);

						if ($save) {
							$doctor = $save;
							$doctor_select = "existing";
						}
					}else{
						$doctor_isexist = true;
					}
				}
				else {
					$this->data['new_doctor_name']['value'] = $this->input->post('new_doctor_name');
					$this->data['new_doctor_address']['value'] = $this->input->post('new_doctor_address');
					$this->data['new_doctor_service']['value'] = $this->input->post('new_doctor_service');
					// $this->data['new_doctor_phone']['value'] = $this->input->post('new_doctor_phone');
				}
			}
			
			if(empty($patient)){
				if($patient_isexist){
					$this->session->set_flashdata('message', 'Pasien sudah terdaftar');
				}else{
					$this->session->set_flashdata('message', 'Pasien tidak boleh kosong');
				}
			} else if(empty($doctor)){
				if($doctor_isexist){
					$this->session->set_flashdata('message', 'Nama Dokter sudah terdaftar');
				}else{
					$this->session->set_flashdata('message', 'Dokter tidak boleh kosong');
				}
			} else  if(empty($examination) || count($examination) == 0){
				$this->session->set_flashdata('message', 'Pemeriksaan tidak boleh kosong');
			} else {
				$valid = true;
			}
			
			$selected_patient = false;
			if($patient){
				$this->data['selected_patient'] = $patient;
				$selected_patient = $this->m_patient->get_one($patient);
				if($selected_patient){
					$this->data['selected_patient_dob'] = $selected_patient->dob;
					$this->data['selected_patient_address'] = $selected_patient->address;
					$this->data['selected_patient_phone'] = $selected_patient->phone;
					$this->data['selected_patient_gender'] = $selected_patient->gender;
				}
			}
			$selected_doctor = false;
			if($doctor){
				$this->data['selected_doctor'] = $doctor;
				$selected_doctor = $this->m_doctor->get_one($doctor);
				if($selected_doctor){
					$this->data['selected_doctor_address'] = $selected_doctor->address;
					$this->data['selected_doctor_phone'] = $selected_doctor->phone;
					$this->data['selected_doctor_service'] = $selected_doctor->service;
				}
			}
			
			if($examination){
				foreach($examination as $exa_key => $value){
					$this->data['selected_examination'][] = $exa_key;
				}
			}
			
			$this->data['doctor_services'] = $doctor_services;
			$this->data['discount'] = $discount;
			$this->data['date'] = $date;
			
			if($valid){
				$this->load->model("m_doctor_services");
				$this->load->model("m_transaction");
				$this->load->model("m_examination_result");
				
				$date_to_insert = false;
				if($date){
					$timestamp = DateTime::createFromFormat('D, d/m/Y, h:i A', $this->data['date'])->getTimestamp();
					$date_to_insert = date("Y-m-d H:i:s", $timestamp);
				}
				
				$condition = "id in (".implode(',', $this->data['selected_examination']).")";
				$examination_selected = $this->m_examination->get_where($condition, 0);
				$examination_result = array();
				$total_examination = 0;
				foreach($examination_selected as $result){
					$examination_result[] = array(
						"examination_id" => $result->id,
						"fare_applied" => $result->fare
					);
					$total_examination += $result->fare;
					$condition = Array(
						"parent_id" => $result->id,
						"status" => 1
					);
					$subexamination_selected = $this->m_examination->get_where($condition, 0);
					foreach($subexamination_selected as $subresult){
						$examination_result[] = array(
							"examination_id" => $subresult->id,
							"fare_applied" => 0
						);
					}
				}
				
				$percent_doctor_services = 0;
				if($doctor_services AND $doctor){
					$percent_doctor_services = $this->m_doctor->get_doctor_service_by_id($doctor);
					// if(count($doctor_services_data) > 0){
						// $percent_doctor_services = $doctor_services_data[0]->percent_value;
					// }
					//$percent_doctor_services = $doctor_services_data[0]->percent_value;
				}
				
				$transaction_input = array(
					"transaction_no" => $this->m_transaction->generate_transaction_no(),
					"doctor_id" => $selected_doctor->id,
					"patient_id" => $selected_patient->id,
					"discount" => $discount,
					"total" => $total_examination,
					"date" => $date_to_insert,
					"percent_doctor_services" => $percent_doctor_services,
					"created_at" => date("Y-m-d"),
					"created_by" => $this->user->id,
					"updated_at" => date("Y-m-d"),
					"updated_by" => $this->user->id
				);
				
				$transaction_id = $this->m_transaction->save($transaction_input);
				if($transaction_id){
					$examination_result_input = array();
					foreach($examination_result as $result){
						$data_to_input = $result;
						$data_to_input["transaction_id"] = $transaction_id;
						$examination_result_input[] = $data_to_input;
					}
					$examination_result_save = $this->m_examination_result->insert_batch($examination_result_input);
					
					if($examination_result_save){
						// redirect to page examination result
						redirect('main/transaction_struk_print/'.$transaction_id."/1", 'refresh');
					} else {
						$this->m_transaction->delete($transaction_id);
						$this->session->set_flashdata('message', 'Data pemeriksaan gagal tersimpan');
						redirect('main/transaction', 'refresh');
					}
				}
			}
		}
		
		$condition = array("status" => 1);
		$this->data['change_patient'] = (isset($patient_select) ? $patient_select : "existing");
		$this->data['change_doctor'] = (isset($doctor_select) ? $doctor_select : "existing");
		$this->data['patients'] = $this->m_patient->get_where($condition, 0);
		$this->data['doctors'] = $this->m_doctor->get_where($condition, 0);
		$this->data['examinations'] = $category;
		$this->data['scripts'] = array();
		$this->data['styles'] = array();
		$this->data['active_menu'] = "examinations_create";
		$this->data['message_success'] = $this->session->flashdata('message_success');
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
		$this->data['scripts'][] = "plugins/moment/moment.min.js";
		$this->data['scripts'][] = "plugins/bootstrap/js/transition.js";
		$this->data['scripts'][] = "plugins/bootstrap/js/collapse.js";
		$this->data['scripts'][] = "plugins/bootstrap-datetimepicker/4.17.37/bootstrap-datetimepicker.min.js";
		$this->data['scripts'][] = "plugins/iCheck/icheck.min.js";
		$this->data['scripts'][] = "plugins/ui-dropdown/transition.min.js";
		$this->data['scripts'][] = "plugins/ui-dropdown/dropdown.min.js";
		$this->data['scripts'][] = "plugins/combodate-1.0.7/combodate.js";
		$this->data['scripts'][] = "scripts/transaction.js";
		$this->data['styles'][] = "plugins/bootstrap-datetimepicker/4.17.37/bootstrap-datetimepicker.min.css";
		$this->data['styles'][] = "plugins/iCheck/all.css";
		$this->data['styles'][] = "plugins/ui-dropdown/transition.min.css";
		$this->data['styles'][] = "plugins/ui-dropdown/dropdown.min.css";
        $this->data['styles'][] = "plugins/datepicker/datepicker3.css";
		$this->view("v_transaction", $this->data);
	}
	
	public function transaction_struk($id = 0){
		$reject_user = !$this->is_has_access("examinations_create");
		$this->load->model("m_transaction");
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		$this->load->model("m_examination_result");
		
		$transaction = $this->m_transaction->get_one($id);
		$result = $this->m_examination_result->get_where(array("transaction_id" => $id), 0);
		
		if(!$transaction || !$result){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->data["patient"] = false;
		$this->data["doctor"] = false;
		$this->data["examination_result"] = array();
		$this->data["examination"] = array();
		$this->data["transaction"] = false;
		$this->data["examination_by_category"] = array();
		
		if($transaction){
			$this->load->model("m_patient");
			$this->load->model("m_doctor");
			$this->data["patient"] = $this->m_patient->get_one($transaction->patient_id);
			$this->data["doctor"] = $this->m_doctor->get_one($transaction->doctor_id);
			$this->data["examination_result"] = $result;
			$this->data["transaction"] = $transaction;
		}
		
		if($result){
			$examination_ids = array();
			$examination_result = array();
			foreach($result as $rslt){
				$examination_ids[] = $rslt->examination_id;
				$examination_result[$rslt->examination_id] = $rslt;
			}
			
			$condition = "id in (".implode(',', $examination_ids).")";
			$examination_data = $this->m_examination->get_where($condition, 0);
			
			$this->data["examination"] = array();
			$category_ids = array();
			$this->data["examination_category"] = array();
			foreach($examination_data as $data){
				$this->data["examination"][$data->id] = $data;
				$category_ids[] = $data->examination_category_id;
				if(!isset($this->data["examination_category"][$data->examination_category_id])){
					$this->data["examination_category"][$data->examination_category_id] = new StdClass;
				}
				if(!isset($this->data["examination_category"][$data->examination_category_id]->examination)){
					$this->data["examination_category"][$data->examination_category_id]->examination = array();
				}
				
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id] = $data;
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id]->examination_result = $examination_result[$data->id];
			}
			
			$condition = "id in (".implode(',', $category_ids).")";
			$examination_category_data = $this->m_examination_category->get_where($condition, 0);
			
			foreach($examination_category_data as $data){
				$examination = $this->data["examination_category"][$data->id]->examination;
				$this->data["examination_category"][$data->id] = $data;
				$this->data["examination_category"][$data->id]->examination = $examination;
			}
			
			$this->data["examination_by_category"] = $examination_category_data;
			
		}
		
		$this->data['transaction_id'] = $id;
		$this->data['active_menu'] = "examinations_create";
		$this->data['message_success'] = $this->session->flashdata('message_success');
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->data['scripts'][] = "scripts/transactionStruck.js";
        $this->view("v_transaction_struk", $this->data);
	}
	
	public function transaction_struk_print($id = 0, $display_menu = 0){
		$reject_user = !$this->is_has_access("examinations_create");
		$this->load->model("m_transaction");
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		$this->load->model("m_examination_result");
		
		$transaction = $this->m_transaction->get_one($id);
		$result = $this->m_examination_result->get_where(array("transaction_id" => $id), 0);
		foreach($result as $key=>$row){
			$exam_check = $this->m_examination->get_one($row->examination_id);
			if($exam_check->parent_id != 0){
				unset($result[$key]);
				
			}
		}
		
		if(!$transaction || !$result){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->data["patient"] = false;
		$this->data["doctor"] = false;
		$this->data["examination_result"] = array();
		$this->data["examination"] = array();
		$this->data["transaction"] = false;
		$this->data["examination_by_category"] = array();
		
		if($transaction){
			$this->load->model("m_patient");
			$this->load->model("m_doctor");
			$this->data["patient"] = $this->m_patient->get_one($transaction->patient_id);
			$this->data["doctor"] = $this->m_doctor->get_one($transaction->doctor_id);
			$this->data["examination_result"] = $result;
			$this->data["transaction"] = $transaction;
		}
		
		if($result){
			$examination_ids = array();
			$examination_result = array();
			foreach($result as $rslt){
				$examination_ids[] = $rslt->examination_id;
				$examination_result[$rslt->examination_id] = $rslt;
			}
			
			$condition = "id in (".implode(',', $examination_ids).")";
			$examination_data = $this->m_examination->get_where($condition, 0);
			
			$this->data["examination"] = array();
			$category_ids = array();
			$this->data["examination_category"] = array();
			foreach($examination_data as $data){
				$this->data["examination"][$data->id] = $data;
				$category_ids[] = $data->examination_category_id;
				if(!isset($this->data["examination_category"][$data->examination_category_id])){
					$this->data["examination_category"][$data->examination_category_id] = new StdClass;
				}
				if(!isset($this->data["examination_category"][$data->examination_category_id]->examination)){
					$this->data["examination_category"][$data->examination_category_id]->examination = array();
				}
				
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id] = $data;
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id]->examination_result = $examination_result[$data->id];
			}
		}
		$this->data["user"] = $this->user;
		$this->data["display_menu"] = $display_menu;
		
        $this->load->view("v_transaction_struk_print", $this->data);
	}
	public function examination_result($id = 0){
		$reject_user = !$this->is_has_access("examinations_results");
		
		$this->load->model("m_transaction");
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		$this->load->model("m_examination_result");
		
		$transaction = $this->m_transaction->get_one($id);
		$result = $this->m_examination_result->get_where(array("transaction_id" => $id), 0);
		
		foreach($result as $key=>$row){
			$exam_check = $this->m_examination->get_one($row->examination_id);
			if($exam_check->parent_id != 0){
				unset($result[$key]);
				
			}
		}
		
		if(!$transaction || !$result){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->data["patient"] = false;
		$this->data["doctor"] = false;
		$this->data["examination_result"] = array();
		$this->data["examination"] = array();
		$this->data["transaction"] = false;
		$this->data["examination_by_category"] = array();
		
		if($transaction){
			$this->load->model("m_patient");
			$this->load->model("m_doctor");
			$this->data["patient"] = $this->m_patient->get_one($transaction->patient_id);
			$this->data["doctor"] = $this->m_doctor->get_one($transaction->doctor_id);
			$this->data["examination_result"] = $result;
			$this->data["transaction"] = $transaction;
		}
		
		if($result){
			$examination_ids = array();
			$examination_result = array();
			foreach($result as $rslt){
				$examination_ids[] = $rslt->examination_id;
				$examination_result[$rslt->examination_id] = $rslt;
			}
			
			$condition = "id in (".implode(',', $examination_ids).")";
			$examination_data = $this->m_examination->get_where($condition, 0);
			
			$this->data["examination"] = array();
			$category_ids = array();
			$this->data["examination_category"] = array();
			
			foreach($examination_data as $data){
				$condition = Array(
					"parent_id" => $data->id,
					"status" => 1
				);
				$subexamination_data = $this->m_examination->get_where($condition, 0);
				$data->subexamination = $subexamination_data;
				
				$this->data["examination"][$data->id] = $data;
				$category_ids[] = $data->examination_category_id;
				if(!isset($this->data["examination_category"][$data->examination_category_id])){
					$this->data["examination_category"][$data->examination_category_id] = new StdClass;
				}
				if(!isset($this->data["examination_category"][$data->examination_category_id]->examination)){
					$this->data["examination_category"][$data->examination_category_id]->examination = array();
				}
				
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id] = $data;
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id]->examination_result = $examination_result[$data->id];

			}
			
			$condition = "id in (".implode(',', $category_ids).")";
			$examination_category_data = $this->m_examination_category->get_where($condition, 0);
			
			foreach($examination_category_data as $data){
				$examination = $this->data["examination_category"][$data->id]->examination;
				$this->data["examination_category"][$data->id] = $data;
				$this->data["examination_category"][$data->id]->examination = $examination;
			}
			
			$this->data["examination_by_category"] = $examination_category_data;
			
		}
		
		$examination = $this->data["examination"];
		// echo '<pre>';
		// print_r($this->data["examination_result"]);
		// die();
		foreach($this->data["examination_result"] as $examination_result){
			$temp = $this->m_examination->get_one($examination_result->examination_id);
			if($temp->type == 1){
			$this->form_validation->set_rules('examination_result_input['.$examination_result->id.']', 
				$examination[$examination_result->examination_id]->name, 'required|xss_clean|min_length[1]|max_length[25]');
			}
		}
		
		foreach($examination_category_data as $examination_data_row){
			foreach($examination_data_row->examination as $subexamination_data_row){
				foreach($subexamination_data_row->subexamination as $row){
						$exam_result = $this->m_examination_result->get_where(array("transaction_id" => $id,"examination_id" =>$row->id), 0);
						if(!$exam_result){
							$transaction_input = Array(
								"examination_id" => $row->id,
								"transaction_id" => $id
							);
							$transaction_result_id = $this->m_examination_result->save($transaction_input);
							$row->result_id = $transaction_result_id;
						}else{
							$row->result_id = $exam_result[0]->id;
						}
						
						$temp = $this->m_examination->get_one($row->id);
						if($temp->type == 1){
						$this->form_validation->set_rules('examination_result_input['.$row->result_id.']', 
							$examination[$examination_result->examination_id]->name, 'required|xss_clean|min_length[1]|max_length[25]');
						}else{
							$this->form_validation->set_rules('examination_result_notes['.$row->result_id.']', 
							$examination[$examination_result->examination_id]->name, 'required|xss_clean|min_length[1]|max_length[25]');
						}
					
				}
			}
		}
		
		$examination_result_notes = $this->input->post('examination_result_notes');
		
        if ($this->form_validation->run() == true) {
			$examination_result_input = $this->input->post('examination_result_input');
			$information = $this->input->post('information');
			$count_saved = 0;
			foreach($examination_result_input as $key => $result_value){
				$data_input = array("result_value" => $result_value);
				if(isset($examination_result_notes[$key])){
					$data_input["notes"] = $examination_result_notes[$key];
				}
				
				$save = $this->m_examination_result->save($data_input, $key);
				if($save) $count_saved++;
			}
			if($count_saved == count($examination_result_input)) $success = true;
			
			if(!empty($information)){
				$data_input = array("information" => $information);
				$save = $this->m_transaction->save($data_input, $transaction->id);
				if($save) $success = true;
				else $success = false;
			}

			if ($success === false) {
				$this->session->set_flashdata('message', 'Failed save result');
				redirect('main/examination_result/'.$id, 'refresh');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success save result!');
				redirect('main/examination_data_print/'.$id.'/1', 'refresh');
			}
            
        }
        else {
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');
			
			foreach($this->data["examination_result"] as $examination_result){					
					$this->data['examination_result_input'][$examination_result->id] = array(
												'name' => 'examination_result_input['.$examination_result->id.']', 
												'type' => 'text', 
												'maxlength' => '25', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'examination_result_input['.$examination_result->id.']', 
												'placeholder' => $examination[$examination_result->examination_id]->name, 
												'value' => $this->form_validation->set_value('examination_result_input['.$examination_result->id.']',
													$examination_result->result_value
												));			
					$this->data['examination_result_notes'][$examination_result->id] = array(
												'name' => 'examination_result_notes['.$examination_result->id.']', 
												'type' => 'text', 
												'maxlength' => '25', 
												'class' => 'form-control information', 
												'field-name' => 'examination_result_notes['.$examination_result->id.']', 
												'placeholder' => "Keterangan ".$examination[$examination_result->examination_id]->name, 
												'value' => $this->form_validation->set_value('examination_result_notes['.$examination_result->id.']',
													$examination_result->notes
												));
												
			}
          
            //load content
			
			$this->data['information'] = ($this->input->post('information') ? $this->input->post('information') : $transaction->information);
			
			$this->data['active_menu'] = "examinations_results";
			$this->data['message_success'] = $this->session->flashdata('message_success');
			$this->data['message'] = $this->session->flashdata('message');
			$this->data['scripts'][] = "scripts/examinationResult.js";
			
			$this->view("v_examination_result", $this->data);
        }
		
	}
	
	public function examination_data($id = 0){
		$reject_user = !$this->is_has_access("examinations_results");
		
		$this->load->model("m_transaction");
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		$this->load->model("m_examination_result");
		
		$transaction = $this->m_transaction->get_one($id);
		$result = $this->m_examination_result->get_where(array("transaction_id" => $id), 0);
		
		if(!$transaction || !$result){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->data["patient"] = false;
		$this->data["doctor"] = false;
		$this->data["examination_result"] = array();
		$this->data["examination"] = array();
		$this->data["transaction"] = false;
		$this->data["examination_by_category"] = array();
		
		if($transaction){
			$this->load->model("m_patient");
			$this->load->model("m_doctor");
			$this->data["patient"] = $this->m_patient->get_one($transaction->patient_id);
			$this->data["doctor"] = $this->m_doctor->get_one($transaction->doctor_id);
			$this->data["examination_result"] = $result;
			$this->data["transaction"] = $transaction;
		}
		
		if($result){
			$examination_ids = array();
			$examination_result = array();
			foreach($result as $rslt){
				$examination_ids[] = $rslt->examination_id;
				$examination_result[$rslt->examination_id] = $rslt;
			}
			
			$condition = "id in (".implode(',', $examination_ids).")";
			$examination_data = $this->m_examination->get_where($condition, 0);
			
			$this->data["examination"] = array();
			$category_ids = array();
			$this->data["examination_category"] = array();
			foreach($examination_data as $data){
				$this->data["examination"][$data->id] = $data;
				$category_ids[] = $data->examination_category_id;
				if(!isset($this->data["examination_category"][$data->examination_category_id])){
					$this->data["examination_category"][$data->examination_category_id] = new StdClass;
				}
				if(!isset($this->data["examination_category"][$data->examination_category_id]->examination)){
					$this->data["examination_category"][$data->examination_category_id]->examination = array();
				}
				
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id] = $data;
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id]->examination_result = $examination_result[$data->id];
			}
			
			$condition = "id in (".implode(',', $category_ids).")";
			$examination_category_data = $this->m_examination_category->get_where($condition, 0);
			
			foreach($examination_category_data as $data){
				$examination = $this->data["examination_category"][$data->id]->examination;
				$this->data["examination_category"][$data->id] = $data;
				$this->data["examination_category"][$data->id]->examination = $examination;
			}
			
			$this->data["examination_by_category"] = $examination_category_data;
			
		}
		
		$this->data['transaction_id'] = $id;
		$this->data['active_menu'] = "examinations_results";
		$this->data['message_success'] = $this->session->flashdata('message_success');
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['scripts'][] = "scripts/examinationData.js";
		
		$this->view("v_examination_data", $this->data);
	}
	
	public function examination_data_print($id, $display_menu = 0){
		$reject_user = !$this->is_has_access("examinations_results");
		
		$this->load->model("m_transaction");
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		$this->load->model("m_examination_result");
		
		$transaction = $this->m_transaction->get_one($id);
		$result = $this->m_examination_result->get_where(array("transaction_id" => $id), 0);
		foreach($result as $key=>$row){
			$exam_check = $this->m_examination->get_one($row->examination_id);
			if($exam_check->parent_id != 0){
				unset($result[$key]);
				
			}
		}
		
		if(!$transaction || !$result){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->data["display_menu"] = $display_menu;
		$this->data["patient"] = false;
		$this->data["doctor"] = false;
		$this->data["examination_result"] = array();
		$this->data["examination"] = array();
		$this->data["transaction"] = false;
		$this->data["examination_by_category"] = array();
		
		if($transaction){
			$this->load->model("m_patient");
			$this->load->model("m_doctor");
			$this->data["patient"] = $this->m_patient->get_one($transaction->patient_id);
			$this->data["doctor"] = $this->m_doctor->get_one($transaction->doctor_id);
			$this->data["examination_result"] = $result;
			$this->data["transaction"] = $transaction;
		}
		
		if($result){
			$examination_ids = array();
			$examination_result = array();
			foreach($result as $rslt){
				$examination_ids[] = $rslt->examination_id;
				$examination_result[$rslt->examination_id] = $rslt;
			}
			
			$condition = "id in (".implode(',', $examination_ids).")";
			$examination_data = $this->m_examination->get_where($condition, 0);
			// echo '<pre>';
				// print_r($examination_data);
				// die();
			$this->data["examination"] = array();
			$category_ids = array();
			$this->data["examination_category"] = array();
			foreach($examination_data as $data){
				$condition = Array(
					"e.parent_id" => $data->id,
					"r.transaction_id" => $id
				);
				$subexamination_data = $this->m_examination->get_examination_by_transaction($condition);
				// echo '<pre>';
				// print_r($subexamination_data);
				// die();
				foreach($subexamination_data as $row){
					$row->examination_result = $this->m_examination_result->get_where(array("transaction_id" => $id,"examination_id" => $row->id),0)[0];
				}
				$data->subexamination = $subexamination_data;
				$this->data["examination"][$data->id] = $data;
				$category_ids[] = $data->examination_category_id;
				if(!isset($this->data["examination_category"][$data->examination_category_id])){
					$this->data["examination_category"][$data->examination_category_id] = new StdClass;
				}
				if(!isset($this->data["examination_category"][$data->examination_category_id]->examination)){
					$this->data["examination_category"][$data->examination_category_id]->examination = array();
				}
				
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id] = $data;
				$this->data["examination_category"][$data->examination_category_id]->examination[$data->id]->examination_result = $examination_result[$data->id];
			}
			
			
			$condition = "id in (".implode(',', $category_ids).")";
			$examination_category_data = $this->m_examination_category->get_where($condition, 0);
			
			foreach($examination_category_data as $data){
				$examination = $this->data["examination_category"][$data->id]->examination;
				$this->data["examination_category"][$data->id] = $data;
				$this->data["examination_category"][$data->id]->examination = $examination;
			}
			
			$this->data["examination_by_category"] = $examination_category_data;
			
		}
		
		$this->data['transaction_id'] = $id;
		$this->data['active_menu'] = "examinations_results";
		
		$this->data["user"] = $this->user;
		
		$this->load->view("v_examination_data_print", $this->data);
	}
	
	public function users()
	{
		$reject_user = !$this->is_has_access("users");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "scripts/data-master/users.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		
		$this->data['active_menu'] = "users";
        $this->view("data_master/v_users", $this->data);
	}
	
	public function users_create()
	{
		$reject_user = !$this->is_has_access("users");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_users");
		$this->load->model("m_groups");
		$this->load->model("m_users_groups");
		
		$this->data["is_edit"] = false;
		$this->data["users_groups"] = false;
		$this->data["groups_list"] = array();
		$users_groups = $this->m_users_groups->get_by($this->user->id, "user_id");
		if($users_groups){
			$this->data["users_groups"] = $users_groups;
			$this->data["groups_list"] = $this->m_groups->get_groups_dropdown($users_groups->group_id);
		}
		
		//validate form input
        $this->form_validation->set_rules('username', 'Username', 'required|xss_clean|min_length[1]|max_length[25]|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean|min_length[1]|max_length[25]|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Confirm Password', 'required|xss_clean|min_length[1]|max_length[25]');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean|min_length[1]|max_length[25]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean|min_length[1]|max_length[25]');
		$this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean|min_length[1]|max_length[20]');
		$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|min_length[1]|max_length[25]|valid_email');

        if ($this->form_validation->run() == true) {						
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$phone = $this->input->post('phone');
			$email = $this->input->post('email');
			$group_id = $this->input->post('group_id');
			
			if(!isset($this->data["groups_list"][$group_id])){
				foreach($this->data["groups_list"] as $key => $value){
					$group_id = $key;
					break;
				}
			}
			$data_input = array(
				"username" => $username,
				"password" => $this->encrypt_password($password),
				"first_name" => $first_name,
				"last_name" => $last_name,
				"phone" => $phone,
				"email" => $email,
				"company" => (isset($this->data["groups_list"][$group_id]) ? $this->data["groups_list"][$group_id] : NULL),
				"active" => 1,
				"created_on" => time(),
				"password_original" => $password,
			);		
			
			$save = $this->m_users->save($data_input);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed save user');
			}
			else {
				$conditon = array("user_id" => $save);
				$save_group = $this->m_users_groups->get_by($save, "user_id");
				$group_result_id = 0;
				if($save_group){
					$group_result_id = $save_group->id;
				}
				$data_input = array(
					"user_id" => $save,
					"group_id" => $group_id,
				);
				$save_group = $this->m_users_groups->save($data_input,$group_result_id);
				if($save_group){
					$this->session->set_flashdata('message_success', 'Success save '.$username.'!');
				} else {
					$delete = $this->m_users->delete($save);
					$this->session->set_flashdata('message', 'Failed save user');
				}
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/users', 'refresh');
            }
            else {
                redirect('main/users_create/', 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['username'] = array('name' => 'username', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Username', 
												'placeholder' => 'Username', 
												'value' => $this->form_validation->set_value('username'));
												
            $this->data['password'] = array('name' => 'password', 
												'type' => 'password', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Password', 
												'placeholder' => 'Password', 
												'value' => $this->form_validation->set_value('password'));		
            $this->data['passconf'] = array('name' => 'passconf', 
												'type' => 'password', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Confirm Password', 
												'placeholder' => 'Confirm Password', 
												'value' => $this->form_validation->set_value('passconf'));

            $this->data['first_name'] = array('name' => 'first_name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'First Name', 
												'placeholder' => 'First Name', 
												'value' => $this->form_validation->set_value('first_name'));

            $this->data['last_name'] = array('name' => 'last_name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Last Name', 
												'placeholder' => 'Last Name', 
												'value' => $this->form_validation->set_value('last_name'));

            $this->data['email'] = array('name' => 'email', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Email', 
												'placeholder' => 'Email', 
												'value' => $this->form_validation->set_value('email'));

            $this->data['phone'] = array('name' => 'phone', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Phone', 
												'placeholder' => 'Phone', 
												'value' => $this->form_validation->set_value('phone'));
												
            //load content
			$this->data["selected_group"] = $this->input->post('group_id');
				
			$this->data['submenu'] = "Add User";
			$this->data['active_menu'] = "users";
			$this->view("data_master/v_users_create", $this->data);
        }
	}
	
	public function users_edit($id = 0)
	{
		$reject_user = !$this->is_has_access("users");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_users");
		$this->load->model("m_groups");
		$this->load->model("m_users_groups");
		
		$this->data["is_edit"] = true;
		$this->data["users_groups"] = false;
		$this->data["groups_list"] = array();
		$users_groups = $this->m_users_groups->get_by($this->user->id, "user_id");
		if($users_groups){
			$this->data["users_groups"] = $users_groups;
			$this->data["groups_list"] = $this->m_groups->get_groups_dropdown($users_groups->group_id);
		}
		
		$form_data = $this->m_users->get_one($id);
		
		$this->data["form_data"] = $form_data;
		$edit_user_group = false;
		
		if(!$form_data){
			$reject_user = true;
		} else {
			$edited_group = array_keys($this->data["groups_list"], $form_data->company);
			$editor_group = array_keys($this->data["groups_list"], $this->user->company);
			if(!isset($edited_group[0]) || !isset($editor_group[0]) || ($edited_group[0] < $editor_group[0])){
				$reject_user = true;
			} else {
				$reject_user = false;
				$edit_user_group = $edited_group[0];
			}
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$edit_password = false;
		if($this->input->post('oldpassword')){
			$edit_password = true;
			$this->form_validation->set_rules('oldpassword', 'Old Password', 'required|xss_clean|min_length[1]|max_length[25]');
			$this->form_validation->set_rules('password', 'Password', 'required|xss_clean|min_length[1]|max_length[25]|matches[passconf]');
			$this->form_validation->set_rules('passconf', 'Confirm Password', 'required|xss_clean|min_length[1]|max_length[25]');
		}
		//validate form input
		if($form_data->username != $this->input->post('username')){
			$this->form_validation->set_rules('username', 'Username', 'required|xss_clean|min_length[1]|max_length[25]|is_unique[users.username]');
		}
        //$this->form_validation->set_rules('username', 'Username', 'required|xss_clean|min_length[1]|max_length[25]|is_unique[users.username]');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean|min_length[1]|max_length[25]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean|min_length[1]|max_length[25]');
		$this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean|min_length[1]|max_length[20]');
		$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|min_length[1]|max_length[25]|valid_email');
		
        if ($this->form_validation->run() == true) {						
			$username = $this->input->post('username');
			$oldpassword = $this->input->post('oldpassword');
			$password = $this->input->post('password');
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$phone = $this->input->post('phone');
			$email = $this->input->post('email');
			$group_id = $this->input->post('group_id');
			
			if(!isset($this->data["groups_list"][$group_id])){
				foreach($this->data["groups_list"] as $key => $value){
					$group_id = $key;
					break;
				}
			}
			
			$data_input = array(
				"username" => $username,
				"first_name" => $first_name,
				"last_name" => $last_name,
				"phone" => $phone,
				"email" => $email,
				"password_original" => $password,
				"company" => (isset($this->data["groups_list"][$group_id]) ? $this->data["groups_list"][$group_id] : NULL)
			);	
			
			if($edit_password){			
				$validate_user = $this->m_users->validate_user($username, $oldpassword);
				
				if(!$validate_user->status){
					$this->session->set_flashdata('message', $validate_user->message);
					redirect('main/users_edit/'.$id, 'refresh');
				} else {
					$data_input["password"] = $this->encrypt_password($password);
				}
			}
			
			$save = $this->m_users->save($data_input, $form_data->id);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed edit user');
			}
			else {
				$conditon = array("user_id" => $save);
				$save_group = $this->m_users_groups->get_by($save, "user_id");
				$group_result_id = 0;
				if($save_group){
					$group_result_id = $save_group->id;
				}
				$data_input = array(
					"user_id" => $save,
					"group_id" => $group_id,
				);
				$save_group = $this->m_users_groups->save($data_input,$group_result_id);
				if($save_group){
					$this->session->set_flashdata('message_success', 'Success edit '.$username.'!');
				} else {
					$this->session->set_flashdata('message', 'Failed edit user');
				}
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/users', 'refresh');
            }
            else {
                redirect('main/users_edit/'.$id, 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['username'] = array('name' => 'username', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												//'disabled' => 'disabled', 
												'field-name' => 'Username', 
												'placeholder' => 'Username', 
												'value' => $this->form_validation->set_value('username', $form_data->username));
												
            $this->data['oldpassword'] = array('name' => 'oldpassword', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Old Password', 
												'placeholder' => 'Old Password', 
												'value' => $this->form_validation->set_value('oldpassword',$form_data->password_original));
												
            $this->data['password'] = array('name' => 'password', 
												'type' => 'password', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Password', 
												'placeholder' => 'Password', 
												'value' => $this->form_validation->set_value('password'));
												
            $this->data['passconf'] = array('name' => 'passconf', 
												'type' => 'password', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Confirm Password', 
												'placeholder' => 'Confirm Password', 
												'value' => $this->form_validation->set_value('passconf'));

            $this->data['first_name'] = array('name' => 'first_name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'First Name', 
												'placeholder' => 'First Name', 
												'value' => $this->form_validation->set_value('first_name', $form_data->first_name));

            $this->data['last_name'] = array('name' => 'last_name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Last Name', 
												'placeholder' => 'Last Name', 
												'value' => $this->form_validation->set_value('last_name', $form_data->last_name));

            $this->data['email'] = array('name' => 'email', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Email', 
												'placeholder' => 'Email', 
												'value' => $this->form_validation->set_value('email', $form_data->email));

            $this->data['phone'] = array('name' => 'phone', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Phone', 
												'placeholder' => 'Phone', 
												'value' => $this->form_validation->set_value('phone', $form_data->phone));
												
            //load content
			$this->data["selected_group"] = ($this->input->post('group_id') ? $this->input->post('group_id') : $edit_user_group);

			$this->data['submenu'] = "Edit User";
			$this->data['active_menu'] = "users";
			$this->view("data_master/v_users_create", $this->data);
        }
	}
	
	public function users_delete($id = 0)
	{
		$reject_user = !$this->is_has_access("users");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_users");
		$user = $this->m_users->get_one($id);
		if($user){
			if($id == $this->user->id){
				$this->session->set_flashdata("message","You can't delete yourself");
			} else {
				$column_updated = array(
					"active" => 0,
					"updated_at" => date("Y-m-d"),
					"updated_by" => $this->user->id,
				);
				$saved = $this->m_users->save($column_updated,$user->id);
				if($saved){
					$this->session->set_flashdata("message_success","User Deleted!");
				} else {
					$this->session->set_flashdata("message","Delete User Failed!");
				}
			}
		} else {
			$this->session->set_flashdata("message","Invalid User!");
		}
		
		redirect("main/users");
	}
	
	public function patients()
	{
		$reject_user = !$this->is_has_access("patients");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "scripts/data-master/patients.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		
		$this->data['active_menu'] = "patients";
        $this->view("data_master/v_patients", $this->data);
	}
	
	public function patients_create()
	{
		$reject_user = !$this->is_has_access("patients_disabled");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_patient");
		
		$this->data["is_edit"] = false;
		$this->data["groups_list"] = _get_gender_dropdown();
		
		//validate form input
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|min_length[1]|max_length[25]');
        $this->form_validation->set_rules('address', 'Address', 'required|xss_clean|min_length[1]|max_length[255]');
        $this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean|min_length[1]|max_length[20]');
        $this->form_validation->set_rules('gender', 'Gender', 'required|xss_clean|min_length[1]|max_length[1]');
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			$address = $this->input->post('address');
			$phone = $this->input->post('phone');
			$gender = $this->input->post('gender');
			
			if(!isset($this->data["groups_list"][$gender])){
				foreach($this->data["groups_list"] as $key => $value){
					$gender = $key;
					break;
				}
			}
			$data_input = array(
				"name" => $name,
				"address" => $address,
				"phone" => $phone,
				"gender" => $gender,
				"created_at" => date("Y-m-d"),
				"created_by" => $this->user->id,
			);		
			
			$save = $this->m_patient->save($data_input);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed save Pasien');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success save Pasien '.$name.'!');
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/patients', 'refresh');
            }
            else {
                redirect('main/patients_create/', 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Name', 
												'value' => $this->form_validation->set_value('name'));
												
            $this->data['address'] = array('name' => 'address', 
												'type' => 'textarea', 
												'rows' => '5', 
												'maxlength' => '500', 
												'class' => 'form-control disable-resize requiredTextField', 
												'field-name' => 'Address', 
												'placeholder' => 'Address', 
												'value' => $this->form_validation->set_value('address'));
												
            $this->data['phone'] = array('name' => 'phone', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Phone', 
												'placeholder' => 'Phone', 
												'value' => $this->form_validation->set_value('phone'));
												
            //load content
			$this->data["selected_group"] = $this->input->post('gender');
				
			$this->data['submenu'] = "Add Pasien";
			$this->data['active_menu'] = "patients";
			$this->view("data_master/v_patients_create", $this->data);
        }
	}
	
	public function patients_edit($id = 0)
	{
		$reject_user = !$this->is_has_access("patients");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_patient");
		
		$this->data["is_edit"] = true;
		$this->data["groups_list"] = _get_gender_dropdown();
		
		$form_data = $this->m_patient->get_one($id);
		$this->data["form_data"] = $form_data;
		$reject_user = false;
		
		if(!$form_data){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		//validate form input
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|min_length[1]|max_length[25]');
        $this->form_validation->set_rules('address', 'Address', 'required|xss_clean|min_length[1]|max_length[255]');
        $this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean|min_length[1]|max_length[20]');
        $this->form_validation->set_rules('gender', 'Gender', 'required|xss_clean|min_length[1]|max_length[1]');
        $this->form_validation->set_rules('dob', 'Date of Birth', 'required|xss_clean|min_length[1]|max_length[10]');
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			$address = $this->input->post('address');
			$phone = $this->input->post('phone');
			$gender = $this->input->post('gender');
			$dob = $this->input->post('dob');
			
			if(!isset($this->data["groups_list"][$gender])){
				foreach($this->data["groups_list"] as $key => $value){
					$gender = $key;
					break;
				}
			}
			$data_input = array(
				"name" => $name,
				"address" => $address,
				"phone" => $phone,
				"gender" => $gender,
				"dob" => $dob,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);
			
			$condition = Array(
				"name" => $name,
				"address" => $address,
				"dob" => $dob,
				"status" => 1,
				"id !=" => $form_data->id
			);
			$data_patient = $this->m_patient->get_where($condition);
			if(!$data_patient){
				$save = $this->m_patient->save($data_input, $form_data->id);

				if ($save === false) {
					$this->session->set_flashdata('message', 'Failed edit Pasien');
				}
				else {
					$this->session->set_flashdata('message_success', 'Success edit Pasien '.$name.'!');
				}
			}else{
				$this->session->set_flashdata('message', 'Failed edit Pasien, Pasien already exist');
			}
			
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/patients', 'refresh');
            }
            else {
                redirect('main/patients_edit/'.$id, 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Name', 
												'value' => $this->form_validation->set_value('name', $form_data->name));
												
            $this->data['address'] = array('name' => 'address', 
												'type' => 'textarea', 
												'rows' => '5', 
												'maxlength' => '500', 
												'class' => 'form-control disable-resize requiredTextField', 
												'field-name' => 'Address', 
												'placeholder' => 'Address', 
												'value' => $this->form_validation->set_value('address', $form_data->address));
												
            $this->data['phone'] = array('name' => 'phone', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Phone', 
												'placeholder' => 'Phone', 
												'value' => $this->form_validation->set_value('phone', $form_data->phone));
								
											
			$this->data['dob'] = array('name' => 'dob', 
											'type' => 'text', 
											'class' => 'form-control combodate-input requiredTextField', 
											'field-name' => 'Date of Birth', 
											'data-format' => 'YYYY-MM-DD', 
											'data-template' => 'D MMMM YYYY',
											'value' => $this->form_validation->set_value('phone', $form_data->dob));				
            //load content
			$this->data["selected_group"] = $this->form_validation->set_value('gender', $form_data->gender);
				
			$this->data['submenu'] = "Edit Pasien";
			$this->data['active_menu'] = "patients";
			$this->data['scripts'][] = "plugins/moment/moment.min.js";
			$this->data['scripts'][] = "plugins/combodate-1.0.7/combodate.js";
			$this->data['scripts'][] = "scripts/data-master/patientsEdit.js";
			$this->view("data_master/v_patients_create", $this->data);
        }
	}
	
	public function patients_delete($id = 0)
	{
		$reject_user = !$this->is_has_access("patients");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_patient");
		$patient = $this->m_patient->get_one($id);
		if($patient){
			$column_updated = array(
				"status" => 0,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);
			$saved = $this->m_patient->save($column_updated,$patient->id);
			if($saved){
				$this->session->set_flashdata("message_success","Pasien Deleted!");
			} else {
				$this->session->set_flashdata("message","Delete Pasien Failed!");
			}
		} else {
			$this->session->set_flashdata("message","Invalid Pasien!");
		}
		
		redirect("main/patients");
	}
	
	public function doctors()
	{
		$reject_user = !$this->is_has_access("doctors");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "scripts/data-master/doctors.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		
		$this->data['active_menu'] = "doctors";
        $this->view("data_master/v_doctors", $this->data);
	}
	
	public function doctors_create()
	{
		$reject_user = !$this->is_has_access("doctors");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_doctor");
		
		$this->data["is_edit"] = false;
		$this->data["groups_list"] = _get_gender_dropdown();
		
		//validate form input
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|min_length[1]');
        $this->form_validation->set_rules('address', 'Address', 'required|xss_clean|min_length[1]');
        $this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean|min_length[1]|max_length[20]');
        $this->form_validation->set_rules('service', 'Jasa', 'required|xss_clean|numeric');
        //$this->form_validation->set_rules('gender', 'Gender', 'required|xss_clean|min_length[1]|max_length[1]');
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			$address = $this->input->post('address');
			$phone = $this->input->post('phone');
			$service = $this->input->post('service');
			//$gender = $this->input->post('gender');
			$gender = 'L';
			
			if(!isset($this->data["groups_list"][$gender])){
				foreach($this->data["groups_list"] as $key => $value){
					$gender = $key;
					break;
				}
			}
			$data_input = array(
				"name" => $name,
				"address" => $address,
				"phone" => $phone,
				"gender" => $gender,
				"service" => $service,
				"created_at" => date("Y-m-d"),
				"created_by" => $this->user->id,
			);

			$condition = Array(
				"name" => $name,
				"address" => $address,
				"status" => 1
			);
			$data_doctor = $this->m_doctor->get_where($condition);
			if(!$data_doctor){
				$save = $this->m_doctor->save($data_input);

				if ($save === false) {
					$this->session->set_flashdata('message', 'Failed save Dokter');
				}
				else {
					$this->session->set_flashdata('message_success', 'Success save Dokter '.$name.'!');
				}
			}else{
				$this->session->set_flashdata('message', 'Failed save Dokter, Name Already exist');
			}
			
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/doctors', 'refresh');
            }
            else {
                redirect('main/doctors_create/', 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Name', 
												'value' => $this->form_validation->set_value('name'));
												
            $this->data['address'] = array('name' => 'address', 
												'type' => 'textarea', 
												'rows' => '5', 
												'maxlength' => '500', 
												'class' => 'form-control disable-resize requiredTextField', 
												'field-name' => 'Address', 
												'placeholder' => 'Address', 
												'value' => $this->form_validation->set_value('address'));
												
            $this->data['phone'] = array('name' => 'phone', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Phone', 
												'placeholder' => 'Phone', 
												'value' => $this->form_validation->set_value('phone'));
												
			$this->data['service'] = array('name' => 'service', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Jasa', 
												'placeholder' => 'Jasa (%)', 
												'value' => $this->form_validation->set_value('service'));
												
            //load content
			//$this->data["selected_group"] = $this->input->post('gender');
				
			$this->data['submenu'] = "Add Dokter";
			$this->data['active_menu'] = "doctors";
			$this->view("data_master/v_doctors_create", $this->data);
        }
	}
	
	public function doctors_edit($id = 0)
	{
		$reject_user = !$this->is_has_access("doctors");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_doctor");
		
		$this->data["is_edit"] = true;
		$this->data["groups_list"] = _get_gender_dropdown();
		
		$form_data = $this->m_doctor->get_one($id);
		$this->data["form_data"] = $form_data;
		$reject_user = false;
		
		if(!$form_data){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		//validate form input
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|min_length[1]');
        $this->form_validation->set_rules('address', 'Address', 'required|xss_clean|min_length[1]');
        $this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean|min_length[1]|max_length[20]');
        //$this->form_validation->set_rules('gender', 'Gender', 'required|xss_clean|min_length[1]|max_length[1]');
		$this->form_validation->set_rules('service', 'Jasa', 'required|xss_clean|numeric');
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			$address = $this->input->post('address');
			$phone = $this->input->post('phone');
			$service = $this->input->post('service');
			//$gender = $this->input->post('gender');
			$gender = "L";
			
			if(!isset($this->data["groups_list"][$gender])){
				foreach($this->data["groups_list"] as $key => $value){
					$gender = $key;
					break;
				}
			}
			$data_input = array(
				"name" => $name,
				"address" => $address,
				"phone" => $phone,
				"gender" => $gender,
				"service" => $service,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);		
			
			$condition = Array(
				"name" => $name,
				"address" => $address,
				"status" => 1,
				"id !=" => $form_data->id
			);
			$data_doctor = $this->m_doctor->get_where($condition);
			if(!$data_doctor){
				$save = $this->m_doctor->save($data_input, $form_data->id);
				if ($save === false) {
					$this->session->set_flashdata('message', 'Failed edit Dokter');
				}
				else {
					$this->session->set_flashdata('message_success', 'Success edit Dokter '.$name.'!');
				}
			}else{
				$this->session->set_flashdata('message', 'Failed edit Dokter, Name already exist');
			}
			
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/doctors', 'refresh');
            }
            else {
                redirect('main/doctors_edit/'.$id, 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Name', 
												'value' => $this->form_validation->set_value('name', $form_data->name));
												
            $this->data['address'] = array('name' => 'address', 
												'type' => 'textarea', 
												'rows' => '5', 
												'maxlength' => '500', 
												'class' => 'form-control disable-resize requiredTextField', 
												'field-name' => 'Address', 
												'placeholder' => 'Address', 
												'value' => $this->form_validation->set_value('address', $form_data->address));
												
            $this->data['phone'] = array('name' => 'phone', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Phone', 
												'placeholder' => 'Phone', 
												'value' => $this->form_validation->set_value('phone', $form_data->phone));
												
			$this->data['service'] = array('name' => 'service', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Jasa', 
												'placeholder' => 'Jasa (%)', 
												'value' => $this->form_validation->set_value('service',$form_data->service));
												
            //load content
			//$this->data["selected_group"] = $this->form_validation->set_value('gender', $form_data->gender);
				
			$this->data['submenu'] = "Edit Dokter";
			$this->data['active_menu'] = "doctors";
			$this->view("data_master/v_doctors_create", $this->data);
        }
	}
	
	public function doctors_delete($id = 0)
	{
		$reject_user = !$this->is_has_access("doctors");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_doctor");
		$doctor = $this->m_doctor->get_one($id);
		if($doctor){
			$column_updated = array(
				"status" => 0,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);
			$saved = $this->m_doctor->save($column_updated,$doctor->id);
			if($saved){
				$this->session->set_flashdata("message_success","Dokter Deleted!");
			} else {
				$this->session->set_flashdata("message","Delete Dokter Failed!");
			}
		} else {
			$this->session->set_flashdata("message","Invalid Dokter!");
		}
		
		redirect("main/doctors");
	}
	
	public function subexaminations()
	{
		$reject_user = !$this->is_has_access("subexaminations");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->load->model("m_examination");
		$exam_id = $this->uri->segment(3);
		if($exam_id == null){
			redirect('main/examinations');
		}
		
		$exam_data = $this->m_examination->get_one($exam_id);
		
		if($exam_data == null){
			redirect('main/examinations');
		}
		
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "scripts/data-master/subexaminations.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		
		$this->data['examinations_data'] = $exam_data;

		$this->data['active_menu'] = "subexaminations";
        $this->view("data_master/v_subexaminations", $this->data);
	}
	
	public function subexaminations_create()
	{
		$reject_user = !$this->is_has_access("subexaminations");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		
		$exam_id = $this->uri->segment(3);
		if($exam_id == null){
			redirect('main/examinations');
		}
		
		$exam_data = $this->m_examination->get_one($exam_id);
		if($exam_data == null){
			redirect('main/examinations');
		}else if($exam_data->parent_id != 0){
			redirect('main/subexaminations/'.$exam_data->id);
		}
		
		$this->data["is_edit"] = false;
		$this->data["groups_list"] = $this->m_examination_category->get_category_dropdown();
		$type = $this->input->post('type');
		if($type == null){
			$type = $exam_data->type;
		}
		$this->data["type"] = $type;
		$this->data["parent_id"] = $exam_id;
		//validate form input
        $this->form_validation->set_rules('name', 'Uraian', 'required|xss_clean|min_length[1]|max_length[25]');
        if($type == 1){
			$this->form_validation->set_rules('unit', 'Satuan', 'required|xss_clean|min_length[1]|max_length[25]');
			$this->form_validation->set_rules('normal_value', 'Normal Value', 'required|xss_clean|min_length[1]|max_length[25]');
		}
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			$fare = 0;
			if($type == 1){
				$unit = $this->input->post('unit');
				$normal_value = $this->input->post('normal_value');
				
			}else{
				$unit = null;
				$normal_value = null;
			}
			
			$data_input = array(
				"examination_category_id" => $exam_data->examination_category_id,
				"parent_id" => $exam_data->id,
				"name" => $name,
				"normal_value" => $normal_value,
				"unit" => $unit,
				"fare" => $fare,
				"status" => 1,
				"type" => $type,
				"created_at" => date("Y-m-d"),
				"created_by" => $this->user->id,
			);		
			$save = $this->m_examination->save($data_input);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed save Sub Pemeriksaan');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success save Sub Pemeriksaan '.$name.'!');
			}
			
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/subexaminations/'.$exam_data->id, 'refresh');
            }
            else {
                redirect('main/subexaminations_create/'.$exam_data->id, 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');
			//$this->data['examinations_data'] = $exam_data;
            $this->data['parent_name'] = array('name' => 'parent_name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Jenis Pemeriksaan', 
												'disabled' => 'disabled',
												'value' => $exam_data->name);
			$this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Uraian',
												'value' => $this->form_validation->set_value('name'));
												
            $this->data['normal_value'] = array('name' => 'normal_value', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Normal Value', 
												'placeholder' => 'Normal Value', 
												'value' => $this->form_validation->set_value('normal_value'));
												
            $this->data['unit'] = array('name' => 'unit', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Unit', 
												'placeholder' => 'Satuan', 
												'value' => $this->form_validation->set_value('unit'));												
            //load content
			$this->data["selected_group"] = $exam_data->examination_category_id;
			$this->data['scripts'][] = "scripts/data-master/subexaminations_create.js";
			$this->data['submenu'] = "Add Sub Pemeriksaan";
			$this->data['active_menu'] = "subexaminations";
			$this->view("data_master/v_subexaminations_create", $this->data);
        }
	}
	
	public function subexaminations_edit()
	{
		$reject_user = !$this->is_has_access("examinations");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		
		$exam_id = $this->uri->segment(3);
		if($exam_id == null){
			redirect('main/examinations');
		}
		
		$form_data = $this->m_examination->get_one($exam_id);
		if($form_data == null){
			redirect('main/examinations');
		}else if($form_data->parent_id == 0){
			redirect('main/subexaminations/'.$form_data->id);
		}
		
		$this->data["is_edit"] = true;
		$this->data["groups_list"] = $this->m_examination_category->get_category_dropdown();
		
		$this->data["form_data"] = $form_data;
		$reject_user = false;
		
		if(!$form_data){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$type = $this->input->post('type');
		if($type == null){
			$type = $form_data->type;
		}
		$this->data["type"] = $type;
		$this->data["parent_id"] = $form_data->parent_id;
		$this->form_validation->set_rules('name', 'Uraian', 'required|xss_clean|min_length[1]|max_length[25]');
		if($type == 1){
			$this->form_validation->set_rules('unit', 'Unit', 'required|xss_clean|min_length[1]|max_length[25]');
			$this->form_validation->set_rules('normal_value', 'Normal Value', 'required|xss_clean|min_length[1]|max_length[25]');
		}
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			$fare = 0;
			if($type == 1){
				$unit = $this->input->post('unit');
				$normal_value = $this->input->post('normal_value');
			}else{
				$unit = null;
				$normal_value = null;
			}
			
			$data_input = array(
				"name" => $name,
				"normal_value" => $normal_value,
				"unit" => $unit,
				"type" => $type,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);		
			
			$save = $this->m_examination->save($data_input, $form_data->id);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed edit Sub Pemeriksaan');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success edit Sub Pemeriksaan '.$name.'!');
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/subexaminations/'.$exam_id, 'refresh');
            }
            else {
                redirect('main/subexaminations_edit/'.$exam_id, 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');
			$form_data_parent = $this->m_examination->get_one($form_data->parent_id);
            $this->data['parent_name'] = array('name' => 'parent_name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Jenis Pemeriksaan', 
												'disabled' => 'disabled',
												'value' => $form_data_parent->name);
												
			$this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Uraian', 
												'placeholder' => 'Uraian', 
												'value' => $this->form_validation->set_value('name', $form_data->name));
												
            $this->data['normal_value'] = array('name' => 'normal_value', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Normal Value', 
												'placeholder' => 'Normal Value', 
												'value' => $this->form_validation->set_value('normal_value', $form_data->normal_value));
												
            $this->data['unit'] = array('name' => 'unit', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Unit', 
												'placeholder' => 'Unit', 
												'value' => $this->form_validation->set_value('unit', $form_data->unit));
												
            //load content
			$this->data["selected_group"] = $form_data->examination_category_id;
			
			$this->data['scripts'][] = "scripts/data-master/subexaminations_create.js";
			$this->data['submenu'] = "Edit Sub Pemeriksaan";
			$this->data['active_menu'] = "subexaminations";
			$this->view("data_master/v_subexaminations_create", $this->data);
        }
	}
	
	public function subexaminations_delete()
	{
		$reject_user = !$this->is_has_access("examinations");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_examination");
		$exam_id = $this->uri->segment(3);
		
		if($exam_id == null){
			redirect('main/examinations');
		}
		
		$examination = $this->m_examination->get_one($exam_id);
		
		if($examination == null){
			redirect('main/examinations');
		}
		
		if($examination){
			$column_updated = array(
				"status" => 0,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);
			$saved = $this->m_examination->save($column_updated,$examination->id);
			if($saved){
				$this->session->set_flashdata("message_success","Sub Pemeriksaan Deleted!");
			} else {
				$this->session->set_flashdata("message","Delete Sub Pemeriksaan Failed!");
			}
		} else {
			$this->session->set_flashdata("message","Invalid Sub Pemeriksaan!");
		}
		
		redirect("main/subexaminations/".$examination->parent_id);
	}
	
	public function examinations()
	{
		$reject_user = !$this->is_has_access("examinations");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "scripts/data-master/examinations.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		
		$this->data['active_menu'] = "examinations";
        $this->view("data_master/v_examinations", $this->data);
	}
	
	public function examinations_create()
	{
		$reject_user = !$this->is_has_access("examinations");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		
		$this->data["is_edit"] = false;
		$this->data["groups_list"] = $this->m_examination_category->get_category_dropdown();
		
		$type = $this->input->post('type');
		
        if($type == 1){
			$this->form_validation->set_rules('unit', 'Satuan', 'required|xss_clean|min_length[1]|max_length[25]');
			$this->form_validation->set_rules('normal_value', 'Normal Value', 'required|xss_clean|min_length[1]|max_length[25]');
		}
		
		//validate form input
        $this->form_validation->set_rules('name', 'Jenis Pemeriksaan', 'required|xss_clean|min_length[1]|max_length[25]');
        $this->form_validation->set_rules('fare', 'Harga', 'required|xss_clean|min_length[1]|numeric|max_length[25]');
        if($type == 1){
			$this->form_validation->set_rules('unit', 'Satuan', 'xss_clean|min_length[1]|max_length[25]');
			$this->form_validation->set_rules('normal_value', 'Normal Value', 'xss_clean|min_length[1]|max_length[25]');
		}
        $this->form_validation->set_rules('examination_category_id', 'Kelompok', 'required|xss_clean|min_length[1]|max_length[1]');
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			$fare = $this->input->post('fare');
			if($type == 1){
				$unit = $this->input->post('unit');
				$normal_value = $this->input->post('normal_value');
			}else{
				$unit = null;
				$normal_value = null;
			}
			$examination_category_id = $this->input->post('examination_category_id');
			
			if(!isset($this->data["groups_list"][$examination_category_id])){
				foreach($this->data["groups_list"] as $key => $value){
					$examination_category_id = $key;
					break;
				}
			}
			$data_input = array(
				"examination_category_id" => $examination_category_id,
				"parent_id" => 0,
				"name" => $name,
				"normal_value" => $normal_value,
				"unit" => $unit,
				"fare" => $fare,
				"status" => 1,
				"type" => $type,
				"created_at" => date("Y-m-d"),
				"created_by" => $this->user->id,
			);	
			
			$save = $this->m_examination->save($data_input);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed save Pemeriksaan');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success save Pemeriksaan '.$name.'!');
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/examinations', 'refresh');
            }
            else {
                redirect('main/examinations_create/', 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Jenis Pemeriksaan', 
												'value' => $this->form_validation->set_value('name'));
												
            $this->data['normal_value'] = array('name' => 'normal_value', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Normal Value', 
												'placeholder' => 'Normal Value', 
												'value' => $this->form_validation->set_value('normal_value'));
												
            $this->data['unit'] = array('name' => 'unit', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Unit', 
												'placeholder' => 'Satuan', 
												'value' => $this->form_validation->set_value('unit'));
												
            $this->data['fare'] = array('name' => 'fare', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Fare', 
												'placeholder' => 'Harga', 
												'value' => $this->form_validation->set_value('fare'));
												
            //load content
			$this->data["selected_group"] = $this->input->post('examination_category_id');
			
			$this->data['scripts'][] = "scripts/data-master/subexaminations_create.js";
				
			$this->data['submenu'] = "Add Pemeriksaan";
			$this->data['active_menu'] = "examinations";
			$this->view("data_master/v_examinations_create", $this->data);
        }
	}
	
	public function examinations_edit($id = 0)
	{
		$reject_user = !$this->is_has_access("examinations");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_examination");
		$this->load->model("m_examination_category");
		
		$this->data["is_edit"] = true;
		$this->data["groups_list"] = $this->m_examination_category->get_category_dropdown();
		
		$form_data = $this->m_examination->get_one($id);
		
		$this->data["form_data"] = $form_data;
		$reject_user = false;
		
		if(!$form_data){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$type = $this->input->post('type');
		if($type == null){
			$type = $form_data->type;
		}
		$this->data["type"] = $type;
		
		
		//validate form input
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|min_length[1]|max_length[25]');
        $this->form_validation->set_rules('fare', 'Fare', 'required|xss_clean|min_length[1]|numeric|max_length[25]');
		if($type == 1){
			$this->form_validation->set_rules('unit', 'Unit', 'xss_clean|min_length[1]|max_length[25]');
			$this->form_validation->set_rules('normal_value', 'Normal Value', 'xss_clean|min_length[1]|max_length[25]');
		}
        
        $this->form_validation->set_rules('examination_category_id', 'Category', 'required|xss_clean|min_length[1]|max_length[1]');
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			$fare = $this->input->post('fare');
			if($type == 1){
				$unit = $this->input->post('unit');
				$normal_value = $this->input->post('normal_value');
			}else{
				$unit = null;
				$normal_value = null;
			}
			$examination_category_id = $this->input->post('examination_category_id');
			
			if(!isset($this->data["groups_list"][$examination_category_id])){
				foreach($this->data["groups_list"] as $key => $value){
					$examination_category_id = $key;
					break;
				}
			}
			$data_input = array(
				"examination_category_id" => $examination_category_id,
				"name" => $name,
				"normal_value" => $normal_value,
				"unit" => $unit,
				"fare" => $fare,
				"type" => $type,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);		
			$save = $this->m_examination->save($data_input, $form_data->id);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed edit Pemeriksaan');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success edit Pemeriksaan '.$name.'!');
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/examinations', 'refresh');
            }
            else {
                redirect('main/examinations_edit/'.$id, 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Name', 
												'value' => $this->form_validation->set_value('name', $form_data->name));
												
            $this->data['normal_value'] = array('name' => 'normal_value', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Normal Value', 
												'placeholder' => 'Normal Value', 
												'value' => $this->form_validation->set_value('normal_value', $form_data->normal_value));
												
            $this->data['unit'] = array('name' => 'unit', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Unit', 
												'placeholder' => 'Unit', 
												'value' => $this->form_validation->set_value('unit', $form_data->unit));
												
            $this->data['fare'] = array('name' => 'fare', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Fare', 
												'placeholder' => 'Fare', 
												'value' => $this->form_validation->set_value('fare', $form_data->fare));
												
            //load content
			$this->data["selected_group"] = $this->form_validation->set_value('examination_category_id', $form_data->examination_category_id);
			$this->data['submenu'] = "Edit Pemeriksaan";
			$this->data['active_menu'] = "examinations";
			$this->data['scripts'][] = "scripts/data-master/subexaminations_create.js";
			$this->view("data_master/v_examinations_create", $this->data);
        }
	}
	
	public function examinations_delete($id = 0)
	{
		$reject_user = !$this->is_has_access("examinations");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_examination");
		$examination = $this->m_examination->get_one($id);
		if($examination){
			$column_updated = array(
				"status" => 0,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);
			$saved = $this->m_examination->save($column_updated,$examination->id);
			if($saved){
				$this->session->set_flashdata("message_success","Pemeriksaan Deleted!");
			} else {
				$this->session->set_flashdata("message","Delete Pemeriksaan Failed!");
			}
		} else {
			$this->session->set_flashdata("message","Invalid Pemeriksaan!");
		}
		
		redirect("main/examinations");
	}
	
	public function employees()
	{
		$reject_user = !$this->is_has_access("employees");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "scripts/data-master/employees.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		
		$this->data['active_menu'] = "employees";
        $this->view("data_master/v_employees", $this->data);
	}
	
	public function employees_create()
	{
		$reject_user = !$this->is_has_access("employees");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_employee");
		
		$this->data["is_edit"] = false;
		//validate form input
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|min_length[1]|max_length[25]');
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			
			$data_input = array(
				"name" => $name,
				"created_at" => date("Y-m-d"),
				"created_by" => $this->user->id,
			);		
			
			$save = $this->m_employee->save($data_input);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed save Pegawai');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success save Pegawai '.$name.'!');
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/employees', 'refresh');
            }
            else {
                redirect('main/employees_create/', 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Name', 
												'value' => $this->form_validation->set_value('name'));
										
			$this->data['submenu'] = "Add Pegawai";
			$this->data['active_menu'] = "employees";
			$this->view("data_master/v_employees_create", $this->data);
        }
	}
	
	public function employees_edit($id = 0)
	{
		$reject_user = !$this->is_has_access("employees");
		
		$this->load->model("m_employee");
		
		$form_data = $this->m_employee->get_one($id);
		$this->data["form_data"] = $form_data;
		$reject_user = false;
		
		if(!$form_data){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		
		}
		$this->data["is_edit"] = true;
		//validate form input
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|min_length[1]|max_length[25]');
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			
			$data_input = array(
				"name" => $name,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);		
			
			$save = $this->m_employee->save($data_input, $id);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed edit Pegawai');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success edit Pegawai '.$name.'!');
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/employees', 'refresh');
            }
            else {
                redirect('main/employees_edit/'.$id, 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Name', 
												'value' => $this->form_validation->set_value('name', $form_data->name));
										
			$this->data['submenu'] = "Edit Pegawai";
			$this->data['active_menu'] = "employees";
			$this->view("data_master/v_employees_create", $this->data);
        }
	}
	
	
	public function employees_delete($id = 0)
	{
		$reject_user = !$this->is_has_access("employees");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_employee");
		$examination = $this->m_employee->get_one($id);
		if($examination){
			$column_updated = array(
				"status" => 0,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);
			$saved = $this->m_employee->save($column_updated,$examination->id);
			if($saved){
				$this->session->set_flashdata("message_success","Pegawai Deleted!");
			} else {
				$this->session->set_flashdata("message","Delete Pegawai Failed!");
			}
		} else {
			$this->session->set_flashdata("message","Invalid Pegawai!");
		}
		
		redirect("main/employees");
	}
	
	public function expense_employees()
	{
		$reject_user = !$this->is_has_access("expense_employees");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "scripts/expense/employees.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		
		$this->data['active_menu'] = "expense_employees";
        $this->view("expense/v_expense_employees", $this->data);
	}
	
	public function expense_employees_create()
	{
		$reject_user = !$this->is_has_access("expense_employees");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_salary");
		$this->load->model("m_employee");
		
		$this->data["is_edit"] = false;
		$this->data["groups_list"] = $this->m_employee->get_employee_dropdown();
		
		//validate form input
        $this->form_validation->set_rules('date', 'Tanggal', 'required|xss_clean|min_length[1]|max_length[25]');
        $this->form_validation->set_rules('basic_salary', 'Gaji Pokok', 'required|xss_clean|min_length[1]|max_length[25]|numeric');
        $this->form_validation->set_rules('employee_id', 'Pegawai', 'required|xss_clean|min_length[1]|max_length[25]|numeric');
		
        if ($this->form_validation->run() == true) {						
			$employee_id = $this->input->post('employee_id');
			$date = date("Y-m-d", strtotime($this->input->post('date')));
			$basic_salary = intval($this->input->post('basic_salary'));
			$incentive = intval($this->input->post('incentive'));
			$overtime = intval($this->input->post('overtime'));
			$rounding_off = intval($this->input->post('rounding_off'));
			$salary = $basic_salary + $incentive + $overtime + $rounding_off;
			$information = $this->input->post('information');
			
			$data_input = array(
				"employee_id" => $employee_id,
				"date" => $date,
				"basic_salary" => $basic_salary,
				"incentive" => $incentive,
				"overtime" => $overtime,
				"rounding_off" => $rounding_off,
				"salary" => $salary,
				"information" => $information,
			);		
			
			$condition = array(
				"employee_id" => $employee_id,
				"date" => $date,
			);
			
			$salary = $this->m_salary->get_where($condition, 1);
			$salary_id = 0;
			/*if($salary && count($salary) > 0){
				$salary_id = $salary[0]->id;
				
				$data_input["status"] = 1;
				$data_input["updated_at"] = date("Y-m-d");
				$data_input["updated_by"] = $this->user->id;
			} else {
				$data_input["created_at"] = date("Y-m-d");
				$data_input["created_by"] = $this->user->id;
				
			}*/
			
			$data_input["created_at"] = date("Y-m-d");
			$data_input["created_by"] = $this->user->id;
			$save = $this->m_salary->save($data_input, $salary_id);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed save Gaji Pegawai');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success save Gaji Pegawai '.$this->data["groups_list"][$employee_id].'!');
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/expense_employees', 'refresh');
            }
            else {
                redirect('main/expense_employees_create/', 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['date'] = array('name' => 'date', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField datepicker-input', 
												'field-name' => 'date', 
												'placeholder' => 'Tanggal', 
												'value' => $this->form_validation->set_value('date', date("d-m-Y")));

            $this->data['basic_salary'] = array('name' => 'basic_salary', 
												'type' => 'text', 
												'id' => 'basic_salary', 
												'class' => 'form-control requiredTextField salaryCalculation numbersOnly', 
												'field-name' => 'basic_salary', 
												'placeholder' => 'Gaji Pokok', 
												'value' => $this->form_validation->set_value('basic_salary'));
			
			$this->data['incentive'] = array('name' => 'incentive', 
												'type' => 'text', 
												'id' => 'incentive', 
												'class' => 'form-control salaryCalculation numbersOnly', 
												'field-name' => 'incentive', 
												'placeholder' => 'Insentif', 
												'value' => $this->input->post('incentive'));
												
			$this->data['overtime'] = array('name' => 'overtime', 
												'type' => 'text', 
												'id' => 'overtime', 
												'class' => 'form-control salaryCalculation numbersOnly', 
												'field-name' => 'overtime', 
												'placeholder' => 'Lembur', 
												'value' => $this->input->post('overtime'));
												
			$this->data['rounding_off'] = array('name' => 'rounding_off', 
												'type' => 'text', 
												'id' => 'rounding_off', 
												'class' => 'form-control salaryCalculation numbersOnly', 
												'field-name' => 'rounding_off', 
												'placeholder' => 'Pembulatan', 
												'value' => $this->input->post('rounding_off'));
												
			$this->data['salary'] = array('name' => 'salary', 
												'type' => 'text', 
												'id' => 'salary', 
												'disabled' => 'disabled',
												'class' => 'form-control numbersOnly', 
												'field-name' => 'salary', 
												'placeholder' => 'Total Gaji', 
												'value' => intval($this->input->post('basic_salary')) + intval($this->input->post('incentive')) + intval($this->input->post('overtime')) + intval($this->input->post('rounding_off')));
												
            $this->data['information'] = array('name' => 'information', 
												'type' => 'textarea', 
												'rows' => '5', 
												'maxlength' => '500', 
												'class' => 'form-control disable-resize',
												'field-name' => 'information', 
												'placeholder' => 'Informasi', 
												'value' => $this->input->post('information'));
										
			$this->data["selected_group"] = $this->input->post('employee_id');
			$this->data['submenu'] = "Add Gaji Pegawai";
			$this->data['active_menu'] = "expense_employees";
			
			$this->data['scripts'][] = "plugins/moment/moment.min.js";
			$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
			$this->data['scripts'][] = "scripts/expense/employeesCreate.js";
			$this->data['styles'][] = "plugins/datepicker/datepicker3.css";
			
			$this->view("expense/v_expense_employees_create", $this->data);
        }
	}
	
	public function expense_employees_edit($id = 0)
	{
		$reject_user = !$this->is_has_access("expense_employees");
		
		$this->load->model("m_salary");
		
		$form_data = $this->m_salary->get_one($id);
		$this->data["form_data"] = $form_data;
		
		if(!$form_data){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		
		$this->load->model("m_employee");
		
		$this->data["is_edit"] = true;
		$this->data["groups_list"] = $this->m_employee->get_employee_dropdown();
		
		//validate form input
        $this->form_validation->set_rules('date', 'Tanggal', 'required|xss_clean|min_length[1]|max_length[25]');
        $this->form_validation->set_rules('basic_salary', 'Gaji Pokok', 'required|xss_clean|min_length[1]|max_length[25]|numeric');
        $this->form_validation->set_rules('employee_id', 'Pegawai', 'required|xss_clean|min_length[1]|max_length[25]|numeric');
		

	   if ($this->form_validation->run() == true) {						
			$employee_id = $this->input->post('employee_id');
			$date = date("Y-m-d", strtotime($this->input->post('date')));
			$basic_salary = intval($this->input->post('basic_salary'));
			$incentive = intval($this->input->post('incentive'));
			$overtime = intval($this->input->post('overtime'));
			$rounding_off = intval($this->input->post('rounding_off'));
			$salary = $basic_salary + $incentive + $overtime + $rounding_off;
			$information = $this->input->post('information');
			
			$data_input = array(
				"employee_id" => $employee_id,
				"date" => $date,
				"basic_salary" => $basic_salary,
				"incentive" => $incentive,
				"overtime" => $overtime,
				"rounding_off" => $rounding_off,
				"salary" => $salary,
				"information" => $information,
			);		
			
			$condition = array(
				"employee_id" => $employee_id,
				"date" => $date,
			);
			
			$salary = $this->m_salary->get_where($condition, 1);
			$salary_id = 0;
			if($salary && count($salary) > 0){
				$salary_id = $salary[0]->id;
				/*
				if($salary_id != $form_data->id){
					$this->m_salary->delete($salary_id);
				}*/
				
			} else {
				$data_input["status"] = 1;
				$data_input["updated_at"] = date("Y-m-d");
				$data_input["updated_by"] = $this->user->id;
				
			}
			
			$save = $this->m_salary->save($data_input, $form_data->id);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed edit Gaji Pegawai');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success edit Gaji Pegawai '.$this->data["groups_list"][$employee_id].'!');
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/expense_employees', 'refresh');
            }
            else {
                redirect('main/expense_employees_edit/'.$id, 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');
			$form_data_array = (array) $form_data;
			
            $this->data['date'] = array('name' => 'date', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField datepicker-input', 
												'field-name' => 'date', 
												'placeholder' => 'Tanggal', 
												'value' => $this->form_validation->set_value('date', date("d-m-Y", strtotime($form_data->date))));

			$this->data['basic_salary'] = array('name' => 'basic_salary', 
												'type' => 'text', 
												'id' => 'basic_salary', 
												'class' => 'form-control requiredTextField salaryCalculation numbersOnly', 
												'field-name' => 'basic_salary', 
												'placeholder' => 'Gaji Pokok', 
												'value' => $this->form_validation->set_value('basic_salary',$form_data->basic_salary));
			
			$this->data['incentive'] = array('name' => 'incentive', 
												'type' => 'text', 
												'id' => 'incentive', 
												'class' => 'form-control salaryCalculation numbersOnly', 
												'field-name' => 'incentive', 
												'placeholder' => 'Insentif', 
												'value' => $this->input->post('incentive') ? $this->input->post('incentive') : $form_data->incentive);
												
			$this->data['overtime'] = array('name' => 'overtime', 
												'type' => 'text', 
												'id' => 'overtime', 
												'class' => 'form-control salaryCalculation numbersOnly', 
												'field-name' => 'overtime', 
												'placeholder' => 'Lembur', 
												'value' => $this->input->post('overtime') ? $this->input->post('overtime') : $form_data->overtime);
												
			$this->data['rounding_off'] = array('name' => 'rounding_off', 
												'type' => 'text', 
												'id' => 'rounding_off', 
												'class' => 'form-control salaryCalculation numbersOnly', 
												'field-name' => 'rounding_off', 
												'placeholder' => 'Pembulatan', 
												'value' => $this->input->post('rounding_off') ? $this->input->post('rounding_off') : $form_data->rounding_off);
												
			$this->data['salary'] = array('name' => 'salary', 
												'type' => 'text', 
												'id' => 'salary', 
												'disabled' => 'disabled',
												'class' => 'form-control numbersOnly', 
												'field-name' => 'salary', 
												'placeholder' => 'Total Gaji', 
												'value' => $form_data->salary ? $form_data->salary : intval($this->input->post('basic_salary')) + intval($this->input->post('incentive')) + intval($this->input->post('overtime')) + intval($this->input->post('rounding_off')));
												
												
            $this->data['information'] = array('name' => 'information', 
												'type' => 'textarea', 
												'rows' => '5', 
												'maxlength' => '500', 
												'class' => 'form-control disable-resize',
												'field-name' => 'information', 
												'placeholder' => 'Informasi', 
												'value' => ($this->input->post('information') ? $this->input->post('information') : $form_data->information));
										
			$this->data["selected_group"] = ($this->input->post('employee_id') ? $this->input->post('employee_id') : $form_data->employee_id);
			$this->data['submenu'] = "Edit Gaji Pegawai";
			$this->data['active_menu'] = "expense_employees";
			
			$this->data['scripts'][] = "plugins/moment/moment.min.js";
			$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
			$this->data['scripts'][] = "scripts/expense/employeesCreate.js";
			$this->data['styles'][] = "plugins/datepicker/datepicker3.css";
			
			$this->view("expense/v_expense_employees_create", $this->data);
        }
	}
	
	
	public function expense_employees_delete($id = 0)
	{
		$reject_user = !$this->is_has_access("expense_employees");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_salary");
		$examination = $this->m_salary->get_one($id);
		if($examination){
			$column_updated = array(
				"status" => 0,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);
			$saved = $this->m_salary->save($column_updated,$examination->id);
			if($saved){
				$this->session->set_flashdata("message_success","Gaji Pegawai Deleted!");
			} else {
				$this->session->set_flashdata("message","Delete Gaji Pegawai Failed!");
			}
		} else {
			$this->session->set_flashdata("message","Invalid Gaji Pegawai!");
		}
		
		redirect("main/expense_employees");
	}
	
	public function expense_operational()
	{
		$reject_user = !$this->is_has_access("expense_operational");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->data['scripts'][] = "plugins/datatables/jquery.dataTables.min.js";
		$this->data['scripts'][] = "plugins/datatables/dataTables.bootstrap.min.js";
		$this->data['scripts'][] = "scripts/expense/operational.js";
		$this->data['styles'][] = "plugins/datatables/dataTables.bootstrap.css";
		
		$this->data['active_menu'] = "expense_operational";
        $this->view("expense/v_expense_operational", $this->data);
	}
	
	public function expense_operational_create()
	{
		$reject_user = !$this->is_has_access("employees");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_expense");
		
		$this->data["is_edit"] = false;
		//validate form input
       $this->form_validation->set_rules('name', 'Uraian', 'required|xss_clean|min_length[1]|max_length[25]');
        $this->form_validation->set_rules('date', 'Tanggal', 'required|xss_clean|min_length[1]|max_length[25]');
        $this->form_validation->set_rules('value', 'Nilai (Rp)', 'required|xss_clean|min_length[1]|max_length[25]|numeric');
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			$date = date("Y-m-d", strtotime($this->input->post('date')));
			$value = $this->input->post('value');
			
			$data_input = array(
				"name" => $name,
				"date" => $date,
				"value" => $value,
				"created_at" => date("Y-m-d"),
				"created_by" => $this->user->id,
			);		
			
			$save = $this->m_expense->save($data_input);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed save Pengeluaran');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success save pengeluaran '.$name.'!');
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/expense_operational', 'refresh');
            }
            else {
                redirect('main/expense_operational_create/', 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Uraian', 
												'value' => $this->form_validation->set_value('name'));

            $this->data['date'] = array('name' => 'date', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField  datepicker-input', 
												'field-name' => 'date', 
												'placeholder' => 'Tanggal', 
												'value' => $this->form_validation->set_value('date', date("d-m-Y")));

            $this->data['value'] = array('name' => 'value', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'value', 
												'placeholder' => 'Nilai', 
												'value' => $this->form_validation->set_value('value'));
										
			$this->data['submenu'] = "Add Pengeluaran";
			$this->data['active_menu'] = "expense_operational";
			
			$this->data['scripts'][] = "plugins/moment/moment.min.js";
			$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
			$this->data['scripts'][] = "scripts/expense/operationalCreate.js";
			$this->data['styles'][] = "plugins/datepicker/datepicker3.css";
			
			$this->view("expense/v_expense_operational_create", $this->data);
        }
	}
	
	public function expense_operational_edit($id = 0)
	{
		$reject_user = !$this->is_has_access("expense_operational");
		
		$this->load->model("m_expense");
		
		$form_data = $this->m_expense->get_one($id);
		$this->data["form_data"] = $form_data;
		$reject_user = false;
		
		if(!$form_data){
			$reject_user = true;
		}
		
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		
		}
		$this->data["is_edit"] = true;
		//validate form input
        $this->form_validation->set_rules('name', 'Nama', 'required|xss_clean|min_length[1]|max_length[25]');
        $this->form_validation->set_rules('date', 'Tanggal', 'required|xss_clean|min_length[1]|max_length[25]');
        $this->form_validation->set_rules('value', 'Nilai', 'required|xss_clean|min_length[1]|max_length[25]|numeric');
		
        if ($this->form_validation->run() == true) {						
			$name = $this->input->post('name');
			$date = date("Y-m-d", strtotime($this->input->post('date')));
			$value = $this->input->post('value');
			
			$data_input = array(
				"name" => $name,
				"date" => $date,
				"value" => $value,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);		
			
			$save = $this->m_expense->save($data_input, $form_data->id);

			if ($save === false) {
				$this->session->set_flashdata('message', 'Failed edit Pengeluaran');
			}
			else {
				$this->session->set_flashdata('message_success', 'Success edit pengeluaran '.$name.'!');
			}
	
            $btnaction = $this->input->post('btnAction');
            if ($btnaction == 'save_exit') {
                redirect('main/expense_operational', 'refresh');
            }
            else {
                redirect('main/expense_operational_edit/'.$id, 'refresh');
            }
        }
        else {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['message_success'] = $this->session->flashdata('message_success');

            $this->data['name'] = array('name' => 'name', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'Name', 
												'placeholder' => 'Nama', 
												'value' => $this->form_validation->set_value('name', $form_data->name));

            $this->data['date'] = array('name' => 'date', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField  datepicker-input', 
												'field-name' => 'date', 
												'placeholder' => 'Tanggal', 
												'value' => $this->form_validation->set_value('date', date("d-m-Y", strtotime($form_data->date))));

            $this->data['value'] = array('name' => 'value', 
												'type' => 'text', 
												'class' => 'form-control requiredTextField', 
												'field-name' => 'value', 
												'placeholder' => 'Nilai', 
												'value' => $this->form_validation->set_value('value', $form_data->value));
										
			$this->data['submenu'] = "Edit Pengeluaran";
			$this->data['active_menu'] = "expense_operational";
			
			$this->data['scripts'][] = "plugins/moment/moment.min.js";
			$this->data['scripts'][] = "plugins/datepicker/bootstrap-datepicker.js";
			$this->data['scripts'][] = "scripts/expense/operationalCreate.js";
			$this->data['styles'][] = "plugins/datepicker/datepicker3.css";
			
			$this->view("expense/v_expense_operational_create", $this->data);
        }
	}
	
	public function expense_operational_delete($id = 0)
	{
		$reject_user = !$this->is_has_access("expense_operational");
		if($reject_user){
			show_error($this->lang->line('error_unauthorized'), 401);
			return;
		}
		$this->load->model("m_expense");
		$examination = $this->m_expense->get_one($id);
		if($examination){
			$column_updated = array(
				"status" => 0,
				"updated_at" => date("Y-m-d"),
				"updated_by" => $this->user->id,
			);
			$saved = $this->m_expense->save($column_updated,$examination->id);
			if($saved){
				$this->session->set_flashdata("message_success","Pengeluaran Deleted!");
			} else {
				$this->session->set_flashdata("message","Delete Pengeluaran Failed!");
			}
		} else {
			$this->session->set_flashdata("message","Invalid Pengeluaran!");
		}
		
		redirect("main/expense_operational");
	}
	
	function dummy(){
		$this->load->model('m_transaction');
		echo $this->m_transaction->get_last_id();
		
	}
}
