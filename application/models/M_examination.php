<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_examination extends MY_Model {
	protected $table = 'examination';
	
	public function get_examination_by_transaction($condition){
		$this->db->select('e.*');
		$this->db->from('examination as e');
		$this->db->join('examination_result as r','e.id = r.examination_id');
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->result();
	}
	
}

