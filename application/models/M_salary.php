<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_salary extends MY_Model {
	protected $table = 'salary';
	
	public function get_outcome($condition){
        $this->db->select("SUM(salary) as salary");
        $this->db->where($condition);
        $this->db->where("status", 1);
		return $this->get($this->table)->row();
	}
}

