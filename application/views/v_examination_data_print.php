<!DOCTYPE html>
<html lang="en">
    <head>
	
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
       
        <title>Mikrotest</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style_struk.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    </head>

    <body>
		<nav class="navbar navbar-default navbar-custom">
		  <div class="container-fluid">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			
				<?php if($display_menu) : ?>
				<ul class="nav navbar-nav">
					<li><a class="btn btn-header" href="<?php echo base_url("main/examinations_results_delete/".$transaction->id);?>">Kembali</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><button onclick="printDiv('printableArea')" class="btn btn-header print" type="button" style="margin-right: 20px;">Print</button></li>
					<li><a a class="btn btn-header" href="<?php echo base_url("main/examinations_results");?>">Selesai</a></li>
				</ul>
				<?php else : ?>
				<ul class="nav navbar-nav navbar-right">
					<li><button onclick="printDiv('printableArea')" class="btn btn-header print" type="button" style="margin-right: 20px;">Print</button></li>
					<li><a class="btn btn-header" href="<?php echo $_SERVER['HTTP_REFERER'];?>">Kembali</a></li>

				</ul>
				<?php endif ?>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		<div id="printableArea" class="container font-12">
			<div class="col-sm-12 padding-top-120">
				<table class="full-width table-border margin-bottom-20">
					<tr>
						<td class="top-border-double border-right-hide" style="width:16%;">Nama Pasien</td>
						<td class="top-border-double border-right-hide border-left-hide" style="width:10px;">:</td>
						<td class="top-border-double border-left-hide" style="width:34%;"><?php echo $patient->name;?></td>
						<td class="top-border-double border-right-hide" style="width:16%;">Nomor</td>
						<td class="top-border-double border-right-hide border-left-hide" style="width:10px;">:</td>
						<td class="top-border-double border-left-hide" style="width:34%;"><?php echo $transaction->transaction_no;?></td>
					</tr>
					<tr>					
						<?php
							$age = false;
							if($patient->dob){
								$age_diff = date_diff(date_create(date("Y-m-d", strtotime($patient->dob))), 
									date_create(date("Y-m-d", strtotime($transaction->date))));
								$age = $age_diff->format("%y Tahun %m Bulan %d Hari");
							}
						?>
						<td class="border-right-hide">Umur</td>
						<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
						<td class="border-left-hide"><?php echo $age;?></td>
						<td class="border-right-hide">Tanggal</td>
						<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
						<td class="border-left-hide"><?php echo date("d/m/Y", strtotime($transaction->date));?></td>
					</tr>
					<tr>
						<td class="border-right-hide">Jenis Kelamin</td>
						<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
						<td class="border-left-hide"><?php echo (strtolower($patient->gender) == "l" ? "Laki-laki" : (strtolower($patient->gender) == "p" ? "Perempuan" : ""));?></td>
						<td class="border-right-hide">Dokter</td>
						<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
						<td class="border-left-hide"><?php echo $doctor->name;?></td>
					</tr>
					<tr>
						<td class="border-right-hide">Alamat</td>
						<td class="border-right-hide border-left-hide" style="width:10px;">:</td>
						<td class="border-left-hide"><?php echo $patient->address;?></td>
						<td class="border-right-hide"></td>
						<td class="border-right-hide border-left-hide" style="width:10px;"></td>
						<td class="border-left-hide"></td>
					</tr>
					<tr>
						<td colspan="6"></td>
					</tr>
				</table>
				<table class="full-width table-border">
					<tr>
						<td class="uppercase table-header" colspan="5">hasil pemeriksaan</td>
					</tr>
					<tr>
						<td class="table-header">Pemeriksaan</td>
						<td class="table-header" style="width:80px;">Hasil</td>
						<td class="table-header" style="width:80px;">Satuan</td>
						<td class="table-header" style="width:100px;">Normal</td>
						<td class="table-header" style="width:300px;">Keterangan</td>
					</tr>
					<?php foreach($examination as $exmntn) : ?>
					<?php if($exmntn->type == 1){ ?>
					<tr>
						<td><?php echo $exmntn->name;?></td>
						<td class="text-center"><?php echo $exmntn->examination_result->result_value;?></td>
						<td class="text-center"><?php echo $exmntn->unit;?></td>
						<td class="text-center"><?php echo $exmntn->normal_value;?></td>
						<td><?php echo $exmntn->examination_result->notes;?></td>
					</tr>
					<?php }else{ ?>
					<tr>
						<td><?php echo $exmntn->name;?></td>
						<td colspan="4" class="text-center"></td>
					</tr>
					<?php } ?>
					<?php foreach($exmntn->subexamination as $subexamination) : ?>
					<?php if($subexamination->type == 1){ ?>
					<tr>
						<td style="padding-left:15px;"><?php echo $subexamination->name;?></td>
						<td class="text-center"><?php echo $subexamination->examination_result->result_value;?></td>
						<td class="text-center"><?php echo $subexamination->unit;?></td>
						<td class="text-center"><?php echo $subexamination->normal_value;?></td>
						<td><?php echo $subexamination->examination_result->notes;?></td>
					</tr>
					<?php }else{ ?>
					<tr>
						<td style="padding-left:15px;"><?php echo $subexamination->name;?></td>
						
						<td colspan="4"><?php echo $subexamination->examination_result->notes;?></td>
					</tr>
					<?php } ?>
					<?php endforeach ?>
					<?php endforeach ?>
				</table>
			</div>
			<div class="col-xs-3 pull-right">
				<p class="text-center padding-top-20">Soreang, <?php echo date("d/m/Y");?> <br>Pemeriksa,</p>
				<p class="text-center padding-top-60"><?php echo $user->first_name." ".$user->last_name;?></p>
			</div>
		</div>
	</body>
	
</html>
<script>

	
		
	function printDiv(divName) {
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
		initDatePicker();
	}
	initDatePicker();
	
	function initDatePicker(){
		//Date picker
		$(document).find('.datepicker-input').datepicker({
			autoclose: true,
			format: "yyyy-mm-dd",
		});	
	}


</script>